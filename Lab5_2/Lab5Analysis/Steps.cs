﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab5Analysis
{
    public partial class Steps : Form
    {
        public MainForm f;

        public Steps(MainForm form)
        {
            InitializeComponent();
            f = form;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            f.groupBoxTable1_1.Visible = true;
            f.groupBoxTable1_2.Visible = false;
            f.groupBoxTable2.Visible = false;
            f.groupBoxTable2_2.Visible = false;
            f.groupBoxTable3_1.Visible = false;
            f.groupBoxTable4_1.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            f.groupBoxTable1_1.Visible = false;
            f.groupBoxTable1_2.Visible = true;
            f.groupBoxTable2.Visible = false;
            f.groupBoxTable2_2.Visible = false;
            f.groupBoxTable3_1.Visible = false;
            f.groupBoxTable4_1.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            f.groupBoxTable1_1.Visible = false;
            f.groupBoxTable1_2.Visible = false;
            f.groupBoxTable2.Visible = true;
            f.groupBoxTable2_2.Visible = false;
            f.groupBoxTable3_1.Visible = false;
            f.groupBoxTable4_1.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            f.groupBoxTable1_1.Visible = false;
            f.groupBoxTable1_2.Visible = false;
            f.groupBoxTable2.Visible = false;
            f.groupBoxTable2_2.Visible = true;
            f.groupBoxTable3_1.Visible = false;
            f.groupBoxTable4_1.Visible = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            f.groupBoxTable1_1.Visible = false;
            f.groupBoxTable1_2.Visible = false;
            f.groupBoxTable2.Visible = false;
            f.groupBoxTable2_2.Visible = false;
            f.groupBoxTable3_1.Visible = true;
            f.groupBoxTable4_1.Visible = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if(f.flag == false)
            f.RecountDataTable4_1();
            f.groupBoxTable1_1.Visible = false;
            f.groupBoxTable1_2.Visible = false;
            f.groupBoxTable2.Visible = false;
            f.groupBoxTable2_2.Visible = false;
            f.groupBoxTable3_1.Visible = false;
            f.groupBoxTable4_1.Visible = true;
        }

        private void вихідToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
