﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab5Analysis
{
    public partial class MainForm : Form
    {
        double percent1 = 0, percent2 = 0, percent3 = 0, percent4 = 0;
        double sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0;
        double counts = 0, counts1 = 0, counts2 = 0, counts3 = 0, counts4 = 0;
        private double bord1Start = 0, bord1End = 0, bord2Start = 0, bord2End = 0, bord3Start = 0, bord3End = 0;
        private double genRysk = 0;
        public bool flag = false;
        private double[] randmass;
        private double[] reqValue;
        private double reqSum;
        private double[] minReq;
        private double[] maxReq;
        private int index;
        private int[] marks;
        private double reduceKoef;

        public Color COLOR_VERY_LOW = Color.LightGreen;
        public Color COLOR_LOW = Color.ForestGreen;
        public Color COLOR_MEDIUM = Color.Yellow;
        public Color COLOR_HIGH = Color.IndianRed;
        public Color COLOR_VERY_HIGH = Color.DarkRed;

        public MainForm()
        {
            InitializeComponent();

            Table1_1.CellValueChanged += Table1_1_CellValueChanged;
            Table1_2.CellValueChanged += Table1_2_CellValueChanged;

            DataForTable1_1();
            DataForTable1_2();
            DataForTable2_1(12, 0, Table2_1_1);
            DataForTable2_1(8, 12, Table2_1_2);
            DataForTable2_1(10, 20, Table2_1_3);
            DataForTable2_1(15, 30, Table2_1_4);
            CountGenRyskForTable2_1();
            minReq = new double[4];
            maxReq = new double[4];
            DataForTable2_2(12, 0, Table2_2_1);
            DataForTable2_2(8, 12, Table2_2_2);
            DataForTable2_2(10, 20, Table2_2_3);
            DataForTable2_2(15, 30, Table2_2_4);
            WorkOutPriority();
            getPriorityForTable2_2(11, Table2_2_1);
            getPriorityForTable2_2(7, Table2_2_2);
            getPriorityForTable2_2(9, Table2_2_3);
            getPriorityForTable2_2(14, Table2_2_4);
            dataForTable_2_Sum(2);
            DataForTable3_1();
            DataForTable4_1();

            groupBoxTable1_1.Visible = true;
            groupBoxTable1_2.Visible = true;
            groupBoxTable2.Visible = true;
            groupBoxTable2_2.Visible = true;
            groupBoxTable3_1.Visible = true;
            groupBoxTable4_1.Visible = true;

            Table2_1_1.CellValueChanged += Table2_1_1_CellValueChanged;
            Table2_1_2.CellValueChanged += Table2_1_2_CellValueChanged;
            Table2_1_3.CellValueChanged += Table2_1_3_CellValueChanged;
            Table2_1_4.CellValueChanged += Table2_1_4_CellValueChanged;

            Table2_2_1.CellValueChanged += Table2_2_1_CellValueChanged;
            Table2_2_2.CellValueChanged += Table2_2_2_CellValueChanged;
            Table2_2_3.CellValueChanged += Table2_2_3_CellValueChanged;
            Table2_2_4.CellValueChanged += Table2_2_4_CellValueChanged;

            Table3_1.CellValueChanged += Table3_1_CellValueChanged;
        }

        private void Table1_2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex != 1) return;

            Table1_2.CellValueChanged -= Table1_2_CellValueChanged;
            RecountChangesTable1(0, 12, 20, 30, 45, Table1_2);

            Color color = Color.Khaki;
            int val = int.Parse(((DataGridView) sender).Rows[e.RowIndex].Cells[1].Value.ToString());

            if(1 == val)
            {
                color = Color.IndianRed;
            }
            else if(0 == val)
            {
                color = Color.LightGreen;
            }

            ((DataGridView) sender).Rows[e.RowIndex].DefaultCellStyle.BackColor = color;

            Table1_2.CellValueChanged += Table1_2_CellValueChanged;
        }

        private void Table1_1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {   
            if(e.ColumnIndex != 1) return;
            
            Table1_1.CellValueChanged -= Table1_1_CellValueChanged;
            RecountChangesTable1(0, 8, 12, 16, 22, Table1_1);

            Color color = Color.Khaki;
            int val = int.Parse(((DataGridView) sender).Rows[e.RowIndex].Cells[1].Value.ToString());

            if(1 == val)
            {
                color = Color.IndianRed;
            }
            else if(0 == val)
            {
                color = Color.LightGreen;
            }

            ((DataGridView) sender).Rows[e.RowIndex].DefaultCellStyle.BackColor = color;

            Table1_1.CellValueChanged += Table1_1_CellValueChanged;
        }

        private void Table3_1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Table3_1.CellValueChanged -= Table3_1_CellValueChanged;
            CountValueForReduceTable4_1();
            RecountChangesTable4_1();
            Table3_1.CellValueChanged += Table3_1_CellValueChanged;
        }

        private void Table2_2_4_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Table2_2_4.CellValueChanged -= Table2_2_4_CellValueChanged;
            index = 0;
            RecountChangesTable2_2(14, 30, Table2_2_4);
            WorkOutPriority();
            //getPriorityForTable2_2(11, Table2_2_1);
            //getPriorityForTable2_2(7, Table2_2_2);
            //getPriorityForTable2_2(9, Table2_2_3);
            //getPriorityForTable2_2(14, Table2_2_4);
            dataForTable_2_Sum(2);
            Table2_2_4.CellValueChanged += Table2_2_4_CellValueChanged;
        }

        private void Table2_2_3_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Table2_2_3.CellValueChanged -= Table2_2_3_CellValueChanged;
            index = 0;
            RecountChangesTable2_2(9, 20, Table2_2_3);
            WorkOutPriority();
            //getPriorityForTable2_2(11, Table2_2_1);
            //getPriorityForTable2_2(7, Table2_2_2);
            //getPriorityForTable2_2(9, Table2_2_3);
            //getPriorityForTable2_2(14, Table2_2_4);
            dataForTable_2_Sum(2);
            Table2_2_3.CellValueChanged += Table2_2_3_CellValueChanged;
        }

        private void Table2_2_2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Table2_2_2.CellValueChanged -= Table2_2_2_CellValueChanged;
            index = 0;
            RecountChangesTable2_2(7, 12, Table2_2_2);
            WorkOutPriority();
            //getPriorityForTable2_2(11, Table2_2_1);
            //getPriorityForTable2_2(7, Table2_2_2);
            //getPriorityForTable2_2(9, Table2_2_3);
            //getPriorityForTable2_2(14, Table2_2_4);
            dataForTable_2_Sum(2);
            Table2_2_2.CellValueChanged += Table2_2_2_CellValueChanged;
        }

        private void Table2_2_1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Table2_2_1.CellValueChanged -= Table2_2_1_CellValueChanged;
            index = 0;
            RecountChangesTable2_2(11, 0, Table2_2_1);
            WorkOutPriority();
            //getPriorityForTable2_2(11, Table2_2_1);
            //getPriorityForTable2_2(7, Table2_2_2);
            //getPriorityForTable2_2(9, Table2_2_3);
            //getPriorityForTable2_2(14, Table2_2_4);
            dataForTable_2_Sum(2);
            Table2_2_1.CellValueChanged += Table2_2_1_CellValueChanged;
        }

        private void Table2_1_3_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Table2_1_3.CellValueChanged -= Table2_1_3_CellValueChanged;
            RecountChangesTable2_1(9, 20, Table2_1_3);
            CountGenRyskForTable2_1();
            Table2_1_3.CellValueChanged += Table2_1_3_CellValueChanged;
        }

        private void Table2_1_2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Table2_1_2.CellValueChanged -= Table2_1_2_CellValueChanged;
            RecountChangesTable2_1(7, 12, Table2_1_2);
            CountGenRyskForTable2_1();
            Table2_1_2.CellValueChanged += Table2_1_2_CellValueChanged;
        }

        private void Table2_1_1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Table2_1_1.CellValueChanged -= Table2_1_1_CellValueChanged;
            RecountChangesTable2_1(11, 0, Table2_1_1);
            CountGenRyskForTable2_1();
            Table2_1_1.CellValueChanged += Table2_1_1_CellValueChanged;
        }

        private void Table2_1_4_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Table2_1_4.CellValueChanged -= Table2_1_4_CellValueChanged;
            RecountChangesTable2_1(14, 30, Table2_1_4);
            CountGenRyskForTable2_1();
            Table2_1_4.CellValueChanged += Table2_1_4_CellValueChanged;
        }

        private void етапиУправлінняРизикамиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Steps s = new Steps(this);
            s.Show();
        }


        public void DataForTable1_1()
        {
            int temp = 0;
            int n = 23;
            for (int i = 0; i < n; i++)
                Table1_1.Rows.Add();

            Table1_1.Rows[0].Cells[0].Value = "1. Множина джерел появи технічних ризиків";
            Table1_1.Rows[0].Cells[0].Style.Font = new Font(Table1_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table1_1.Rows[1].Cells[0].Value = "1.1. функціональні характеристики ПЗ";
            Table1_1.Rows[2].Cells[0].Value = "1.2. характеристики якості ПЗ";
            Table1_1.Rows[3].Cells[0].Value = "1.3. характеристики надійності ПЗ";
            Table1_1.Rows[4].Cells[0].Value = "1.4. застосовність ПЗ";
            Table1_1.Rows[5].Cells[0].Value = "1.5. часова продуктивність ПЗ";
            Table1_1.Rows[6].Cells[0].Value = "1.6. супроводжуваність ПЗ";
            Table1_1.Rows[7].Cells[0].Value = "1.7. повторне використання компонент ПЗ";
            Table1_1.Rows[8].Cells[0].Value = "2.Множина джерел появи вартісних ризиків";
            Table1_1.Rows[8].Cells[0].Style.Font = new Font(Table1_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table1_1.Rows[9].Cells[0].Value = "2.1. обмеження сумарного бюджету на програмний проект";
            Table1_1.Rows[10].Cells[0].Value = "2.2. недоступна вартість реалізації програмного проекту";
            Table1_1.Rows[11].Cells[0].Value =
                "2.3. низька ступінь реалізму при оцінюванні витрат на \n програмний проект;";
            Table1_1.Rows[12].Cells[0].Value = "3. Множина джерел появи планових ризиків";
            Table1_1.Rows[12].Cells[0].Style.Font = new Font(Table1_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table1_1.Rows[13].Cells[0].Value =
                "3.1. властивості та можливості гнучкості внесення змін до планів життєвого циклу розроблення ПЗ,";
            Table1_1.Rows[14].Cells[0].Value =
                "3.2. можливості порушення встановлених термінів реалізації етапів життєвого циклу розроблення ПЗ,";
            Table1_1.Rows[15].Cells[0].Value =
                "3.3. низька ступінь реалізму при встановленні планів і етапів життєвого циклу розроблення ПЗ;";
            Table1_1.Rows[16].Cells[0].Value =
                "4. Множина джерел появи ризиків реалізації процесу управління програмним проектом";
            Table1_1.Rows[16].Cells[0].Style.Font = new Font(Table1_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table1_1.Rows[17].Cells[0].Value = "4.1. хибна стратегія реалізації програмного проекту";
            Table1_1.Rows[18].Cells[0].Value = "4.2. неефективне планування проекту розроблення ПЗ";
            Table1_1.Rows[19].Cells[0].Value = "4.3. неякісне оцінювання програмного проекту";
            Table1_1.Rows[20].Cells[0].Value = "4.4. прогалини в документуванні етапів реалізації програмного проекту";
            Table1_1.Rows[21].Cells[0].Value =
                "4.5. промахи в прогнозуванні результатів реалізації програмного проекту";
            Table1_1.Rows[22].Cells[0].Value = "Всього";
            Table1_1.Rows[22].Cells[0].Style.Font = new Font(Table1_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table1_1.Rows[22].Cells[0].ReadOnly = true;
            Table1_1.Rows[22].Cells[1].ReadOnly = true;
            Table1_1.Rows[22].Cells[2].ReadOnly = true;

            for (int i = 0; i < n; i++)
            {
                Table1_1.Rows[i].Cells[0].ReadOnly = true;
                Table1_1.Rows[i].Cells[2].ReadOnly = true;
            }

            for (int i = 0; i < 7; i++)
            {
                Table1_1.Rows[i + 1].Cells[1].Value = 1;
                temp++;
            }

            Table1_1.Rows[0].Cells[1].Value = temp;
            Table1_1.Rows[0].Cells[1].ReadOnly = true;

            temp = 0;
            for (int i = 0; i < 3; i++)
            {
                Table1_1.Rows[i + 9].Cells[1].Value = 1;
                Table1_1.Rows[i + 13].Cells[1].Value = 1;
                temp++;
            }

            Table1_1.Rows[8].Cells[1].Value = temp;
            Table1_1.Rows[12].Cells[1].Value = temp;
            Table1_1.Rows[8].Cells[1].ReadOnly = true;
            Table1_1.Rows[12].Cells[1].ReadOnly = true;

            temp = 0;
            for (int i = 0; i < 5; i++)
            {
                Table1_1.Rows[i + 17].Cells[1].Value = 1;
                temp++;
            }

            Table1_1.Rows[16].Cells[1].Value = temp;
            Table1_1.Rows[16].Cells[1].ReadOnly = true;

            RecountChangesTable1(0, 8, 12, 16, 22, Table1_1);
        }

        public void RecountChangesTable1(int Req1, int Req2, int Req3, int Req4, int AllReq, DataGridView table)
        {
            sum1 = 0;
            sum2 = 0;
            sum3 = 0;
            sum4 = 0;
            counts1 = Convert.ToInt32(table.Rows[Req1].Cells[1].Value);
            counts2 = Convert.ToInt32(table.Rows[Req2].Cells[1].Value);
            counts3 = Convert.ToInt32(table.Rows[Req3].Cells[1].Value);
            counts4 = Convert.ToInt32(table.Rows[Req4].Cells[1].Value);

            for (int i = 0; i < AllReq; i++)
            {
                if (i == Req1 || i == Req2 || i == Req3 || i == Req4)
                {
                    continue;
                }

                try
                {
                    if (Convert.ToInt32(table.Rows[i].Cells[1].Value) != 0 &&
                        Convert.ToInt32(table.Rows[i].Cells[1].Value) != 1)
                    {
                        MessageBox.Show("Значення можуть бути лише або 1, або 0!", "Увага", MessageBoxButtons.OK);
                        table.Rows[i].Cells[1].Value = 1;
                        return;
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show("Значення можуть бути лише цілого типу!", "Увага", MessageBoxButtons.OK);
                    table.Rows[i].Cells[1].Value = 1;
                    return;
                }
            }

            counts = counts1 + counts2 + counts3 + counts4;

            for (int i = 0; i < counts1; i++)
            {
                sum1 += Convert.ToInt32(table.Rows[i + Req1 + 1].Cells[1].Value);
            }

            percent1 = (sum1 / counts) * 100;
            table.Rows[Req1].Cells[2].Value = percent1.ToString("##.##") + " %";

            for (int i = 0; i < counts2; i++)
            {
                sum2 += Convert.ToInt32(table.Rows[i + Req2 + 1].Cells[1].Value);
            }

            percent2 = (sum2 / counts) * 100;
            table.Rows[Req2].Cells[2].Value = percent2.ToString("##.##") + " %";

            for (int i = 0; i < counts3; i++)
            {
                sum3 += Convert.ToInt32(table.Rows[i + Req3 + 1].Cells[1].Value);
            }

            percent3 = (sum3 / counts) * 100;
            table.Rows[Req3].Cells[2].Value = percent3.ToString("##.##") + " %";

            for (int i = 0; i < counts4; i++)
            {
                sum4 += Convert.ToInt32(table.Rows[i + Req4 + 1].Cells[1].Value);
            }

            percent4 = (sum4 / counts) * 100;
            table.Rows[Req4].Cells[2].Value = percent4.ToString("##.##") + " %";
            sum1 = 0;
            sum2 = 0;
            sum3 = 0;
            sum4 = 0;

            table.Rows[AllReq].Cells[1].Value = counts;
            table.Rows[AllReq].Cells[2].Value = (percent1 + percent2 + percent3 + percent4).ToString("##.##") + " %";
        }

        public void DataForTable1_2()
        {
            int temp = 0;
            int n = 46;
            for (int i = 0; i < n; i++)
                Table1_2.Rows.Add();

            Table1_2.Rows[0].Cells[0].Value = "1. Множина настання технічних ризикових подій";
            Table1_2.Rows[0].Cells[0].Style.Font = new Font(Table1_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table1_2.Rows[1].Cells[0].Value =
                "1.1. Затримки у постачанні обладнання, необхідного для підтримки процесу розроблення ПЗ";
            Table1_2.Rows[2].Cells[0].Value =
                "1.2. Затримки у постачанні інструментальних засобів, необхідних для підтримки процесу розроблення ПЗ";
            Table1_2.Rows[3].Cells[0].Value =
                "1.3. Небажання команди виконавців використовувати інструментальні засоби для підтримки процесу розроблення ПЗ";
            Table1_2.Rows[4].Cells[0].Value =
                "1.4. Формування запитів на більш потужні інструментальні засоби розроблення ПЗ";
            Table1_2.Rows[5].Cells[0].Value = "1.5. Відмова команди виконавців від CASE-засобів розроблення ПЗ";
            Table1_2.Rows[6].Cells[0].Value =
                "1.6. Неефективність програмного коду, згенерованого CASE-засобами розроблення ПЗ";
            Table1_2.Rows[7].Cells[0].Value =
                "1.7. Неможливість інтеграції CASE-засобів з іншими інструментальними засобами для підтримки процесу розроблення ПЗ";
            Table1_2.Rows[8].Cells[0].Value =
                "1.8. Недостатня продуктивність баз(и) даних для підтримки процесу розроблення ПЗ";
            Table1_2.Rows[9].Cells[0].Value =
                "1.9. Програмні компоненти, які використовують повторно в ПЗ, мають дефекти та обмежені функціональні можливості";
            Table1_2.Rows[10].Cells[0].Value =
                "1.10. Швидкість виявлення дефектів у програмному коді є нижчою від раніше запланованих термінів";
            Table1_2.Rows[11].Cells[0].Value =
                "1.11. Поява дефектних системних компонент, які використовують для розроблення ПЗ";
            Table1_2.Rows[12].Cells[0].Value = "2.Множина настання вартісних ризикових подій";
            Table1_2.Rows[12].Cells[0].Style.Font = new Font(Table1_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table1_2.Rows[13].Cells[0].Value =
                "2.1. Недо(пере)оцінювання витрат на реалізацію програмного проекту (надмірно низька вартість)";
            Table1_2.Rows[14].Cells[0].Value = "2.2. Фінансові ускладнення у компанії-замовника ПЗ";
            Table1_2.Rows[15].Cells[0].Value = "2.3. фінансові ускладнення у компанії-розробника ПЗ";
            Table1_2.Rows[16].Cells[0].Value =
                "2.4. Змен(збіль)шення бюджету програмного проекта з ініціативи компанії-замовника ПЗ під час його реалізації";
            Table1_2.Rows[17].Cells[0].Value =
                "2.5. Висока вартість виконання повторних робіт, необхідних для зміни вимог до ПЗ";
            Table1_2.Rows[18].Cells[0].Value = "2.6. Реорганізація структурних підрозділів у компанії-замовника ПЗ";
            Table1_2.Rows[19].Cells[0].Value = "2.7. Реорганізація команди виконавців у компанії-розробника ПЗ";
            Table1_2.Rows[20].Cells[0].Value = "3. Множина настання планових ризикових подій";
            Table1_2.Rows[20].Cells[0].Style.Font = new Font(Table1_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table1_2.Rows[21].Cells[0].Value = "3.1.  зміни графіка виконання робіт з боку замовника чи розробника ПЗ";
            Table1_2.Rows[22].Cells[0].Value =
                "3.2.   порушення графіка виконання робіт з боку компанії-розробника ПЗ;";
            Table1_2.Rows[23].Cells[0].Value =
                "3.3.   потреба зміни користувацьких вимог до ПЗ з боку компанії-замовника ПЗ";
            Table1_2.Rows[24].Cells[0].Value =
                "3.4.   потреба зміни функціональних вимог до ПЗ з боку компанії-розробника ПЗ";
            Table1_2.Rows[25].Cells[0].Value =
                "3.5.   потреба виконання великої кількості повторних робіт, необхідних для зміни вимог до ПЗ";
            Table1_2.Rows[26].Cells[0].Value =
                "3.6. недо(пере)оцінювання тривалості етапів реалізації програмного проекту з боку компанії-замовника ПЗ";
            Table1_2.Rows[27].Cells[0].Value =
                "3.7.   остаточний розмір ПЗ значно перевищує (менший від) заплановані(их) його характеристики";
            Table1_2.Rows[28].Cells[0].Value = "3.8.   поява на ринку аналогічного ПЗ до виходу замовленого";
            Table1_2.Rows[29].Cells[0].Value = "3.9.   поява на ринку більш конкурентоздатного ПЗ";
            Table1_2.Rows[30].Cells[0].Value =
                "4. Множина настання ризикових подій реалізації процесу управління програмним проектом";
            Table1_2.Rows[30].Cells[0].Style.Font = new Font(Table1_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table1_2.Rows[31].Cells[0].Value = "4.1.   низький моральний стан персоналу команди виконавців ПЗ";
            Table1_2.Rows[32].Cells[0].Value = "4.2.   низька взаємодія між членами команди виконавців ПЗ";
            Table1_2.Rows[33].Cells[0].Value = "4.3.   пасивність керівника (менеджера) програмного проекту";
            Table1_2.Rows[34].Cells[0].Value =
                "4.4.   недостатня компетентність керівника (менеджера) програмного проекту";
            Table1_2.Rows[35].Cells[0].Value =
                "4.5.   незадоволеність замовника результатами етапів реалізації програмного проекту";
            Table1_2.Rows[36].Cells[0].Value =
                "4.6.   недостатня кількість фахівців у команді виконавців ПЗ з необхідним професійним рівнем";
            Table1_2.Rows[37].Cells[0].Value =
                "4.7.   хвороба провідного виконавця в найкритичніший момент розроблення ПЗ";
            Table1_2.Rows[38].Cells[0].Value = "4.8.   одночасна хвороба декількох виконавців підчас розроблення ПЗ";
            Table1_2.Rows[39].Cells[0].Value =
                "4.9.   неможливість організації необхідного навчання персоналу команди виконавців ПЗ";
            Table1_2.Rows[40].Cells[0].Value = "4.10.   зміна пріоритетів у процесі управління програмним проектом";
            Table1_2.Rows[41].Cells[0].Value =
                "4.11. недо(пере)оцінювання необхідної кількості розробників (підрядників і субпідрядників) на етапах життєвого циклу розроблення ПЗ";
            Table1_2.Rows[42].Cells[0].Value =
                "4.12. недостатнє (надмірне) документування результатів на етапах реалізації програмного проекту";
            Table1_2.Rows[43].Cells[0].Value =
                "4.13.   нереалістичне прогнозування результатів на етапах реалізації програмного проекту";
            Table1_2.Rows[44].Cells[0].Value =
                "4.14.   недостатній професійний рівень представників від компанії-замовника ПЗ";
            Table1_2.Rows[45].Cells[0].Value = "Всього";
            Table1_2.Rows[45].Cells[0].Style.Font = new Font(Table1_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table1_2.Rows[45].Cells[0].ReadOnly = true;
            Table1_2.Rows[45].Cells[1].ReadOnly = true;
            Table1_2.Rows[45].Cells[2].ReadOnly = true;

            temp = 0;

            for (int i = 0; i < n; i++)
            {
                Table1_2.Rows[i].Cells[0].ReadOnly = true;
                Table1_2.Rows[i].Cells[2].ReadOnly = true;
            }

            for (int i = 0; i < 11; i++)
            {
                Table1_2.Rows[i + 1].Cells[1].Value = 1;
                temp++;
            }

            Table1_2.Rows[0].Cells[1].Value = temp;
            Table1_2.Rows[0].Cells[1].ReadOnly = true;

            temp = 0;

            for (int i = 0; i < 7; i++)
            {
                Table1_2.Rows[i + 13].Cells[1].Value = 1;
                temp++;
            }

            Table1_2.Rows[12].Cells[1].Value = temp;
            Table1_2.Rows[12].Cells[1].ReadOnly = true;

            temp = 0;

            for (int i = 0; i < 9; i++)
            {
                Table1_2.Rows[i + 21].Cells[1].Value = 1;
                temp++;
            }

            Table1_2.Rows[20].Cells[1].Value = temp;
            Table1_2.Rows[20].Cells[1].ReadOnly = true;

            temp = 0;

            for (int i = 0; i < 14; i++)
            {
                Table1_2.Rows[i + 31].Cells[1].Value = 1;
                temp++;
            }

            Table1_2.Rows[30].Cells[1].Value = temp;
            Table1_2.Rows[30].Cells[1].ReadOnly = true;

            RecountChangesTable1(0, 12, 20, 30, 45, Table1_2);
        }

        private void Table1_1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void Table1_2_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
        }

        public void DataForTable2_1(int ReqCnt, int ReqPosition, DataGridView tablefill2)
        {
            for (int i = 0; i < ReqCnt; i++)
            {
                if (tablefill2.Rows.Count >= ReqCnt)
                    break;
                tablefill2.Rows.Add();
            }

            Random rand = new Random();

            for (int i = 0; i < ReqCnt; i++)
            {
                tablefill2.Rows[i].Cells[0].Value = Table1_2.Rows[i + ReqPosition].Cells[0].Value;
                tablefill2.Rows[i].Cells[0].ReadOnly = true;
                tablefill2.Rows[i].Cells[1].Value = Table1_2.Rows[i + ReqPosition].Cells[1].Value;
                tablefill2.Rows[i].Cells[0].Style.BackColor = Color.Azure;
                tablefill2.Rows[i].Cells[1].Style.BackColor = Color.Azure;
            }

            tablefill2.Rows[0].Cells[1].ReadOnly = true;
            tablefill2.Rows[0].Cells[0].Style.Font = new Font(Table1_1.DefaultCellStyle.Font, FontStyle.Bold);

            marks = new int[10];
            int marksSum = 0;

            for (int i = 0; i < 10; i++)
            {
                tablefill2.Rows[0].Cells[i + 2].Value = rand.Next(5, 11);
                marks[i] = Convert.ToInt32(tablefill2.Rows[0].Cells[i + 2].Value);
                tablefill2.Rows[0].Cells[i + 2].Style.BackColor = COLOR_LOW;
                marksSum += Convert.ToInt32(tablefill2.Rows[0].Cells[i + 2].Value);
                tablefill2.Rows[0].Cells[i + 13].Style.BackColor = Color.CornflowerBlue;
                tablefill2.Rows[0].Cells[i + 13].ReadOnly = true;

            }

            randmass = new double[(ReqCnt - 1) * 10];

            for (int i = 0; i < randmass.Length; i++)
            {
                randmass[i] = (Convert.ToDouble(rand.Next(0, 100)) / 100) * 0.8;
            }

            int q = 0;

            for (int i = 0; i < ReqCnt - 1; i++)
            {
                tablefill2.Rows[i + 1].Cells[12].Style.BackColor = Color.Aquamarine;
                tablefill2.Rows[i + 1].Cells[12].ReadOnly = true;
                tablefill2.Rows[i + 1].Cells[23].Style.BackColor = Color.DarkKhaki;
                tablefill2.Rows[i + 1].Cells[23].ReadOnly = true;
                tablefill2.Rows[i].Cells[24].ReadOnly = true;

                for (int j = 0; j < 10; j++)
                {
                    tablefill2.Rows[i + 1].Cells[j + 2].Value = randmass[q];
                    tablefill2.Rows[i + 1].Cells[j + 13].ReadOnly = true;
                    q++;
                }
            }

            tablefill2.Rows[0].Cells[12].Style.BackColor = Color.CadetBlue;
            tablefill2.Rows[0].Cells[12].ReadOnly = true;
            tablefill2.Rows[0].Cells[12].Value = marksSum;
            tablefill2.Rows[0].Cells[23].Style.BackColor = Color.Gold;
            tablefill2.Rows[0].Cells[23].ReadOnly = true;

            RecountChangesTable2_1(ReqCnt - 1, ReqPosition, tablefill2);
        }

        public void RecountChangesTable2_1(int reqCount, int ReqPosition, DataGridView table2)
        {          
            for (int i = 0; i < reqCount; i++)
            {
                table2.Rows[i + 1].Cells[1].Value = Table1_2.Rows[i + ReqPosition + 1].Cells[1].Value;
            }

            for (int i = 0; i < reqCount; i++)
            {
                try
                {
                    if (Convert.ToInt32(table2.Rows[i + 1].Cells[1].Value) != 0 &&
                        Convert.ToInt32(table2.Rows[i + 1].Cells[1].Value) != 1)
                    {
                        MessageBox.Show("Значення можуть бути лише або 1, або 0!", "Увага", MessageBoxButtons.OK);
                        table2.Rows[i + 1].Cells[1].Value = Table1_2.Rows[i + ReqPosition + 1].Cells[1].Value;
                        return;
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show("Значення можуть бути лише цілого типу!", "Увага", MessageBoxButtons.OK);
                    table2.Rows[i + 1].Cells[1].Value = Table1_2.Rows[i + ReqPosition + 1].Cells[1].Value;
                    return;
                }
            }

            int marksSum = 0;

            for (int i = 0; i < 10; i++)
            {
                try
                {
                    marksSum += Convert.ToInt32(table2.Rows[0].Cells[i + 2].Value);
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show("Значення можуть бути лише цілого типу!", "Увага", MessageBoxButtons.OK);
                    table2.Rows[0].Cells[i + 2].Value = marks[i];
                    return;
                }
            }

            table2.Rows[0].Cells[12].Value = marksSum;

            double averagesum = 0;

            for (int i = 0; i < reqCount; i++)
            {

                for (int j = 0; j < 10; j++)
                {
                    try
                    {
                        if (Convert.ToDouble(table2.Rows[i + 1].Cells[j + 2].Value) > 1 ||
                            Convert.ToDouble(table2.Rows[i + 1].Cells[j + 2].Value) < 0)
                        {
                            MessageBox.Show("Значення можуть бути лише в діапазоні від 0 до 1!", "Увага",
                                MessageBoxButtons.OK);
                            table2.Rows[i + 1].Cells[j + 2].Value = randmass[i * 10 + j];
                            return;
                        }

                        averagesum += Convert.ToDouble(table2.Rows[i + 1].Cells[j + 2].Value);
                    }
                    catch (FormatException ex)
                    {
                        Console.WriteLine(ex);
                        MessageBox.Show("Значення можуть бути лише дійсного типу!", "Увага", MessageBoxButtons.OK);
                        table2.Rows[i + 1].Cells[j + 2].Value = randmass[i * 10 + j];
                        return;
                    }
                }

                table2.Rows[i + 1].Cells[12].Value = (averagesum / 10).ToString("0.00");
                averagesum = 0;
            }

            for (int i = 0; i < reqCount; i++)
            {
                for (int j = 0; j < 10; j++)
                {

                    if (Convert.ToInt32(table2.Rows[i + 1].Cells[1].Value) == 0)
                        table2.Rows[i + 1].Cells[j + 13].Value = 0.00;
                    else
                        table2.Rows[i + 1].Cells[j + 13].Value =
                            Convert.ToDouble(table2.Rows[i + 1].Cells[j + 2].Value) *
                            Convert.ToDouble(table2.Rows[0].Cells[j + 2].Value);
                }
            }

            averagesum = 0;
            counts = 0;

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < reqCount; j++)
                {
                    if (Convert.ToDouble(table2.Rows[j + 1].Cells[i + 13].Value) > 0)
                    {
                        counts++;
                        averagesum += Convert.ToDouble(table2.Rows[j + 1].Cells[i + 13].Value);
                    }
                }

                table2.Rows[0].Cells[i + 13].Value =
                    (averagesum / counts / Convert.ToDouble(table2.Rows[0].Cells[i + 2].Value)).ToString("0.00");
            }

            averagesum = 0;

            for (int i = 0; i < reqCount; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    averagesum += Convert.ToDouble(table2.Rows[i + 1].Cells[j + 13].Value);
                }

                table2.Rows[i + 1].Cells[23].Value =
                    (averagesum / Convert.ToDouble(table2.Rows[0].Cells[12].Value)).ToString("0.00");
                averagesum = 0;

            }

            counts = 0;
            for (int i = 0; i < reqCount; i++)
            {
                if (Convert.ToDouble(table2.Rows[i + 1].Cells[23].Value) > 0)
                {
                    counts++;
                    averagesum += Convert.ToDouble(table2.Rows[i + 1].Cells[23].Value);
                }
            }

            table2.Rows[0].Cells[23].Value = (averagesum / counts).ToString("0.00");
            genRysk += Convert.ToDouble(table2.Rows[0].Cells[23].Value);

            for (int i = 0; i <= reqCount; i++)
            {
                if (Convert.ToDouble(table2.Rows[i].Cells[23].Value) == 0.00)
                {
                    table2.Rows[i].Cells[24].Value = " ";
                    table2.Rows[i].Cells[24].Style.BackColor = Color.White;
                }
                else if (Convert.ToDouble(table2.Rows[i].Cells[23].Value) > 0 &&
                         Convert.ToDouble(table2.Rows[i].Cells[23].Value) <= 0.1)
                {
                    table2.Rows[i].Cells[24].Value = "Дуже низька";
                    table2.Rows[i].Cells[24].Style.BackColor = COLOR_VERY_LOW;
                }
                else if (Convert.ToDouble(table2.Rows[i].Cells[23].Value) > 0.1 &&
                         Convert.ToDouble(table2.Rows[i].Cells[23].Value) <= 0.25)
                {
                    table2.Rows[i].Cells[24].Value = "Низька";
                    table2.Rows[i].Cells[24].Style.BackColor = COLOR_LOW;
                }
                else if (Convert.ToDouble(table2.Rows[i].Cells[23].Value) > 0.25 &&
                         Convert.ToDouble(table2.Rows[i].Cells[23].Value) <= 0.5)
                {
                    table2.Rows[i].Cells[24].Value = "Середня";
                    table2.Rows[i].Cells[24].Style.BackColor = COLOR_MEDIUM;
                }
                else if (Convert.ToDouble(table2.Rows[i].Cells[23].Value) > 0.5 &&
                         Convert.ToDouble(table2.Rows[i].Cells[23].Value) <= 0.75)
                {
                    table2.Rows[i].Cells[24].Value = "Висока";
                    table2.Rows[i].Cells[24].Style.BackColor = COLOR_HIGH;
                }
                else if (Convert.ToDouble(table2.Rows[i].Cells[23].Value) > 0.75)
                {
                    table2.Rows[i].Cells[24].Value = "Дуже висока";
                    table2.Rows[i].Cells[24].Style.BackColor = COLOR_VERY_HIGH;
                }
            }
        }

        private void buttonTable2_1_1_Click(object sender, EventArgs e)
        {
            //   DataForTable2_1(12, 0, Table2_1_1);
            RecountChangesTable2_1(11, 0, Table2_1_1);
            groupBoxTable2_1_1.Visible = true;
            groupBoxTable2_1_2.Visible = false;
            groupBoxTable2_1_3.Visible = false;
            groupBoxTable2_1_4.Visible = false;
        }

        private void Table2_1_1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
       
        }

        private void buttonTable2_1_2_Click(object sender, EventArgs e)
        {
            //  DataForTable2_1(8, 12, Table2_1_2);
            RecountChangesTable2_1(7, 12, Table2_1_2);
            groupBoxTable2_1_1.Visible = false;
            groupBoxTable2_1_2.Visible = true;
            groupBoxTable2_1_3.Visible = false;
            groupBoxTable2_1_4.Visible = false;
        }

        private void Table2_1_2_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
                
        }

        private void Table2_1_3_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void buttonTable2_1_3_Click(object sender, EventArgs e)
        {
            // DataForTable2_1(10, 20, Table2_1_3);
            RecountChangesTable2_1(9, 20, Table2_1_3);
            groupBoxTable2_1_1.Visible = false;
            groupBoxTable2_1_2.Visible = false;
            groupBoxTable2_1_3.Visible = true;
            groupBoxTable2_1_4.Visible = false;
        }

        private void Table2_1_4_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void buttonTable2_1_4_Click(object sender, EventArgs e)
        {
            //  DataForTable2_1(15, 30, Table2_1_4);
            RecountChangesTable2_1(14, 30, Table2_1_4);
            groupBoxTable2_1_1.Visible = false;
            groupBoxTable2_1_2.Visible = false;
            groupBoxTable2_1_3.Visible = false;
            groupBoxTable2_1_4.Visible = true;
        }

        public void CountGenRyskForTable2_1()
        {
            genRysk = 0;
            RecountChangesTable2_1(11, 0, Table2_1_1);
            RecountChangesTable2_1(7, 12, Table2_1_2);
            RecountChangesTable2_1(9, 20, Table2_1_3);
            RecountChangesTable2_1(14, 30, Table2_1_4);
            genRysk /= 4;

            genRyskValue.Text = genRysk.ToString("0.000");
            if (genRysk == 0.00)
            {
                genRyskLevel.Text = " ";
                genRyskLevel.BackColor = Color.White;
            }
            else if (genRysk > 0 && genRysk <= 0.1)
            {
                genRyskLevel.Text = "Дуже низька";
                genRyskLevel.BackColor = COLOR_VERY_LOW;
            }
            else if (genRysk > 0.1 && genRysk <= 0.25)
            {
                genRyskLevel.Text = "Низька";
                genRyskLevel.BackColor = COLOR_LOW;
            }
            else if (genRysk > 0.25 && genRysk <= 0.5)
            {
                genRyskLevel.Text = "Середня";
                genRyskLevel.BackColor = COLOR_MEDIUM;
            }
            else if (genRysk > 0.5 && genRysk <= 0.75)
            {
                genRyskLevel.Text = "Висока";
                genRyskLevel.BackColor = COLOR_HIGH;
            }
            else if (genRysk > 0.75)
            {
                genRyskLevel.Text = "Дуже висока";
                genRyskLevel.BackColor = COLOR_VERY_HIGH;
            }
        }

        public void DataForTable2_2(int ReqCnt, int ReqPosition, DataGridView tablefill2_2)
        {
            for (int i = 0; i < ReqCnt; i++)
            {
                if (tablefill2_2.Rows.Count >= ReqCnt)
                    break;
                tablefill2_2.Rows.Add();
            }

            Random rand = new Random();
            reqValue = new double[ReqCnt];
            reqSum = 0;

            for (int i = 0; i < ReqCnt; i++)
            {
                tablefill2_2.Rows[i].Cells[0].Value = Table1_2.Rows[i + ReqPosition].Cells[0].Value;
                tablefill2_2.Rows[i].Cells[0].ReadOnly = true;
                tablefill2_2.Rows[i].Cells[1].Value = Table1_2.Rows[i + ReqPosition].Cells[1].Value;
                tablefill2_2.Rows[i].Cells[0].Style.BackColor = Color.Azure;
                tablefill2_2.Rows[i].Cells[1].Style.BackColor = Color.Azure;
                tablefill2_2.Rows[i].Cells[2].Style.BackColor = Color.LightCyan;
            }

            for (int i = 0; i < ReqCnt - 1; i++)
            {
                reqValue[i] = Convert.ToDouble(rand.Next(2000, 3600)) / 100;
                tablefill2_2.Rows[i + 1].Cells[2].Value = reqValue[i].ToString("00.00");
                reqSum += Convert.ToDouble(tablefill2_2.Rows[i + 1].Cells[2].Value);
            }

            tablefill2_2.Rows[0].Cells[2].Value = reqSum.ToString("00.00");
            tablefill2_2.Rows[0].Cells[2].Style.Font = new Font(Table1_1.DefaultCellStyle.Font, FontStyle.Bold);
            tablefill2_2.Rows[0].Cells[2].Style.BackColor = Color.IndianRed;

            tablefill2_2.Rows[0].Cells[1].ReadOnly = true;
            tablefill2_2.Rows[0].Cells[2].ReadOnly = true;
            tablefill2_2.Rows[0].Cells[0].Style.Font = new Font(Table1_1.DefaultCellStyle.Font, FontStyle.Bold);

            marks = new int[10];
            int marksSum = 0;

            for (int i = 0; i < 10; i++)
            {
                tablefill2_2.Rows[0].Cells[i + 3].Value = rand.Next(5, 11);
                marks[i] = Convert.ToInt32(tablefill2_2.Rows[0].Cells[i + 3].Value);
                tablefill2_2.Rows[0].Cells[i + 3].Style.BackColor = COLOR_LOW;
                marksSum += Convert.ToInt32(tablefill2_2.Rows[0].Cells[i + 3].Value);
                tablefill2_2.Rows[0].Cells[i + 14].Style.BackColor = Color.CornflowerBlue;
                tablefill2_2.Rows[0].Cells[i + 14].ReadOnly = true;
            }

            randmass = new double[(ReqCnt - 1) * 10];

            for (int i = 0; i < randmass.Length; i++)
            {
                randmass[i] = (Convert.ToDouble(rand.Next(0, 100)) / 100) * 0.8;
            }

            int q = 0;

            for (int i = 0; i < ReqCnt - 1; i++)
            {
                tablefill2_2.Rows[i + 1].Cells[13].Style.BackColor = Color.Aquamarine;
                tablefill2_2.Rows[i + 1].Cells[13].ReadOnly = true;
                tablefill2_2.Rows[i + 1].Cells[24].Style.BackColor = Color.Lime;
                tablefill2_2.Rows[i + 1].Cells[24].ReadOnly = true;
                tablefill2_2.Rows[i + 1].Cells[25].Style.BackColor = Color.LimeGreen;
                tablefill2_2.Rows[i + 1].Cells[25].ReadOnly = true;
                tablefill2_2.Rows[i].Cells[26].ReadOnly = true;

                for (int j = 0; j < 10; j++)
                {
                    tablefill2_2.Rows[i + 1].Cells[j + 3].Value = randmass[q];
                    tablefill2_2.Rows[i + 1].Cells[j + 14].ReadOnly = true;
                    q++;
                }
            }

            tablefill2_2.Rows[0].Cells[13].Style.BackColor = Color.CadetBlue;
            tablefill2_2.Rows[0].Cells[13].ReadOnly = true;
            tablefill2_2.Rows[0].Cells[13].Value = marksSum;
            tablefill2_2.Rows[0].Cells[24].Style.BackColor = Color.Lime;
            tablefill2_2.Rows[0].Cells[24].ReadOnly = true;
            tablefill2_2.Rows[0].Cells[25].Style.BackColor = Color.LimeGreen;
            tablefill2_2.Rows[0].Cells[25].ReadOnly = true;
            tablefill2_2.Rows[0].Cells[24].Style.Font = new Font(tablefill2_2.DefaultCellStyle.Font, FontStyle.Bold);
            tablefill2_2.Rows[0].Cells[25].Style.Font = new Font(tablefill2_2.DefaultCellStyle.Font, FontStyle.Bold);

            RecountChangesTable2_2(ReqCnt - 1, ReqPosition, tablefill2_2);
        }

        public void RecountChangesTable2_2(int reqCount, int ReqPosition, DataGridView table2)
        {
            for (int i = 0; i < reqCount; i++)
            {
                table2.Rows[i + 1].Cells[1].Value = Table1_2.Rows[i + ReqPosition + 1].Cells[1].Value;
            }

            for (int i = 0; i < reqCount; i++)
            {
                try
                {
                    if (Convert.ToInt32(table2.Rows[i + 1].Cells[1].Value) != 0 &&
                        Convert.ToInt32(table2.Rows[i + 1].Cells[1].Value) != 1)
                    {
                        MessageBox.Show("Значення можуть бути лише або 1, або 0!", "Увага", MessageBoxButtons.OK);
                        table2.Rows[i + 1].Cells[1].Value = Table1_2.Rows[i + ReqPosition + 1].Cells[1].Value;
                        return;
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show("Значення можуть бути лише цілого типу!", "Увага", MessageBoxButtons.OK);
                    table2.Rows[i + 1].Cells[1].Value = Table1_2.Rows[i + ReqPosition + 1].Cells[1].Value;
                    return;
                }
            }

            int marksSum = 0;

            for (int i = 0; i < 10; i++)
            {
                try
                {
                    marksSum += Convert.ToInt32(table2.Rows[0].Cells[i + 3].Value);
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show("Значення можуть бути лише цілого типу!", "Увага", MessageBoxButtons.OK);
                    table2.Rows[0].Cells[i + 3].Value = marks[i];
                    return;
                }
            }

            table2.Rows[0].Cells[13].Value = marksSum;

            double averagesum = 0;

            for (int i = 0; i < reqCount; i++)
            {

                for (int j = 0; j < 10; j++)
                {
                    try
                    {
                        if (Convert.ToDouble(table2.Rows[i + 1].Cells[j + 3].Value) > 1 ||
                            Convert.ToDouble(table2.Rows[i + 1].Cells[j + 3].Value) < 0)
                        {
                            MessageBox.Show("Значення можуть бути лише в діапазоні від 0 до 1!", "Увага",
                                MessageBoxButtons.OK);
                            table2.Rows[i + 1].Cells[j + 2].Value = randmass[i * 10 + j];
                            return;
                        }

                        averagesum += Convert.ToDouble(table2.Rows[i + 1].Cells[j + 3].Value);
                    }
                    catch (FormatException ex)
                    {
                        Console.WriteLine(ex);
                        MessageBox.Show("Значення можуть бути лише дійсного типу!", "Увага", MessageBoxButtons.OK);
                        table2.Rows[i + 1].Cells[j + 3].Value = randmass[i * 10 + j];
                        return;
                    }
                }

                table2.Rows[i + 1].Cells[13].Value = (averagesum / 10).ToString("0.00");
                averagesum = 0;
            }

            for (int i = 0; i < reqCount; i++)
            {
                for (int j = 0; j < 10; j++)
                {

                    if (Convert.ToInt32(table2.Rows[i + 1].Cells[1].Value) == 0)
                        table2.Rows[i + 1].Cells[j + 14].Value = 0.00;
                    else
                        table2.Rows[i + 1].Cells[j + 14].Value =
                            Convert.ToDouble(table2.Rows[i + 1].Cells[j + 3].Value) *
                            Convert.ToDouble(table2.Rows[0].Cells[j + 3].Value);
                }
            }

            averagesum = 0;
            counts = 0;

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < reqCount; j++)
                {
                    if (Convert.ToDouble(table2.Rows[j + 1].Cells[i + 14].Value) > 0)
                    {
                        counts++;
                        averagesum += Convert.ToDouble(table2.Rows[j + 1].Cells[i + 14].Value);
                    }
                }

                table2.Rows[0].Cells[i + 14].Value =
                    (averagesum / counts / Convert.ToDouble(table2.Rows[0].Cells[i + 3].Value)).ToString("0.00");

            }

            averagesum = 0;

            for (int i = 0; i < reqCount; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    averagesum += Convert.ToDouble(table2.Rows[i + 1].Cells[j + 14].Value);
                }

                table2.Rows[i + 1].Cells[24].Value =
                    (averagesum / Convert.ToDouble(table2.Rows[0].Cells[13].Value) *
                     Convert.ToDouble(table2.Rows[i + 1].Cells[2].Value)).ToString("0.00");
                averagesum = 0;
            }

            double[] reqAddSum = new double[reqCount];

            for (int i = 0; i < reqCount; i++)
            {
                string cell = Convert.ToString(table2.Rows[i + 1].Cells[1].Value);
                if (cell != "0")
                {
                    table2.Rows[i + 1].Cells[25].Value =
                        Convert.ToDouble(table2.Rows[i + 1].Cells[2].Value) +
                        Convert.ToDouble(table2.Rows[i + 1].Cells[24].Value);
                    reqAddSum[i] = Convert.ToDouble(table2.Rows[i + 1].Cells[24].Value);
                }
                else
                {
                    table2.Rows[i + 1].Cells[25].Value = 0.00;
                }
            }

            double max = reqAddSum[0], min = reqAddSum[0];

            for (int i = 1; i < reqAddSum.Length; i++)
            {
                if (reqAddSum[i] == 0.00)
                    continue;
                if (reqAddSum[i] < min)
                    min = reqAddSum[i];
            }

            for (int i = 1; i < reqAddSum.Length; i++)
            {
                if (reqAddSum[i] == 0.00)
                    continue;
                if (reqAddSum[i] > max)
                    max = reqAddSum[i];
            }

            minReq[index] = min;
            maxReq[index] = max;

            index++;

            for (int i = 0; i < reqCount; i++)
            {
                averagesum += Convert.ToDouble(table2.Rows[i + 1].Cells[24].Value);
            }

            table2.Rows[0].Cells[24].Value = averagesum;
            table2.Rows[0].Cells[25].Value =
                Convert.ToDouble(table2.Rows[0].Cells[2].Value) + Convert.ToDouble(table2.Rows[0].Cells[24].Value);
        }

        public void WorkOutPriority()
        {
            double max_Req = maxReq[0];
            double min_Req = minReq[0];

            for (int i = 1; i < maxReq.Length; i++)
            {
                if (maxReq[i] > max_Req)
                    max_Req = maxReq[i];

                if (minReq[i] < min_Req)
                    min_Req = minReq[i];
            }

            minReqValue.Text = min_Req.ToString("00.00");
            maxReqValue.Text = max_Req.ToString("00.00");

            double step = (max_Req - min_Req) / 3;
            bord1Start = min_Req;
            bord1End = min_Req + step;
            lowLevel.Text = bord1Start.ToString("00.00") + " - " + bord1End.ToString("00.00");
            bord2Start = bord1End;
            bord2End = bord2Start + step;
            mediumLevel.Text = bord2Start.ToString("00.00") + " - " + bord2End.ToString("00.00");
            bord3Start = bord2End;
            bord3End = bord3Start + step;
            highLevel.Text = bord3Start.ToString("00.00") + " - " + bord3End.ToString("00.00");
        }

        public void getPriorityForTable2_2(int reqCount, DataGridView table2_2)
        {
            for (int i = 0; i <= reqCount; i++)
            {
                if (Convert.ToString(table2_2.Rows[i].Cells[1].Value) == "0")
                {
                    table2_2.Rows[i].Cells[26].Value = " ";
                    table2_2.Rows[i].Cells[26].Style.BackColor = Color.White;
                    continue;
                }

                if (Convert.ToDouble(table2_2.Rows[i].Cells[24].Value) >= bord1Start - 1 &&
                    Convert.ToDouble(table2_2.Rows[i].Cells[24].Value) <= bord1End)
                {
                    table2_2.Rows[i].Cells[26].Value = "Низький";
                    table2_2.Rows[i].Cells[26].Style.BackColor = COLOR_LOW;
                }
                else if (Convert.ToDouble(table2_2.Rows[i].Cells[24].Value) > bord2Start &&
                         Convert.ToDouble(table2_2.Rows[i].Cells[24].Value) <= bord2End)
                {
                    table2_2.Rows[i].Cells[26].Value = "Середній";
                    table2_2.Rows[i].Cells[26].Style.BackColor = COLOR_MEDIUM;
                }
                else if (Convert.ToDouble(table2_2.Rows[i].Cells[24].Value) > bord3Start &&
                         Convert.ToDouble(table2_2.Rows[i].Cells[24].Value) <= bord3End + 1)
                {
                    table2_2.Rows[i].Cells[26].Value = "Високий";
                    table2_2.Rows[i].Cells[26].Style.BackColor = COLOR_HIGH;
                }
            }
        }

        public void dataForTable_2_Sum(int rowCount)
        {
            for (int i = 0; i < rowCount; i++)
            {
                if (Table2_2_Sum.Rows.Count >= rowCount)
                    break;
                Table2_2_Sum.Rows.Add();
            }

            Table2_2_Sum.Rows[0].Cells[0].Value = "Початкова вартість реалізації проекту, тис. грн";
            Table2_2_Sum.Rows[1].Cells[0].Value = "Кінцева вартість реалізації проекту, тис. грн";
            Table2_2_Sum.Rows[0].Cells[2].Value = Table2_2_1.Rows[0].Cells[2].Value;
            Table2_2_Sum.Rows[0].Cells[3].Value = Table2_2_2.Rows[0].Cells[2].Value;
            Table2_2_Sum.Rows[0].Cells[4].Value = Table2_2_3.Rows[0].Cells[2].Value;
            Table2_2_Sum.Rows[0].Cells[5].Value = Table2_2_4.Rows[0].Cells[2].Value;

            Table2_2_Sum.Rows[1].Cells[2].Value = Table2_2_1.Rows[0].Cells[25].Value;
            Table2_2_Sum.Rows[1].Cells[3].Value = Table2_2_2.Rows[0].Cells[25].Value;
            Table2_2_Sum.Rows[1].Cells[4].Value = Table2_2_3.Rows[0].Cells[25].Value;
            Table2_2_Sum.Rows[1].Cells[5].Value = Table2_2_4.Rows[0].Cells[25].Value;

            double projectSumStart = 0;
            double projectSumEnd = 0;

            for (int i = 0; i < Table2_2_Sum.ColumnCount - 2; i++)
            {
                Table2_2_Sum.Rows[0].Cells[i + 1].ReadOnly = true;
                Table2_2_Sum.Rows[1].Cells[i + 1].ReadOnly = true;
                Table2_2_Sum.Rows[0].Cells[i + 2].Style.BackColor = Color.Chocolate;
                projectSumStart += Convert.ToDouble(Table2_2_Sum.Rows[0].Cells[i + 2].Value);
                Table2_2_Sum.Rows[1].Cells[i + 2].Style.BackColor = Color.Chartreuse;
                projectSumEnd += Convert.ToDouble(Table2_2_Sum.Rows[1].Cells[i + 2].Value);
            }

            Table2_2_Sum.Rows[0].Cells[1].ReadOnly = true;
            Table2_2_Sum.Rows[1].Cells[1].ReadOnly = true;
            Table2_2_Sum.Rows[0].Cells[1].Style.Font = new Font(Table2_2_Sum.DefaultCellStyle.Font, FontStyle.Bold);
            Table2_2_Sum.Rows[0].Cells[1].Style.BackColor = Color.Chocolate;
            Table2_2_Sum.Rows[0].Cells[1].Value = projectSumStart.ToString("#######.##");
            Table2_2_Sum.Rows[1].Cells[1].Style.Font = new Font(Table2_2_Sum.DefaultCellStyle.Font, FontStyle.Bold);
            Table2_2_Sum.Rows[1].Cells[1].Style.BackColor = Color.Chartreuse;
            Table2_2_Sum.Rows[1].Cells[1].Value = projectSumEnd.ToString("#######.##");
            projectSumStart = 0;
            projectSumEnd = 0;
        }

        private void buttonTable2_2_1_Click(object sender, EventArgs e)
        {
            index = 0;
            RecountChangesTable2_2(11, 0, Table2_2_1);
            WorkOutPriority();
            groupBoxTable2_2_1.Visible = true;
            groupBoxTable2_2_2.Visible = false;
            groupBoxTable2_2_3.Visible = false;
            groupBoxTable2_2_4.Visible = false;
            groupBoxTable2_2_Sum.Visible = false;
        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void Table2_1_4_Click(object sender, EventArgs e)
        {
           
        }

        private void buttonTable2_2_2_Click(object sender, EventArgs e)
        {
            index = 1;
            RecountChangesTable2_2(7, 12, Table2_2_2);
            groupBoxTable2_2_1.Visible = false;
            groupBoxTable2_2_2.Visible = true;
            groupBoxTable2_2_3.Visible = false;
            groupBoxTable2_2_4.Visible = false;
            groupBoxTable2_2_Sum.Visible = false;
        }

        private void buttonTable2_2_3_Click(object sender, EventArgs e)
        {
            index = 2;
            RecountChangesTable2_2(9, 20, Table2_2_3);
            WorkOutPriority();
            groupBoxTable2_2_1.Visible = false;
            groupBoxTable2_2_2.Visible = false;
            groupBoxTable2_2_3.Visible = true;
            groupBoxTable2_2_4.Visible = false;
            groupBoxTable2_2_Sum.Visible = false;
        }

        private void buttonTable2_2_4_Click(object sender, EventArgs e)
        {
            index = 3;
            RecountChangesTable2_2(14, 30, Table2_2_4);
            WorkOutPriority();
            groupBoxTable2_2_1.Visible = false;
            groupBoxTable2_2_2.Visible = false;
            groupBoxTable2_2_3.Visible = false;
            groupBoxTable2_2_4.Visible = true;
            groupBoxTable2_2_Sum.Visible = false;
        }

        private void Table2_2_1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void Table2_2_2_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void Table2_2_3_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
          
        }

        private void Table2_2_4_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void buttonTableSum_Click(object sender, EventArgs e)
        {
            groupBoxTable2_2_1.Visible = false;
            groupBoxTable2_2_2.Visible = false;
            groupBoxTable2_2_3.Visible = false;
            groupBoxTable2_2_4.Visible = false;
            groupBoxTable2_2_Sum.Visible = true;
        }

        public void DataForTable3_1()
        {
            int n = 20;
            for (int i = 0; i < n; i++)
                Table3_1.Rows.Add();

            Table3_1.Rows[0].Cells[0].Value = "Назва заходів";
            Table3_1.Rows[0].Cells[0].Style.Font = new Font(Table3_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table3_1.Rows[1].Cells[0].Value = "1. Попереднє навчання членів проектного колективу";
            Table3_1.Rows[2].Cells[0].Value = "2. Узгодження детального переліку вимог до ПЗ із замовником";
            Table3_1.Rows[3].Cells[0].Value = "3. Внесення узгодженого переліку вимог до ПЗ замовника в договір";
            Table3_1.Rows[4].Cells[0].Value =
                "4. Точне слідування вимогам замовника з узгодженого переліку вимог до ПЗ";
            Table3_1.Rows[5].Cells[0].Value = "5. Попередні дослідження ринку";
            Table3_1.Rows[6].Cells[0].Value =
                "6. Експертна оцінка програмного проекту досвідченим стороннім консультантом";
            Table3_1.Rows[7].Cells[0].Value = "7. Консультації досвідченого стороннього консультанта";
            Table3_1.Rows[8].Cells[0].Value = "8. Тренінг з вивчення необхідних інструментів розроблення ПЗ";
            Table3_1.Rows[9].Cells[0].Value = "9. Укладання договору страхування";
            Table3_1.Rows[10].Cells[0].Value =
                "10. Використання \"шаблонних\" рішень з вдалих попередніх проектів при управлінні програмним проектом";
            Table3_1.Rows[11].Cells[0].Value =
                "11. Підготовка документів, які показують важливість даного проекту для досягнення фінансових цілей компанії-розробника";
            Table3_1.Rows[12].Cells[0].Value =
                "12. Реорганізація роботи проектного колективу так, щоб обов\'язки та робота членів колективу перекривали один одного";
            Table3_1.Rows[13].Cells[0].Value =
                "13. Придбання (замовлення) частини компонент розроблюваного ПЗ";
            Table3_1.Rows[14].Cells[0].Value =
                "14. Заміна потенційно дефектних компонент розроблюваного ПЗ придбаними компонентами, які гарантують якість виконання роботи";
            Table3_1.Rows[15].Cells[0].Value =
                "15. Придбання більш продуктивної бази даних";
            Table3_1.Rows[16].Cells[0].Value =
                "16. Використання генератора програмного коду";
            Table3_1.Rows[17].Cells[0].Value =
                "17. Реорганізація роботи проектного колективу залежно від рівня труднощів виконання завдань та професійних рівнів розробників";
            Table3_1.Rows[18].Cells[0].Value =
                "18. Повторне використання придатних компонент ПЗ, які були розроблені для інших програмних проектів";
            Table3_1.Rows[19].Cells[0].Value = "19. Аналіз доцільності розроблення даного ПЗ";
            Table3_1.Rows[0].Cells[1].Style.Font = new Font(Table3_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table3_1.Rows[0].Cells[2].Style.Font = new Font(Table3_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table3_1.Rows[0].Cells[3].Style.Font = new Font(Table3_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table3_1.Rows[0].Cells[4].Style.Font = new Font(Table3_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table3_1.Rows[0].Cells[1].Value = "Пом'якшення";
            Table3_1.Rows[0].Cells[2].Value = "Прийняття";
            Table3_1.Rows[0].Cells[3].Value = "Ухилення";
            Table3_1.Rows[0].Cells[4].Value = "Передача";

            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < Table3_1.ColumnCount - 1; j++)
                {
                    Table3_1.Rows[i + 1].Cells[j + 1].Value = "0";
                }
            }

            lowLevelBefore.Text = lowLevel.Text;
            MediumLevelBefore.Text = mediumLevel.Text;
            HighLevelBefore.Text = highLevel.Text;
        }


        public void DataForTable4_1()
        {
            int n = 45;
            for (int i = 0; i < n; i++)
                Table4_1.Rows.Add();

            for (int i = 0; i < n; i++)
            {
                Table4_1.Rows[i].Cells[0].ReadOnly = true;
                Table4_1.Rows[i].Cells[1].ReadOnly = true;
                Table4_1.Rows[i].Cells[2].ReadOnly = true;
                Table4_1.Rows[i].Cells[3].ReadOnly = true;
            }

            Table4_1.Rows[0].Cells[0].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table4_1.Rows[0].Cells[1].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table4_1.Rows[0].Cells[2].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table4_1.Rows[0].Cells[3].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);

            for (int i = 0; i < 12; i++)
            {
                Table4_1.Rows[i].Cells[0].Value = Table2_2_1.Rows[i].Cells[0].Value;
                Table4_1.Rows[i].Cells[1].Value = Table2_2_1.Rows[i].Cells[1].Value;
                Table4_1.Rows[i].Cells[2].Value = Table2_2_1.Rows[i].Cells[24].Value;
                Table4_1.Rows[i].Cells[3].Value = Table2_2_1.Rows[i].Cells[26].Value;
                Table4_1.Rows[i].Cells[3].Style.BackColor = Table2_2_1.Rows[i].Cells[26].Style.BackColor;
            }

            Table4_1.Rows[12].Cells[0].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table4_1.Rows[12].Cells[1].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table4_1.Rows[12].Cells[2].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table4_1.Rows[12].Cells[3].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);

            for (int i = 0; i < 8; i++)
            {
                Table4_1.Rows[i + 12].Cells[0].Value = Table2_2_2.Rows[i].Cells[0].Value;
                Table4_1.Rows[i + 12].Cells[1].Value = Table2_2_2.Rows[i].Cells[1].Value;
                Table4_1.Rows[i + 12].Cells[2].Value = Table2_2_2.Rows[i].Cells[24].Value;
                Table4_1.Rows[i + 12].Cells[3].Value = Table2_2_2.Rows[i].Cells[26].Value;
                Table4_1.Rows[i + 12].Cells[3].Style.BackColor = Table2_2_2.Rows[i].Cells[26].Style.BackColor;
            }

            Table4_1.Rows[20].Cells[0].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table4_1.Rows[20].Cells[1].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table4_1.Rows[20].Cells[2].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table4_1.Rows[20].Cells[3].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);

            for (int i = 0; i < 10; i++)
            {
                Table4_1.Rows[i + 20].Cells[0].Value = Table2_2_3.Rows[i].Cells[0].Value;
                Table4_1.Rows[i + 20].Cells[1].Value = Table2_2_3.Rows[i].Cells[1].Value;
                Table4_1.Rows[i + 20].Cells[2].Value = Table2_2_3.Rows[i].Cells[24].Value;
                Table4_1.Rows[i + 20].Cells[3].Value = Table2_2_3.Rows[i].Cells[26].Value;
                Table4_1.Rows[i + 20].Cells[3].Style.BackColor = Table2_2_3.Rows[i].Cells[26].Style.BackColor;
            }

            Table4_1.Rows[30].Cells[0].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table4_1.Rows[30].Cells[1].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table4_1.Rows[30].Cells[2].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);
            Table4_1.Rows[30].Cells[3].Style.Font = new Font(Table4_1.DefaultCellStyle.Font, FontStyle.Bold);

            for (int i = 0; i < 15; i++)
            {
                Table4_1.Rows[i + 30].Cells[0].Value = Table2_2_4.Rows[i].Cells[0].Value;
                Table4_1.Rows[i + 30].Cells[1].Value = Table2_2_4.Rows[i].Cells[1].Value;
                Table4_1.Rows[i + 30].Cells[2].Value = Table2_2_4.Rows[i].Cells[24].Value;
                Table4_1.Rows[i + 30].Cells[3].Value = Table2_2_4.Rows[i].Cells[26].Value;
                Table4_1.Rows[i + 30].Cells[3].Style.BackColor = Table2_2_4.Rows[i].Cells[26].Style.BackColor;
            }

        }

        public void CountValueForReduceTable4_1()
        {
            int counts = 0;
            int tempcount = 0;
            for (int i = 0; i < Table3_1.RowCount - 1; i++)
            {
                tempcount = 0;
                for (int j = 0; j < Table3_1.ColumnCount - 1; j++)
                {
                    try
                    {
                        if (Convert.ToInt32(Table3_1.Rows[i + 1].Cells[j + 1].Value) != 0 &&
                            Convert.ToInt32(Table3_1.Rows[i + 1].Cells[j + 1].Value) != 1)
                        {
                            MessageBox.Show("Значення можуть бути лише або 1, або 0!", "Увага", MessageBoxButtons.OK);
                            Table3_1.Rows[i + 1].Cells[j + 1].Value = "0";
                            return;
                        }
                    }
                    catch (FormatException ex)
                    {
                        Console.WriteLine(ex);
                        MessageBox.Show("Значення можуть бути лише цілого типу!", "Увага", MessageBoxButtons.OK);
                        Table3_1.Rows[i + 1].Cells[j + 1].Value = "0";
                        return;
                    }

                    if (Convert.ToInt32(Table3_1.Rows[i + 1].Cells[j + 1].Value) == 1)
                    {
                        counts++;
                        tempcount++;
                    }

                    if (tempcount > 1)
                    {
                        MessageBox.Show("Лише один напрям може бути вибраний для відповідного заходу!", "Увага",
                            MessageBoxButtons.OK);
                        Table3_1.Rows[i + 1].Cells[j + 1].Value = "0";
                        return;
                    }
                }
            }

            reduceKoef = Math.Pow(0.97, counts);
            counts = 0;
        }


        public void RecountDataTable4_1()
        {
            int n = 45;
            for (int i = 0; i < 12; i++)
            {
                Table4_1.Rows[i].Cells[1].Value = Table2_2_1.Rows[i].Cells[1].Value;
                Table4_1.Rows[i].Cells[2].Value = Table2_2_1.Rows[i].Cells[24].Value;
                Table4_1.Rows[i].Cells[3].Value = Table2_2_1.Rows[i].Cells[26].Value;
                Table4_1.Rows[i].Cells[3].Style.BackColor = Table2_2_1.Rows[i].Cells[26].Style.BackColor;
            }

            for (int i = 0; i < 8; i++)
            {
                Table4_1.Rows[i + 12].Cells[1].Value = Table2_2_2.Rows[i].Cells[1].Value;
                Table4_1.Rows[i + 12].Cells[2].Value = Table2_2_2.Rows[i].Cells[24].Value;
                Table4_1.Rows[i + 12].Cells[3].Value = Table2_2_2.Rows[i].Cells[26].Value;
                Table4_1.Rows[i + 12].Cells[3].Style.BackColor = Table2_2_2.Rows[i].Cells[26].Style.BackColor;
            }

            for (int i = 0; i < 10; i++)
            {
                Table4_1.Rows[i + 20].Cells[1].Value = Table2_2_3.Rows[i].Cells[1].Value;
                Table4_1.Rows[i + 20].Cells[2].Value = Table2_2_3.Rows[i].Cells[24].Value;
                Table4_1.Rows[i + 20].Cells[3].Value = Table2_2_3.Rows[i].Cells[26].Value;
                Table4_1.Rows[i + 20].Cells[3].Style.BackColor = Table2_2_3.Rows[i].Cells[26].Style.BackColor;
            }

            for (int i = 0; i < 15; i++)
            {
                Table4_1.Rows[i + 30].Cells[1].Value = Table2_2_4.Rows[i].Cells[1].Value;
                Table4_1.Rows[i + 30].Cells[2].Value = Table2_2_4.Rows[i].Cells[24].Value;
                Table4_1.Rows[i + 30].Cells[3].Value = Table2_2_4.Rows[i].Cells[26].Value;
                Table4_1.Rows[i + 30].Cells[3].Style.BackColor = Table2_2_4.Rows[i].Cells[26].Style.BackColor;
            }
        }

        public void RecountChangesTable4_1()
        {
            flag = true;
            int n = 45;
            for (int i = 0; i < 12; i++)
            {
                Table4_1.Rows[i].Cells[1].Value = Table2_2_1.Rows[i].Cells[1].Value;
                Table4_1.Rows[i].Cells[2].Value = Table2_2_1.Rows[i].Cells[24].Value;
                Table4_1.Rows[i].Cells[3].Value = Table2_2_1.Rows[i].Cells[26].Value;
                Table4_1.Rows[i].Cells[3].Style.BackColor = Table2_2_1.Rows[i].Cells[26].Style.BackColor;
            }

            for (int i = 0; i < 8; i++)
            {
                Table4_1.Rows[i + 12].Cells[1].Value = Table2_2_2.Rows[i].Cells[1].Value;
                Table4_1.Rows[i + 12].Cells[2].Value = Table2_2_2.Rows[i].Cells[24].Value;
                Table4_1.Rows[i + 12].Cells[3].Value = Table2_2_2.Rows[i].Cells[26].Value;
                Table4_1.Rows[i + 12].Cells[3].Style.BackColor = Table2_2_2.Rows[i].Cells[26].Style.BackColor;
            }

            for (int i = 0; i < 10; i++)
            {
                Table4_1.Rows[i + 20].Cells[1].Value = Table2_2_3.Rows[i].Cells[1].Value;
                Table4_1.Rows[i + 20].Cells[2].Value = Table2_2_3.Rows[i].Cells[24].Value;
                Table4_1.Rows[i + 20].Cells[3].Value = Table2_2_3.Rows[i].Cells[26].Value;
                Table4_1.Rows[i + 20].Cells[3].Style.BackColor = Table2_2_3.Rows[i].Cells[26].Style.BackColor;
            }

            for (int i = 0; i < 15; i++)
            {
                Table4_1.Rows[i + 30].Cells[1].Value = Table2_2_4.Rows[i].Cells[1].Value;
                Table4_1.Rows[i + 30].Cells[2].Value = Table2_2_4.Rows[i].Cells[24].Value;
                Table4_1.Rows[i + 30].Cells[3].Value = Table2_2_4.Rows[i].Cells[26].Value;
                Table4_1.Rows[i + 30].Cells[3].Style.BackColor = Table2_2_4.Rows[i].Cells[26].Style.BackColor;
            }

            for (int i = 0; i < n; i++)
            {
                if (i == 0 || i == 12 || i == 20 || i == 30)
                    continue;

                if (Convert.ToInt32(Table4_1.Rows[i].Cells[1].Value) == 0)
                {
                    continue;
                }

                Table4_1.Rows[i].Cells[2].Value =
                    (Convert.ToDouble(Table4_1.Rows[i].Cells[2].Value) * reduceKoef).ToString("####.##");


                string nss = Convert.ToString(Table4_1.Rows[i].Cells[2].Value);
                if (nss == "")
                {
                    continue;
                }

                if (Convert.ToDouble(Table4_1.Rows[i].Cells[2].Value) <= bord1End)
                {
                    Table4_1.Rows[i].Cells[3].Value = "Низький";
                    Table4_1.Rows[i].Cells[3].Style.BackColor = COLOR_LOW;
                }
                else if (Convert.ToDouble(Table4_1.Rows[i].Cells[2].Value) > bord2Start &&
                         Convert.ToDouble(Table4_1.Rows[i].Cells[2].Value) <= bord2End)
                {
                    Table4_1.Rows[i].Cells[3].Value = "Середній";
                    Table4_1.Rows[i].Cells[3].Style.BackColor = COLOR_MEDIUM;
                }
                else if (Convert.ToDouble(Table4_1.Rows[i].Cells[2].Value) > bord3Start &&
                         Convert.ToDouble(Table4_1.Rows[i].Cells[2].Value) <= bord3End + 1)
                {
                    Table4_1.Rows[i].Cells[3].Value = "Високий";
                    Table4_1.Rows[i].Cells[3].Style.BackColor = COLOR_HIGH;
                }
            }

            double reduceSum = 0;

            for (int i = 0; i < 11; i++)
            {
                if (Convert.ToString(Table4_1.Rows[i+1].Cells[2].Value)== "")
                {
                    continue;
                }
                reduceSum += Convert.ToDouble(Table4_1.Rows[i + 1].Cells[2].Value);
            }

            Table4_1.Rows[0].Cells[2].Value = reduceSum;
            reduceSum = 0;

            for (int i = 0; i < 7; i++)
            {
                if (Convert.ToString(Table4_1.Rows[i + 1].Cells[2].Value) == "")
                {
                    continue;
                }
                reduceSum += Convert.ToDouble(Table4_1.Rows[i + 13].Cells[2].Value);
            }

            Table4_1.Rows[12].Cells[2].Value = reduceSum;
            reduceSum = 0;

            for (int i = 0; i < 9; i++)
            {
                if (Convert.ToString(Table4_1.Rows[i + 1].Cells[2].Value) == "")
                {
                    continue;
                }
                reduceSum += Convert.ToDouble(Table4_1.Rows[i + 21].Cells[2].Value);
            }

            Table4_1.Rows[20].Cells[2].Value = reduceSum;
            reduceSum = 0;

            for (int i = 0; i < 14; i++)
            {
                if (Convert.ToString(Table4_1.Rows[i + 1].Cells[2].Value) == "")
                {
                    continue;
                }
                reduceSum += Convert.ToDouble(Table4_1.Rows[i + 31].Cells[2].Value);
            }

            Table4_1.Rows[30].Cells[2].Value = reduceSum;
            reduceSum = 0;

        }

        private void Table3_1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void вихідToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}