﻿namespace Lab5Analysis
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBoxTable1_1 = new System.Windows.Forms.GroupBox();
            this.Table1_1 = new System.Windows.Forms.DataGridView();
            this.RequireText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValueReq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PercentSum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBoxTable1_2 = new System.Windows.Forms.GroupBox();
            this.Table1_2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBoxTable2 = new System.Windows.Forms.GroupBox();
            this.genRyskLevel = new System.Windows.Forms.TextBox();
            this.genRyskValue = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBoxTable2_1_4 = new System.Windows.Forms.GroupBox();
            this.Table2_1_4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn65 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn66 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn67 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn68 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn69 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn70 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn71 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn72 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn73 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn74 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn75 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn76 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn77 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn78 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn79 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn80 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxTable2_1_3 = new System.Windows.Forms.GroupBox();
            this.Table2_1_3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxTable2_1_2 = new System.Windows.Forms.GroupBox();
            this.Table2_1_2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxTable2_1_1 = new System.Windows.Forms.GroupBox();
            this.Table2_1_1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonTable2_1_4 = new System.Windows.Forms.Button();
            this.buttonTable2_1_3 = new System.Windows.Forms.Button();
            this.buttonTable2_1_2 = new System.Windows.Forms.Button();
            this.buttonTable2_1_1 = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBoxTable2_2 = new System.Windows.Forms.GroupBox();
            this.groupBoxTable2_2_Sum = new System.Windows.Forms.GroupBox();
            this.Table2_2_Sum = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn183 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn184 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn181 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn182 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minReqValue = new System.Windows.Forms.Label();
            this.maxReqValue = new System.Windows.Forms.Label();
            this.highLevel = new System.Windows.Forms.Label();
            this.mediumLevel = new System.Windows.Forms.Label();
            this.lowLevel = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.buttonTableSum = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.groupBoxTable2_2_4 = new System.Windows.Forms.GroupBox();
            this.Table2_2_4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn81 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn82 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn83 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn84 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn85 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn86 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn87 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn88 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn89 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn90 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn91 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn92 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn93 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn94 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn95 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn96 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn97 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn98 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn99 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn100 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn101 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn102 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn103 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn104 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn105 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxTable2_2_3 = new System.Windows.Forms.GroupBox();
            this.Table2_2_3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn106 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn107 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn108 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn109 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn110 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn111 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn112 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn113 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn114 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn115 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn116 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn117 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn118 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn119 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn120 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn121 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn122 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn123 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn124 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn125 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn126 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn127 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn128 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn129 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn130 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxTable2_2_2 = new System.Windows.Forms.GroupBox();
            this.Table2_2_2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn131 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn132 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn133 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn134 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn135 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn136 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn137 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn138 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn139 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn140 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn141 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn142 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn143 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn144 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn145 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn146 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn147 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn148 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn149 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn150 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn151 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn152 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn153 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn154 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn155 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxTable2_2_1 = new System.Windows.Forms.GroupBox();
            this.Table2_2_1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn156 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn157 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn158 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn159 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn160 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn161 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn162 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn163 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn164 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn165 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn166 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn167 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn168 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn169 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn170 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn171 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn172 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn173 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn174 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn175 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn176 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn177 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn178 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn179 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn180 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.buttonTable2_2_4 = new System.Windows.Forms.Button();
            this.buttonTable2_2_3 = new System.Windows.Forms.Button();
            this.buttonTable2_2_2 = new System.Windows.Forms.Button();
            this.buttonTable2_2_1 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBoxTable3_1 = new System.Windows.Forms.GroupBox();
            this.Table3_1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn274 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn275 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn276 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn277 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label42 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBoxTable4_1 = new System.Windows.Forms.GroupBox();
            this.lowLevelBefore = new System.Windows.Forms.Label();
            this.MediumLevelBefore = new System.Windows.Forms.Label();
            this.HighLevelBefore = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.Table4_1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn185 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn186 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn187 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn188 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.tabControl1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBoxTable1_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table1_1)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.groupBoxTable1_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table1_2)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBoxTable2.SuspendLayout();
            this.groupBoxTable2_1_4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table2_1_4)).BeginInit();
            this.groupBoxTable2_1_3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table2_1_3)).BeginInit();
            this.groupBoxTable2_1_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table2_1_2)).BeginInit();
            this.groupBoxTable2_1_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table2_1_1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBoxTable2_2.SuspendLayout();
            this.groupBoxTable2_2_Sum.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table2_2_Sum)).BeginInit();
            this.groupBoxTable2_2_4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table2_2_4)).BeginInit();
            this.groupBoxTable2_2_3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table2_2_3)).BeginInit();
            this.groupBoxTable2_2_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table2_2_2)).BeginInit();
            this.groupBoxTable2_2_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table2_2_1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBoxTable3_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table3_1)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.groupBoxTable4_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table4_1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(0, -1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1579, 800);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.groupBoxTable1_1);
            this.tabPage6.Location = new System.Drawing.Point(4, 25);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1571, 771);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Етап 1.1";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBoxTable1_1
            // 
            this.groupBoxTable1_1.BackColor = System.Drawing.Color.PapayaWhip;
            this.groupBoxTable1_1.Controls.Add(this.Table1_1);
            this.groupBoxTable1_1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxTable1_1.Location = new System.Drawing.Point(3, 3);
            this.groupBoxTable1_1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable1_1.Name = "groupBoxTable1_1";
            this.groupBoxTable1_1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable1_1.Size = new System.Drawing.Size(1579, 764);
            this.groupBoxTable1_1.TabIndex = 4;
            this.groupBoxTable1_1.TabStop = false;
            this.groupBoxTable1_1.Text = "Визначення можливих джерел появи ризиків";
            this.groupBoxTable1_1.Visible = false;
            // 
            // Table1_1
            // 
            this.Table1_1.AllowUserToAddRows = false;
            this.Table1_1.AllowUserToDeleteRows = false;
            this.Table1_1.AllowUserToResizeRows = false;
            this.Table1_1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Table1_1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Table1_1.BackgroundColor = System.Drawing.Color.PapayaWhip;
            this.Table1_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Table1_1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table1_1.ColumnHeadersVisible = false;
            this.Table1_1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RequireText,
            this.ValueReq,
            this.PercentSum});
            this.Table1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Table1_1.Location = new System.Drawing.Point(17, 31);
            this.Table1_1.Margin = new System.Windows.Forms.Padding(4);
            this.Table1_1.Name = "Table1_1";
            this.Table1_1.RowHeadersVisible = false;
            this.Table1_1.RowHeadersWidth = 51;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Table1_1.RowsDefaultCellStyle = dataGridViewCellStyle27;
            this.Table1_1.Size = new System.Drawing.Size(1544, 700);
            this.Table1_1.TabIndex = 2;
            this.Table1_1.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.Table1_1_CellEnter);
            // 
            // RequireText
            // 
            this.RequireText.HeaderText = "RequireText";
            this.RequireText.MinimumWidth = 6;
            this.RequireText.Name = "RequireText";
            // 
            // ValueReq
            // 
            this.ValueReq.HeaderText = "ValueReq";
            this.ValueReq.MinimumWidth = 6;
            this.ValueReq.Name = "ValueReq";
            // 
            // PercentSum
            // 
            this.PercentSum.HeaderText = "PercentSum";
            this.PercentSum.MinimumWidth = 6;
            this.PercentSum.Name = "PercentSum";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBoxTable1_2);
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1571, 771);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Етап 1.2";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBoxTable1_2
            // 
            this.groupBoxTable1_2.BackColor = System.Drawing.Color.PapayaWhip;
            this.groupBoxTable1_2.Controls.Add(this.Table1_2);
            this.groupBoxTable1_2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxTable1_2.Location = new System.Drawing.Point(3, 3);
            this.groupBoxTable1_2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable1_2.Name = "groupBoxTable1_2";
            this.groupBoxTable1_2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable1_2.Size = new System.Drawing.Size(1576, 763);
            this.groupBoxTable1_2.TabIndex = 5;
            this.groupBoxTable1_2.TabStop = false;
            this.groupBoxTable1_2.Text = "Ідентифікація потенційних ризикових подій";
            this.groupBoxTable1_2.Visible = false;
            // 
            // Table1_2
            // 
            this.Table1_2.AllowUserToAddRows = false;
            this.Table1_2.AllowUserToDeleteRows = false;
            this.Table1_2.AllowUserToResizeRows = false;
            this.Table1_2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Table1_2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Table1_2.BackgroundColor = System.Drawing.Color.PapayaWhip;
            this.Table1_2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Table1_2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table1_2.ColumnHeadersVisible = false;
            this.Table1_2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.Table1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Table1_2.Location = new System.Drawing.Point(17, 31);
            this.Table1_2.Margin = new System.Windows.Forms.Padding(4);
            this.Table1_2.Name = "Table1_2";
            this.Table1_2.RowHeadersVisible = false;
            this.Table1_2.RowHeadersWidth = 51;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Table1_2.RowsDefaultCellStyle = dataGridViewCellStyle28;
            this.Table1_2.Size = new System.Drawing.Size(1544, 700);
            this.Table1_2.TabIndex = 2;
            this.Table1_2.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.Table1_2_CellEnter);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "RequireText";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "ValueReq";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "PercentSum";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBoxTable2);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1571, 771);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Етап 2.1";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBoxTable2
            // 
            this.groupBoxTable2.BackColor = System.Drawing.Color.PapayaWhip;
            this.groupBoxTable2.Controls.Add(this.panel1);
            this.groupBoxTable2.Controls.Add(this.groupBoxTable2_1_4);
            this.groupBoxTable2.Controls.Add(this.genRyskLevel);
            this.groupBoxTable2.Controls.Add(this.genRyskValue);
            this.groupBoxTable2.Controls.Add(this.label10);
            this.groupBoxTable2.Controls.Add(this.groupBoxTable2_1_3);
            this.groupBoxTable2.Controls.Add(this.groupBoxTable2_1_2);
            this.groupBoxTable2.Controls.Add(this.groupBoxTable2_1_1);
            this.groupBoxTable2.Controls.Add(this.label6);
            this.groupBoxTable2.Controls.Add(this.label2);
            this.groupBoxTable2.Controls.Add(this.buttonTable2_1_4);
            this.groupBoxTable2.Controls.Add(this.buttonTable2_1_3);
            this.groupBoxTable2.Controls.Add(this.buttonTable2_1_2);
            this.groupBoxTable2.Controls.Add(this.buttonTable2_1_1);
            this.groupBoxTable2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxTable2.Location = new System.Drawing.Point(3, 3);
            this.groupBoxTable2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2.Name = "groupBoxTable2";
            this.groupBoxTable2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2.Size = new System.Drawing.Size(1576, 763);
            this.groupBoxTable2.TabIndex = 6;
            this.groupBoxTable2.TabStop = false;
            this.groupBoxTable2.Text = "Визначення ймовірності настання ризикових подій";
            this.groupBoxTable2.Visible = false;
            // 
            // genRyskLevel
            // 
            this.genRyskLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.genRyskLevel.Location = new System.Drawing.Point(713, 55);
            this.genRyskLevel.Margin = new System.Windows.Forms.Padding(4);
            this.genRyskLevel.Name = "genRyskLevel";
            this.genRyskLevel.ReadOnly = true;
            this.genRyskLevel.Size = new System.Drawing.Size(133, 27);
            this.genRyskLevel.TabIndex = 30;
            this.genRyskLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // genRyskValue
            // 
            this.genRyskValue.AutoSize = true;
            this.genRyskValue.Location = new System.Drawing.Point(974, 26);
            this.genRyskValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.genRyskValue.Name = "genRyskValue";
            this.genRyskValue.Size = new System.Drawing.Size(70, 21);
            this.genRyskValue.TabIndex = 29;
            this.genRyskValue.Text = "GenRysk";
            this.genRyskValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.PapayaWhip;
            this.label10.Location = new System.Drawing.Point(497, 26);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(453, 21);
            this.label10.TabIndex = 28;
            this.label10.Text = "Загальне значення ймовірності виникнення ризикових подій:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBoxTable2_1_4
            // 
            this.groupBoxTable2_1_4.Controls.Add(this.Table2_1_4);
            this.groupBoxTable2_1_4.Location = new System.Drawing.Point(210, 100);
            this.groupBoxTable2_1_4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_1_4.Name = "groupBoxTable2_1_4";
            this.groupBoxTable2_1_4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_1_4.Size = new System.Drawing.Size(1093, 650);
            this.groupBoxTable2_1_4.TabIndex = 26;
            this.groupBoxTable2_1_4.TabStop = false;
            this.groupBoxTable2_1_4.Text = "Множина настання ризикових подій реалізації процесу управління програмним проекто" +
    "м";
            this.groupBoxTable2_1_4.Visible = false;
            // 
            // Table2_1_4
            // 
            this.Table2_1_4.AllowUserToAddRows = false;
            this.Table2_1_4.AllowUserToDeleteRows = false;
            this.Table2_1_4.AllowUserToResizeRows = false;
            this.Table2_1_4.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Table2_1_4.BackgroundColor = System.Drawing.Color.PapayaWhip;
            this.Table2_1_4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Table2_1_4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table2_1_4.ColumnHeadersVisible = false;
            this.Table2_1_4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn56,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewTextBoxColumn59,
            this.dataGridViewTextBoxColumn60,
            this.dataGridViewTextBoxColumn61,
            this.dataGridViewTextBoxColumn62,
            this.dataGridViewTextBoxColumn63,
            this.dataGridViewTextBoxColumn64,
            this.dataGridViewTextBoxColumn65,
            this.dataGridViewTextBoxColumn66,
            this.dataGridViewTextBoxColumn67,
            this.dataGridViewTextBoxColumn68,
            this.dataGridViewTextBoxColumn69,
            this.dataGridViewTextBoxColumn70,
            this.dataGridViewTextBoxColumn71,
            this.dataGridViewTextBoxColumn72,
            this.dataGridViewTextBoxColumn73,
            this.dataGridViewTextBoxColumn74,
            this.dataGridViewTextBoxColumn75,
            this.dataGridViewTextBoxColumn76,
            this.dataGridViewTextBoxColumn77,
            this.dataGridViewTextBoxColumn78,
            this.dataGridViewTextBoxColumn79,
            this.dataGridViewTextBoxColumn80});
            this.Table2_1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Table2_1_4.Location = new System.Drawing.Point(20, 23);
            this.Table2_1_4.Margin = new System.Windows.Forms.Padding(4);
            this.Table2_1_4.Name = "Table2_1_4";
            this.Table2_1_4.RowHeadersVisible = false;
            this.Table2_1_4.RowHeadersWidth = 51;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Table2_1_4.RowsDefaultCellStyle = dataGridViewCellStyle29;
            this.Table2_1_4.Size = new System.Drawing.Size(1076, 630);
            this.Table2_1_4.TabIndex = 2;
            this.Table2_1_4.Click += new System.EventHandler(this.Table2_1_4_Click);
            // 
            // dataGridViewTextBoxColumn56
            // 
            this.dataGridViewTextBoxColumn56.HeaderText = "RequireText";
            this.dataGridViewTextBoxColumn56.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            this.dataGridViewTextBoxColumn56.Width = 300;
            // 
            // dataGridViewTextBoxColumn57
            // 
            this.dataGridViewTextBoxColumn57.HeaderText = "ValueReq";
            this.dataGridViewTextBoxColumn57.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            this.dataGridViewTextBoxColumn57.Width = 50;
            // 
            // dataGridViewTextBoxColumn58
            // 
            this.dataGridViewTextBoxColumn58.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn58.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            this.dataGridViewTextBoxColumn58.Width = 50;
            // 
            // dataGridViewTextBoxColumn59
            // 
            this.dataGridViewTextBoxColumn59.HeaderText = "Column2";
            this.dataGridViewTextBoxColumn59.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn59.Name = "dataGridViewTextBoxColumn59";
            this.dataGridViewTextBoxColumn59.Width = 50;
            // 
            // dataGridViewTextBoxColumn60
            // 
            this.dataGridViewTextBoxColumn60.HeaderText = "Column3";
            this.dataGridViewTextBoxColumn60.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            this.dataGridViewTextBoxColumn60.Width = 50;
            // 
            // dataGridViewTextBoxColumn61
            // 
            this.dataGridViewTextBoxColumn61.HeaderText = "Column4";
            this.dataGridViewTextBoxColumn61.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn61.Name = "dataGridViewTextBoxColumn61";
            this.dataGridViewTextBoxColumn61.Width = 50;
            // 
            // dataGridViewTextBoxColumn62
            // 
            this.dataGridViewTextBoxColumn62.HeaderText = "Column5";
            this.dataGridViewTextBoxColumn62.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn62.Name = "dataGridViewTextBoxColumn62";
            this.dataGridViewTextBoxColumn62.Width = 50;
            // 
            // dataGridViewTextBoxColumn63
            // 
            this.dataGridViewTextBoxColumn63.HeaderText = "Column6";
            this.dataGridViewTextBoxColumn63.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn63.Name = "dataGridViewTextBoxColumn63";
            this.dataGridViewTextBoxColumn63.Width = 50;
            // 
            // dataGridViewTextBoxColumn64
            // 
            this.dataGridViewTextBoxColumn64.HeaderText = "Column7";
            this.dataGridViewTextBoxColumn64.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn64.Name = "dataGridViewTextBoxColumn64";
            this.dataGridViewTextBoxColumn64.Width = 50;
            // 
            // dataGridViewTextBoxColumn65
            // 
            this.dataGridViewTextBoxColumn65.HeaderText = "Column8";
            this.dataGridViewTextBoxColumn65.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn65.Name = "dataGridViewTextBoxColumn65";
            this.dataGridViewTextBoxColumn65.Width = 50;
            // 
            // dataGridViewTextBoxColumn66
            // 
            this.dataGridViewTextBoxColumn66.HeaderText = "Column9";
            this.dataGridViewTextBoxColumn66.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn66.Name = "dataGridViewTextBoxColumn66";
            this.dataGridViewTextBoxColumn66.Width = 50;
            // 
            // dataGridViewTextBoxColumn67
            // 
            this.dataGridViewTextBoxColumn67.HeaderText = "Column10";
            this.dataGridViewTextBoxColumn67.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn67.Name = "dataGridViewTextBoxColumn67";
            this.dataGridViewTextBoxColumn67.Width = 50;
            // 
            // dataGridViewTextBoxColumn68
            // 
            this.dataGridViewTextBoxColumn68.HeaderText = "Column11";
            this.dataGridViewTextBoxColumn68.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn68.Name = "dataGridViewTextBoxColumn68";
            this.dataGridViewTextBoxColumn68.Width = 50;
            // 
            // dataGridViewTextBoxColumn69
            // 
            this.dataGridViewTextBoxColumn69.HeaderText = "Column12";
            this.dataGridViewTextBoxColumn69.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn69.Name = "dataGridViewTextBoxColumn69";
            this.dataGridViewTextBoxColumn69.Width = 50;
            // 
            // dataGridViewTextBoxColumn70
            // 
            this.dataGridViewTextBoxColumn70.HeaderText = "Column13";
            this.dataGridViewTextBoxColumn70.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn70.Name = "dataGridViewTextBoxColumn70";
            this.dataGridViewTextBoxColumn70.Width = 50;
            // 
            // dataGridViewTextBoxColumn71
            // 
            this.dataGridViewTextBoxColumn71.HeaderText = "Column14";
            this.dataGridViewTextBoxColumn71.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn71.Name = "dataGridViewTextBoxColumn71";
            this.dataGridViewTextBoxColumn71.Width = 50;
            // 
            // dataGridViewTextBoxColumn72
            // 
            this.dataGridViewTextBoxColumn72.HeaderText = "Column15";
            this.dataGridViewTextBoxColumn72.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn72.Name = "dataGridViewTextBoxColumn72";
            this.dataGridViewTextBoxColumn72.Width = 50;
            // 
            // dataGridViewTextBoxColumn73
            // 
            this.dataGridViewTextBoxColumn73.HeaderText = "Column16";
            this.dataGridViewTextBoxColumn73.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn73.Name = "dataGridViewTextBoxColumn73";
            this.dataGridViewTextBoxColumn73.Width = 50;
            // 
            // dataGridViewTextBoxColumn74
            // 
            this.dataGridViewTextBoxColumn74.HeaderText = "Column17";
            this.dataGridViewTextBoxColumn74.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn74.Name = "dataGridViewTextBoxColumn74";
            this.dataGridViewTextBoxColumn74.Width = 50;
            // 
            // dataGridViewTextBoxColumn75
            // 
            this.dataGridViewTextBoxColumn75.HeaderText = "Column18";
            this.dataGridViewTextBoxColumn75.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn75.Name = "dataGridViewTextBoxColumn75";
            this.dataGridViewTextBoxColumn75.Width = 50;
            // 
            // dataGridViewTextBoxColumn76
            // 
            this.dataGridViewTextBoxColumn76.HeaderText = "Column19";
            this.dataGridViewTextBoxColumn76.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn76.Name = "dataGridViewTextBoxColumn76";
            this.dataGridViewTextBoxColumn76.Width = 50;
            // 
            // dataGridViewTextBoxColumn77
            // 
            this.dataGridViewTextBoxColumn77.HeaderText = "Column20";
            this.dataGridViewTextBoxColumn77.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn77.Name = "dataGridViewTextBoxColumn77";
            this.dataGridViewTextBoxColumn77.Width = 50;
            // 
            // dataGridViewTextBoxColumn78
            // 
            this.dataGridViewTextBoxColumn78.HeaderText = "Column21";
            this.dataGridViewTextBoxColumn78.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn78.Name = "dataGridViewTextBoxColumn78";
            this.dataGridViewTextBoxColumn78.Width = 50;
            // 
            // dataGridViewTextBoxColumn79
            // 
            this.dataGridViewTextBoxColumn79.HeaderText = "Column22";
            this.dataGridViewTextBoxColumn79.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn79.Name = "dataGridViewTextBoxColumn79";
            this.dataGridViewTextBoxColumn79.Width = 50;
            // 
            // dataGridViewTextBoxColumn80
            // 
            this.dataGridViewTextBoxColumn80.HeaderText = "Column23";
            this.dataGridViewTextBoxColumn80.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn80.Name = "dataGridViewTextBoxColumn80";
            this.dataGridViewTextBoxColumn80.Width = 125;
            // 
            // groupBoxTable2_1_3
            // 
            this.groupBoxTable2_1_3.Controls.Add(this.Table2_1_3);
            this.groupBoxTable2_1_3.Location = new System.Drawing.Point(210, 100);
            this.groupBoxTable2_1_3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_1_3.Name = "groupBoxTable2_1_3";
            this.groupBoxTable2_1_3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_1_3.Size = new System.Drawing.Size(1096, 697);
            this.groupBoxTable2_1_3.TabIndex = 25;
            this.groupBoxTable2_1_3.TabStop = false;
            this.groupBoxTable2_1_3.Text = "Множина настання планових ризикових подій";
            this.groupBoxTable2_1_3.Visible = false;
            // 
            // Table2_1_3
            // 
            this.Table2_1_3.AllowUserToAddRows = false;
            this.Table2_1_3.AllowUserToDeleteRows = false;
            this.Table2_1_3.AllowUserToResizeRows = false;
            this.Table2_1_3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Table2_1_3.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.Table2_1_3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Table2_1_3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table2_1_3.ColumnHeadersVisible = false;
            this.Table2_1_3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40,
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn45,
            this.dataGridViewTextBoxColumn46,
            this.dataGridViewTextBoxColumn47,
            this.dataGridViewTextBoxColumn48,
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn53,
            this.dataGridViewTextBoxColumn54,
            this.dataGridViewTextBoxColumn55});
            this.Table2_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Table2_1_3.Location = new System.Drawing.Point(20, 23);
            this.Table2_1_3.Margin = new System.Windows.Forms.Padding(4);
            this.Table2_1_3.Name = "Table2_1_3";
            this.Table2_1_3.RowHeadersVisible = false;
            this.Table2_1_3.RowHeadersWidth = 51;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Table2_1_3.RowsDefaultCellStyle = dataGridViewCellStyle30;
            this.Table2_1_3.Size = new System.Drawing.Size(1076, 645);
            this.Table2_1_3.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.HeaderText = "RequireText";
            this.dataGridViewTextBoxColumn31.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.Width = 300;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.HeaderText = "ValueReq";
            this.dataGridViewTextBoxColumn32.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.Width = 50;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn33.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.Width = 50;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.HeaderText = "Column2";
            this.dataGridViewTextBoxColumn34.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.Width = 50;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.HeaderText = "Column3";
            this.dataGridViewTextBoxColumn35.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.Width = 50;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.HeaderText = "Column4";
            this.dataGridViewTextBoxColumn36.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.Width = 50;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.HeaderText = "Column5";
            this.dataGridViewTextBoxColumn37.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.Width = 50;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.HeaderText = "Column6";
            this.dataGridViewTextBoxColumn38.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.Width = 50;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.HeaderText = "Column7";
            this.dataGridViewTextBoxColumn39.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.Width = 50;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.HeaderText = "Column8";
            this.dataGridViewTextBoxColumn40.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.Width = 50;
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.HeaderText = "Column9";
            this.dataGridViewTextBoxColumn41.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.Width = 50;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.HeaderText = "Column10";
            this.dataGridViewTextBoxColumn42.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.Width = 50;
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.HeaderText = "Column11";
            this.dataGridViewTextBoxColumn43.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.Width = 50;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.HeaderText = "Column12";
            this.dataGridViewTextBoxColumn44.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.Width = 50;
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.HeaderText = "Column13";
            this.dataGridViewTextBoxColumn45.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.Width = 50;
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.HeaderText = "Column14";
            this.dataGridViewTextBoxColumn46.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.Width = 50;
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.HeaderText = "Column15";
            this.dataGridViewTextBoxColumn47.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.Width = 50;
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.HeaderText = "Column16";
            this.dataGridViewTextBoxColumn48.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.Width = 50;
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.HeaderText = "Column17";
            this.dataGridViewTextBoxColumn49.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.Width = 50;
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.HeaderText = "Column18";
            this.dataGridViewTextBoxColumn50.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.Width = 50;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.HeaderText = "Column19";
            this.dataGridViewTextBoxColumn51.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.Width = 50;
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.HeaderText = "Column20";
            this.dataGridViewTextBoxColumn52.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.Width = 50;
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.HeaderText = "Column21";
            this.dataGridViewTextBoxColumn53.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.Width = 50;
            // 
            // dataGridViewTextBoxColumn54
            // 
            this.dataGridViewTextBoxColumn54.HeaderText = "Column22";
            this.dataGridViewTextBoxColumn54.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            this.dataGridViewTextBoxColumn54.Width = 50;
            // 
            // dataGridViewTextBoxColumn55
            // 
            this.dataGridViewTextBoxColumn55.HeaderText = "Column23";
            this.dataGridViewTextBoxColumn55.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            this.dataGridViewTextBoxColumn55.Width = 125;
            // 
            // groupBoxTable2_1_2
            // 
            this.groupBoxTable2_1_2.Controls.Add(this.Table2_1_2);
            this.groupBoxTable2_1_2.Location = new System.Drawing.Point(210, 100);
            this.groupBoxTable2_1_2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_1_2.Name = "groupBoxTable2_1_2";
            this.groupBoxTable2_1_2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_1_2.Size = new System.Drawing.Size(1096, 697);
            this.groupBoxTable2_1_2.TabIndex = 24;
            this.groupBoxTable2_1_2.TabStop = false;
            this.groupBoxTable2_1_2.Text = "Множина настання вартісних ризикових подій";
            this.groupBoxTable2_1_2.Visible = false;
            // 
            // Table2_1_2
            // 
            this.Table2_1_2.AllowUserToAddRows = false;
            this.Table2_1_2.AllowUserToDeleteRows = false;
            this.Table2_1_2.AllowUserToResizeRows = false;
            this.Table2_1_2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Table2_1_2.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.Table2_1_2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Table2_1_2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table2_1_2.ColumnHeadersVisible = false;
            this.Table2_1_2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30});
            this.Table2_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Table2_1_2.Location = new System.Drawing.Point(20, 23);
            this.Table2_1_2.Margin = new System.Windows.Forms.Padding(4);
            this.Table2_1_2.Name = "Table2_1_2";
            this.Table2_1_2.RowHeadersVisible = false;
            this.Table2_1_2.RowHeadersWidth = 51;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Table2_1_2.RowsDefaultCellStyle = dataGridViewCellStyle31;
            this.Table2_1_2.Size = new System.Drawing.Size(1076, 645);
            this.Table2_1_2.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "RequireText";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 300;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "ValueReq";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 50;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 50;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Column2";
            this.dataGridViewTextBoxColumn9.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Width = 50;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "Column3";
            this.dataGridViewTextBoxColumn10.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 50;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "Column4";
            this.dataGridViewTextBoxColumn11.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Width = 50;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "Column5";
            this.dataGridViewTextBoxColumn12.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Width = 50;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "Column6";
            this.dataGridViewTextBoxColumn13.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Width = 50;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "Column7";
            this.dataGridViewTextBoxColumn14.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Width = 50;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "Column8";
            this.dataGridViewTextBoxColumn15.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Width = 50;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "Column9";
            this.dataGridViewTextBoxColumn16.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.Width = 50;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.HeaderText = "Column10";
            this.dataGridViewTextBoxColumn17.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.Width = 50;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "Column11";
            this.dataGridViewTextBoxColumn18.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.Width = 50;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.HeaderText = "Column12";
            this.dataGridViewTextBoxColumn19.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.Width = 50;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.HeaderText = "Column13";
            this.dataGridViewTextBoxColumn20.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.Width = 50;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.HeaderText = "Column14";
            this.dataGridViewTextBoxColumn21.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.Width = 50;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "Column15";
            this.dataGridViewTextBoxColumn22.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.Width = 50;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.HeaderText = "Column16";
            this.dataGridViewTextBoxColumn23.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.Width = 50;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "Column17";
            this.dataGridViewTextBoxColumn24.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.Width = 50;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.HeaderText = "Column18";
            this.dataGridViewTextBoxColumn25.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.Width = 50;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.HeaderText = "Column19";
            this.dataGridViewTextBoxColumn26.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.Width = 50;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.HeaderText = "Column20";
            this.dataGridViewTextBoxColumn27.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.Width = 50;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.HeaderText = "Column21";
            this.dataGridViewTextBoxColumn28.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.Width = 50;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.HeaderText = "Column22";
            this.dataGridViewTextBoxColumn29.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.Width = 50;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.HeaderText = "Column23";
            this.dataGridViewTextBoxColumn30.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.Width = 125;
            // 
            // groupBoxTable2_1_1
            // 
            this.groupBoxTable2_1_1.Controls.Add(this.Table2_1_1);
            this.groupBoxTable2_1_1.Location = new System.Drawing.Point(210, 100);
            this.groupBoxTable2_1_1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_1_1.Name = "groupBoxTable2_1_1";
            this.groupBoxTable2_1_1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_1_1.Size = new System.Drawing.Size(1096, 697);
            this.groupBoxTable2_1_1.TabIndex = 23;
            this.groupBoxTable2_1_1.TabStop = false;
            this.groupBoxTable2_1_1.Text = "Множина настання технічних ризикових подій";
            this.groupBoxTable2_1_1.Visible = false;
            // 
            // Table2_1_1
            // 
            this.Table2_1_1.AllowUserToAddRows = false;
            this.Table2_1_1.AllowUserToDeleteRows = false;
            this.Table2_1_1.AllowUserToResizeRows = false;
            this.Table2_1_1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Table2_1_1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.Table2_1_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Table2_1_1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table2_1_1.ColumnHeadersVisible = false;
            this.Table2_1_1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column20,
            this.Column21,
            this.Column22,
            this.Column23});
            this.Table2_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Table2_1_1.Location = new System.Drawing.Point(20, 23);
            this.Table2_1_1.Margin = new System.Windows.Forms.Padding(4);
            this.Table2_1_1.Name = "Table2_1_1";
            this.Table2_1_1.RowHeadersVisible = false;
            this.Table2_1_1.RowHeadersWidth = 51;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Table2_1_1.RowsDefaultCellStyle = dataGridViewCellStyle32;
            this.Table2_1_1.Size = new System.Drawing.Size(1076, 645);
            this.Table2_1_1.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "RequireText";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 300;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "ValueReq";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 50;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.Width = 50;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.Width = 50;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.Width = 50;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Column5";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.Width = 50;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Column6";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            this.Column6.Width = 50;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Column7";
            this.Column7.MinimumWidth = 6;
            this.Column7.Name = "Column7";
            this.Column7.Width = 50;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Column8";
            this.Column8.MinimumWidth = 6;
            this.Column8.Name = "Column8";
            this.Column8.Width = 50;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Column9";
            this.Column9.MinimumWidth = 6;
            this.Column9.Name = "Column9";
            this.Column9.Width = 50;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Column10";
            this.Column10.MinimumWidth = 6;
            this.Column10.Name = "Column10";
            this.Column10.Width = 50;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "Column11";
            this.Column11.MinimumWidth = 6;
            this.Column11.Name = "Column11";
            this.Column11.Width = 50;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "Column12";
            this.Column12.MinimumWidth = 6;
            this.Column12.Name = "Column12";
            this.Column12.Width = 50;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "Column13";
            this.Column13.MinimumWidth = 6;
            this.Column13.Name = "Column13";
            this.Column13.Width = 50;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "Column14";
            this.Column14.MinimumWidth = 6;
            this.Column14.Name = "Column14";
            this.Column14.Width = 50;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "Column15";
            this.Column15.MinimumWidth = 6;
            this.Column15.Name = "Column15";
            this.Column15.Width = 50;
            // 
            // Column16
            // 
            this.Column16.HeaderText = "Column16";
            this.Column16.MinimumWidth = 6;
            this.Column16.Name = "Column16";
            this.Column16.Width = 50;
            // 
            // Column17
            // 
            this.Column17.HeaderText = "Column17";
            this.Column17.MinimumWidth = 6;
            this.Column17.Name = "Column17";
            this.Column17.Width = 50;
            // 
            // Column18
            // 
            this.Column18.HeaderText = "Column18";
            this.Column18.MinimumWidth = 6;
            this.Column18.Name = "Column18";
            this.Column18.Width = 50;
            // 
            // Column19
            // 
            this.Column19.HeaderText = "Column19";
            this.Column19.MinimumWidth = 6;
            this.Column19.Name = "Column19";
            this.Column19.Width = 50;
            // 
            // Column20
            // 
            this.Column20.HeaderText = "Column20";
            this.Column20.MinimumWidth = 6;
            this.Column20.Name = "Column20";
            this.Column20.Width = 50;
            // 
            // Column21
            // 
            this.Column21.HeaderText = "Column21";
            this.Column21.MinimumWidth = 6;
            this.Column21.Name = "Column21";
            this.Column21.Width = 50;
            // 
            // Column22
            // 
            this.Column22.HeaderText = "Column22";
            this.Column22.MinimumWidth = 6;
            this.Column22.Name = "Column22";
            this.Column22.Width = 50;
            // 
            // Column23
            // 
            this.Column23.HeaderText = "Column23";
            this.Column23.MinimumWidth = 6;
            this.Column23.Name = "Column23";
            this.Column23.Width = 125;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1393, 219);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 21);
            this.label6.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1393, 169);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 21);
            this.label2.TabIndex = 9;
            // 
            // buttonTable2_1_4
            // 
            this.buttonTable2_1_4.BackColor = System.Drawing.Color.NavajoWhite;
            this.buttonTable2_1_4.Location = new System.Drawing.Point(8, 400);
            this.buttonTable2_1_4.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTable2_1_4.Name = "buttonTable2_1_4";
            this.buttonTable2_1_4.Size = new System.Drawing.Size(173, 123);
            this.buttonTable2_1_4.TabIndex = 6;
            this.buttonTable2_1_4.Text = "Множина настання ризикових подій реалізації процесу управління програмним проекто" +
    "м";
            this.buttonTable2_1_4.UseVisualStyleBackColor = false;
            this.buttonTable2_1_4.Click += new System.EventHandler(this.buttonTable2_1_4_Click);
            // 
            // buttonTable2_1_3
            // 
            this.buttonTable2_1_3.BackColor = System.Drawing.Color.NavajoWhite;
            this.buttonTable2_1_3.Location = new System.Drawing.Point(8, 308);
            this.buttonTable2_1_3.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTable2_1_3.Name = "buttonTable2_1_3";
            this.buttonTable2_1_3.Size = new System.Drawing.Size(173, 74);
            this.buttonTable2_1_3.TabIndex = 5;
            this.buttonTable2_1_3.Text = "Множина настання планових ризикових подій";
            this.buttonTable2_1_3.UseVisualStyleBackColor = false;
            this.buttonTable2_1_3.Click += new System.EventHandler(this.buttonTable2_1_3_Click);
            // 
            // buttonTable2_1_2
            // 
            this.buttonTable2_1_2.BackColor = System.Drawing.Color.NavajoWhite;
            this.buttonTable2_1_2.Location = new System.Drawing.Point(8, 215);
            this.buttonTable2_1_2.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTable2_1_2.Name = "buttonTable2_1_2";
            this.buttonTable2_1_2.Size = new System.Drawing.Size(173, 73);
            this.buttonTable2_1_2.TabIndex = 4;
            this.buttonTable2_1_2.Text = "Множина настання вартісних ризикових подій";
            this.buttonTable2_1_2.UseVisualStyleBackColor = false;
            this.buttonTable2_1_2.Click += new System.EventHandler(this.buttonTable2_1_2_Click);
            // 
            // buttonTable2_1_1
            // 
            this.buttonTable2_1_1.BackColor = System.Drawing.Color.NavajoWhite;
            this.buttonTable2_1_1.Location = new System.Drawing.Point(8, 123);
            this.buttonTable2_1_1.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTable2_1_1.Name = "buttonTable2_1_1";
            this.buttonTable2_1_1.Size = new System.Drawing.Size(173, 74);
            this.buttonTable2_1_1.TabIndex = 3;
            this.buttonTable2_1_1.Text = "Множина настання технічних ризикових подій";
            this.buttonTable2_1_1.UseVisualStyleBackColor = false;
            this.buttonTable2_1_1.Click += new System.EventHandler(this.buttonTable2_1_1_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBoxTable2_2);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1571, 771);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Етап 2.2";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBoxTable2_2
            // 
            this.groupBoxTable2_2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBoxTable2_2.BackColor = System.Drawing.Color.PapayaWhip;
            this.groupBoxTable2_2.Controls.Add(this.groupBoxTable2_2_Sum);
            this.groupBoxTable2_2.Controls.Add(this.minReqValue);
            this.groupBoxTable2_2.Controls.Add(this.maxReqValue);
            this.groupBoxTable2_2.Controls.Add(this.highLevel);
            this.groupBoxTable2_2.Controls.Add(this.mediumLevel);
            this.groupBoxTable2_2.Controls.Add(this.lowLevel);
            this.groupBoxTable2_2.Controls.Add(this.label25);
            this.groupBoxTable2_2.Controls.Add(this.label24);
            this.groupBoxTable2_2.Controls.Add(this.label23);
            this.groupBoxTable2_2.Controls.Add(this.label12);
            this.groupBoxTable2_2.Controls.Add(this.label1);
            this.groupBoxTable2_2.Controls.Add(this.label11);
            this.groupBoxTable2_2.Controls.Add(this.label22);
            this.groupBoxTable2_2.Controls.Add(this.textBox16);
            this.groupBoxTable2_2.Controls.Add(this.buttonTableSum);
            this.groupBoxTable2_2.Controls.Add(this.label21);
            this.groupBoxTable2_2.Controls.Add(this.textBox15);
            this.groupBoxTable2_2.Controls.Add(this.label20);
            this.groupBoxTable2_2.Controls.Add(this.textBox14);
            this.groupBoxTable2_2.Controls.Add(this.groupBoxTable2_2_4);
            this.groupBoxTable2_2.Controls.Add(this.groupBoxTable2_2_3);
            this.groupBoxTable2_2.Controls.Add(this.groupBoxTable2_2_2);
            this.groupBoxTable2_2.Controls.Add(this.groupBoxTable2_2_1);
            this.groupBoxTable2_2.Controls.Add(this.label13);
            this.groupBoxTable2_2.Controls.Add(this.textBox9);
            this.groupBoxTable2_2.Controls.Add(this.label14);
            this.groupBoxTable2_2.Controls.Add(this.textBox10);
            this.groupBoxTable2_2.Controls.Add(this.label15);
            this.groupBoxTable2_2.Controls.Add(this.label16);
            this.groupBoxTable2_2.Controls.Add(this.textBox11);
            this.groupBoxTable2_2.Controls.Add(this.label17);
            this.groupBoxTable2_2.Controls.Add(this.label18);
            this.groupBoxTable2_2.Controls.Add(this.textBox12);
            this.groupBoxTable2_2.Controls.Add(this.label19);
            this.groupBoxTable2_2.Controls.Add(this.textBox13);
            this.groupBoxTable2_2.Controls.Add(this.buttonTable2_2_4);
            this.groupBoxTable2_2.Controls.Add(this.buttonTable2_2_3);
            this.groupBoxTable2_2.Controls.Add(this.buttonTable2_2_2);
            this.groupBoxTable2_2.Controls.Add(this.buttonTable2_2_1);
            this.groupBoxTable2_2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxTable2_2.Location = new System.Drawing.Point(3, 3);
            this.groupBoxTable2_2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_2.Name = "groupBoxTable2_2";
            this.groupBoxTable2_2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_2.Size = new System.Drawing.Size(1576, 763);
            this.groupBoxTable2_2.TabIndex = 7;
            this.groupBoxTable2_2.TabStop = false;
            this.groupBoxTable2_2.Text = "Визначення частки можливих збитків від прояву ризику та визначення величини збитк" +
    "ів від прояву ризику (математичне сподівання збитку)";
            this.groupBoxTable2_2.Visible = false;
            // 
            // groupBoxTable2_2_Sum
            // 
            this.groupBoxTable2_2_Sum.Controls.Add(this.Table2_2_Sum);
            this.groupBoxTable2_2_Sum.Location = new System.Drawing.Point(217, 50);
            this.groupBoxTable2_2_Sum.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_2_Sum.Name = "groupBoxTable2_2_Sum";
            this.groupBoxTable2_2_Sum.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_2_Sum.Size = new System.Drawing.Size(1129, 697);
            this.groupBoxTable2_2_Sum.TabIndex = 50;
            this.groupBoxTable2_2_Sum.TabStop = false;
            this.groupBoxTable2_2_Sum.Text = "Розподіл вартості реалізації проекту за множинами ризиків";
            this.groupBoxTable2_2_Sum.Visible = false;
            // 
            // Table2_2_Sum
            // 
            this.Table2_2_Sum.AllowUserToAddRows = false;
            this.Table2_2_Sum.AllowUserToDeleteRows = false;
            this.Table2_2_Sum.AllowUserToResizeRows = false;
            this.Table2_2_Sum.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Table2_2_Sum.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Table2_2_Sum.BackgroundColor = System.Drawing.Color.PapayaWhip;
            this.Table2_2_Sum.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Table2_2_Sum.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table2_2_Sum.ColumnHeadersVisible = false;
            this.Table2_2_Sum.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn183,
            this.dataGridViewTextBoxColumn184,
            this.dataGridViewTextBoxColumn181,
            this.dataGridViewTextBoxColumn182,
            this.Column32,
            this.Column33});
            this.Table2_2_Sum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Table2_2_Sum.Location = new System.Drawing.Point(20, 23);
            this.Table2_2_Sum.Margin = new System.Windows.Forms.Padding(4);
            this.Table2_2_Sum.Name = "Table2_2_Sum";
            this.Table2_2_Sum.RowHeadersVisible = false;
            this.Table2_2_Sum.RowHeadersWidth = 51;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Table2_2_Sum.RowsDefaultCellStyle = dataGridViewCellStyle33;
            this.Table2_2_Sum.Size = new System.Drawing.Size(1100, 645);
            this.Table2_2_Sum.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn183
            // 
            this.dataGridViewTextBoxColumn183.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn183.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn183.Name = "dataGridViewTextBoxColumn183";
            // 
            // dataGridViewTextBoxColumn184
            // 
            this.dataGridViewTextBoxColumn184.HeaderText = "Column2";
            this.dataGridViewTextBoxColumn184.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn184.Name = "dataGridViewTextBoxColumn184";
            // 
            // dataGridViewTextBoxColumn181
            // 
            this.dataGridViewTextBoxColumn181.HeaderText = "Column3";
            this.dataGridViewTextBoxColumn181.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn181.Name = "dataGridViewTextBoxColumn181";
            // 
            // dataGridViewTextBoxColumn182
            // 
            this.dataGridViewTextBoxColumn182.HeaderText = "Column4";
            this.dataGridViewTextBoxColumn182.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn182.Name = "dataGridViewTextBoxColumn182";
            // 
            // Column32
            // 
            this.Column32.HeaderText = "Column5";
            this.Column32.MinimumWidth = 6;
            this.Column32.Name = "Column32";
            // 
            // Column33
            // 
            this.Column33.HeaderText = "Column6";
            this.Column33.MinimumWidth = 6;
            this.Column33.Name = "Column33";
            // 
            // minReqValue
            // 
            this.minReqValue.AutoSize = true;
            this.minReqValue.Location = new System.Drawing.Point(1428, 580);
            this.minReqValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.minReqValue.Name = "minReqValue";
            this.minReqValue.Size = new System.Drawing.Size(61, 21);
            this.minReqValue.TabIndex = 49;
            this.minReqValue.Text = "label27";
            // 
            // maxReqValue
            // 
            this.maxReqValue.AutoSize = true;
            this.maxReqValue.Location = new System.Drawing.Point(1427, 514);
            this.maxReqValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.maxReqValue.Name = "maxReqValue";
            this.maxReqValue.Size = new System.Drawing.Size(61, 21);
            this.maxReqValue.TabIndex = 48;
            this.maxReqValue.Text = "label26";
            // 
            // highLevel
            // 
            this.highLevel.AutoSize = true;
            this.highLevel.Location = new System.Drawing.Point(1447, 726);
            this.highLevel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.highLevel.Name = "highLevel";
            this.highLevel.Size = new System.Drawing.Size(75, 21);
            this.highLevel.TabIndex = 47;
            this.highLevel.Text = "highLevel";
            // 
            // mediumLevel
            // 
            this.mediumLevel.AutoSize = true;
            this.mediumLevel.Location = new System.Drawing.Point(1447, 687);
            this.mediumLevel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.mediumLevel.Name = "mediumLevel";
            this.mediumLevel.Size = new System.Drawing.Size(103, 21);
            this.mediumLevel.TabIndex = 46;
            this.mediumLevel.Text = "mediumLevel";
            // 
            // lowLevel
            // 
            this.lowLevel.AutoSize = true;
            this.lowLevel.Location = new System.Drawing.Point(1447, 647);
            this.lowLevel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lowLevel.Name = "lowLevel";
            this.lowLevel.Size = new System.Drawing.Size(70, 21);
            this.lowLevel.TabIndex = 45;
            this.lowLevel.Text = "lowLevel";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(1352, 726);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.MaximumSize = new System.Drawing.Size(173, 86);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(79, 21);
            this.label25.TabIndex = 44;
            this.label25.Text = "Високий :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(1347, 687);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.MaximumSize = new System.Drawing.Size(173, 86);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(84, 21);
            this.label24.TabIndex = 43;
            this.label24.Text = "Середній :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(1365, 606);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.MaximumSize = new System.Drawing.Size(213, 86);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(190, 21);
            this.label23.TabIndex = 42;
            this.label23.Text = "Рівні пріоритетів ризиків";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(1353, 647);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.MaximumSize = new System.Drawing.Size(173, 86);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 21);
            this.label12.TabIndex = 41;
            this.label12.Text = "Низький :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(1368, 535);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.MaximumSize = new System.Drawing.Size(213, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 42);
            this.label1.TabIndex = 40;
            this.label1.Text = "Min додаткова вартість вимоги";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(1368, 473);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.MaximumSize = new System.Drawing.Size(213, 86);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(184, 42);
            this.label11.TabIndex = 39;
            this.label11.Text = "Max додаткова вартість вимоги";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(1393, 421);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.MaximumSize = new System.Drawing.Size(173, 86);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(166, 42);
            this.label22.TabIndex = 37;
            this.label22.Text = "Кінцева вартість, тис. грн";
            // 
            // textBox16
            // 
            this.textBox16.BackColor = System.Drawing.Color.LimeGreen;
            this.textBox16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox16.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox16.Enabled = false;
            this.textBox16.Location = new System.Drawing.Point(1353, 427);
            this.textBox16.Margin = new System.Windows.Forms.Padding(4);
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.Size = new System.Drawing.Size(37, 27);
            this.textBox16.TabIndex = 36;
            // 
            // buttonTableSum
            // 
            this.buttonTableSum.BackColor = System.Drawing.Color.NavajoWhite;
            this.buttonTableSum.Location = new System.Drawing.Point(22, 506);
            this.buttonTableSum.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTableSum.Name = "buttonTableSum";
            this.buttonTableSum.Size = new System.Drawing.Size(173, 98);
            this.buttonTableSum.TabIndex = 35;
            this.buttonTableSum.Text = "Розподіл вартості реалізації проекту за множинами ризиків";
            this.buttonTableSum.UseVisualStyleBackColor = false;
            this.buttonTableSum.Click += new System.EventHandler(this.buttonTableSum_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(1393, 100);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.MaximumSize = new System.Drawing.Size(173, 86);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(147, 42);
            this.label21.TabIndex = 34;
            this.label21.Text = "Вартість реалізації вимоги";
            // 
            // textBox15
            // 
            this.textBox15.BackColor = System.Drawing.Color.LightCyan;
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox15.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox15.Enabled = false;
            this.textBox15.Location = new System.Drawing.Point(1353, 103);
            this.textBox15.Margin = new System.Windows.Forms.Padding(4);
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(37, 27);
            this.textBox15.TabIndex = 33;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(1393, 49);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.MaximumSize = new System.Drawing.Size(173, 86);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(152, 42);
            this.label20.TabIndex = 32;
            this.label20.Text = "Початкова вартість реалізації проекту";
            // 
            // textBox14
            // 
            this.textBox14.BackColor = System.Drawing.Color.IndianRed;
            this.textBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox14.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox14.Enabled = false;
            this.textBox14.Location = new System.Drawing.Point(1353, 54);
            this.textBox14.Margin = new System.Windows.Forms.Padding(4);
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.Size = new System.Drawing.Size(37, 27);
            this.textBox14.TabIndex = 31;
            // 
            // groupBoxTable2_2_4
            // 
            this.groupBoxTable2_2_4.Controls.Add(this.Table2_2_4);
            this.groupBoxTable2_2_4.Location = new System.Drawing.Point(216, 50);
            this.groupBoxTable2_2_4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_2_4.Name = "groupBoxTable2_2_4";
            this.groupBoxTable2_2_4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_2_4.Size = new System.Drawing.Size(1129, 697);
            this.groupBoxTable2_2_4.TabIndex = 26;
            this.groupBoxTable2_2_4.TabStop = false;
            this.groupBoxTable2_2_4.Text = "Множина настання ризикових подій реалізації процесу управління програмним проекто" +
    "м";
            this.groupBoxTable2_2_4.Visible = false;
            // 
            // Table2_2_4
            // 
            this.Table2_2_4.AllowUserToAddRows = false;
            this.Table2_2_4.AllowUserToDeleteRows = false;
            this.Table2_2_4.AllowUserToResizeRows = false;
            this.Table2_2_4.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Table2_2_4.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.Table2_2_4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Table2_2_4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table2_2_4.ColumnHeadersVisible = false;
            this.Table2_2_4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn81,
            this.dataGridViewTextBoxColumn82,
            this.dataGridViewTextBoxColumn83,
            this.dataGridViewTextBoxColumn84,
            this.dataGridViewTextBoxColumn85,
            this.dataGridViewTextBoxColumn86,
            this.dataGridViewTextBoxColumn87,
            this.dataGridViewTextBoxColumn88,
            this.dataGridViewTextBoxColumn89,
            this.dataGridViewTextBoxColumn90,
            this.dataGridViewTextBoxColumn91,
            this.dataGridViewTextBoxColumn92,
            this.dataGridViewTextBoxColumn93,
            this.dataGridViewTextBoxColumn94,
            this.dataGridViewTextBoxColumn95,
            this.dataGridViewTextBoxColumn96,
            this.dataGridViewTextBoxColumn97,
            this.dataGridViewTextBoxColumn98,
            this.dataGridViewTextBoxColumn99,
            this.dataGridViewTextBoxColumn100,
            this.dataGridViewTextBoxColumn101,
            this.dataGridViewTextBoxColumn102,
            this.dataGridViewTextBoxColumn103,
            this.dataGridViewTextBoxColumn104,
            this.dataGridViewTextBoxColumn105,
            this.Column24,
            this.Column25});
            this.Table2_2_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Table2_2_4.Location = new System.Drawing.Point(20, 23);
            this.Table2_2_4.Margin = new System.Windows.Forms.Padding(4);
            this.Table2_2_4.Name = "Table2_2_4";
            this.Table2_2_4.RowHeadersVisible = false;
            this.Table2_2_4.RowHeadersWidth = 51;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Table2_2_4.RowsDefaultCellStyle = dataGridViewCellStyle34;
            this.Table2_2_4.Size = new System.Drawing.Size(1076, 645);
            this.Table2_2_4.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn81
            // 
            this.dataGridViewTextBoxColumn81.HeaderText = "RequireText";
            this.dataGridViewTextBoxColumn81.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn81.Name = "dataGridViewTextBoxColumn81";
            this.dataGridViewTextBoxColumn81.Width = 300;
            // 
            // dataGridViewTextBoxColumn82
            // 
            this.dataGridViewTextBoxColumn82.HeaderText = "ValueReq";
            this.dataGridViewTextBoxColumn82.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn82.Name = "dataGridViewTextBoxColumn82";
            this.dataGridViewTextBoxColumn82.Width = 50;
            // 
            // dataGridViewTextBoxColumn83
            // 
            this.dataGridViewTextBoxColumn83.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn83.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn83.Name = "dataGridViewTextBoxColumn83";
            this.dataGridViewTextBoxColumn83.Width = 50;
            // 
            // dataGridViewTextBoxColumn84
            // 
            this.dataGridViewTextBoxColumn84.HeaderText = "Column2";
            this.dataGridViewTextBoxColumn84.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn84.Name = "dataGridViewTextBoxColumn84";
            this.dataGridViewTextBoxColumn84.Width = 50;
            // 
            // dataGridViewTextBoxColumn85
            // 
            this.dataGridViewTextBoxColumn85.HeaderText = "Column3";
            this.dataGridViewTextBoxColumn85.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn85.Name = "dataGridViewTextBoxColumn85";
            this.dataGridViewTextBoxColumn85.Width = 50;
            // 
            // dataGridViewTextBoxColumn86
            // 
            this.dataGridViewTextBoxColumn86.HeaderText = "Column4";
            this.dataGridViewTextBoxColumn86.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn86.Name = "dataGridViewTextBoxColumn86";
            this.dataGridViewTextBoxColumn86.Width = 50;
            // 
            // dataGridViewTextBoxColumn87
            // 
            this.dataGridViewTextBoxColumn87.HeaderText = "Column5";
            this.dataGridViewTextBoxColumn87.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn87.Name = "dataGridViewTextBoxColumn87";
            this.dataGridViewTextBoxColumn87.Width = 50;
            // 
            // dataGridViewTextBoxColumn88
            // 
            this.dataGridViewTextBoxColumn88.HeaderText = "Column6";
            this.dataGridViewTextBoxColumn88.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn88.Name = "dataGridViewTextBoxColumn88";
            this.dataGridViewTextBoxColumn88.Width = 50;
            // 
            // dataGridViewTextBoxColumn89
            // 
            this.dataGridViewTextBoxColumn89.HeaderText = "Column7";
            this.dataGridViewTextBoxColumn89.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn89.Name = "dataGridViewTextBoxColumn89";
            this.dataGridViewTextBoxColumn89.Width = 50;
            // 
            // dataGridViewTextBoxColumn90
            // 
            this.dataGridViewTextBoxColumn90.HeaderText = "Column8";
            this.dataGridViewTextBoxColumn90.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn90.Name = "dataGridViewTextBoxColumn90";
            this.dataGridViewTextBoxColumn90.Width = 50;
            // 
            // dataGridViewTextBoxColumn91
            // 
            this.dataGridViewTextBoxColumn91.HeaderText = "Column9";
            this.dataGridViewTextBoxColumn91.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn91.Name = "dataGridViewTextBoxColumn91";
            this.dataGridViewTextBoxColumn91.Width = 50;
            // 
            // dataGridViewTextBoxColumn92
            // 
            this.dataGridViewTextBoxColumn92.HeaderText = "Column10";
            this.dataGridViewTextBoxColumn92.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn92.Name = "dataGridViewTextBoxColumn92";
            this.dataGridViewTextBoxColumn92.Width = 50;
            // 
            // dataGridViewTextBoxColumn93
            // 
            this.dataGridViewTextBoxColumn93.HeaderText = "Column11";
            this.dataGridViewTextBoxColumn93.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn93.Name = "dataGridViewTextBoxColumn93";
            this.dataGridViewTextBoxColumn93.Width = 50;
            // 
            // dataGridViewTextBoxColumn94
            // 
            this.dataGridViewTextBoxColumn94.HeaderText = "Column12";
            this.dataGridViewTextBoxColumn94.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn94.Name = "dataGridViewTextBoxColumn94";
            this.dataGridViewTextBoxColumn94.Width = 50;
            // 
            // dataGridViewTextBoxColumn95
            // 
            this.dataGridViewTextBoxColumn95.HeaderText = "Column13";
            this.dataGridViewTextBoxColumn95.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn95.Name = "dataGridViewTextBoxColumn95";
            this.dataGridViewTextBoxColumn95.Width = 50;
            // 
            // dataGridViewTextBoxColumn96
            // 
            this.dataGridViewTextBoxColumn96.HeaderText = "Column14";
            this.dataGridViewTextBoxColumn96.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn96.Name = "dataGridViewTextBoxColumn96";
            this.dataGridViewTextBoxColumn96.Width = 50;
            // 
            // dataGridViewTextBoxColumn97
            // 
            this.dataGridViewTextBoxColumn97.HeaderText = "Column15";
            this.dataGridViewTextBoxColumn97.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn97.Name = "dataGridViewTextBoxColumn97";
            this.dataGridViewTextBoxColumn97.Width = 50;
            // 
            // dataGridViewTextBoxColumn98
            // 
            this.dataGridViewTextBoxColumn98.HeaderText = "Column16";
            this.dataGridViewTextBoxColumn98.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn98.Name = "dataGridViewTextBoxColumn98";
            this.dataGridViewTextBoxColumn98.Width = 50;
            // 
            // dataGridViewTextBoxColumn99
            // 
            this.dataGridViewTextBoxColumn99.HeaderText = "Column17";
            this.dataGridViewTextBoxColumn99.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn99.Name = "dataGridViewTextBoxColumn99";
            this.dataGridViewTextBoxColumn99.Width = 50;
            // 
            // dataGridViewTextBoxColumn100
            // 
            this.dataGridViewTextBoxColumn100.HeaderText = "Column18";
            this.dataGridViewTextBoxColumn100.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn100.Name = "dataGridViewTextBoxColumn100";
            this.dataGridViewTextBoxColumn100.Width = 50;
            // 
            // dataGridViewTextBoxColumn101
            // 
            this.dataGridViewTextBoxColumn101.HeaderText = "Column19";
            this.dataGridViewTextBoxColumn101.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn101.Name = "dataGridViewTextBoxColumn101";
            this.dataGridViewTextBoxColumn101.Width = 50;
            // 
            // dataGridViewTextBoxColumn102
            // 
            this.dataGridViewTextBoxColumn102.HeaderText = "Column20";
            this.dataGridViewTextBoxColumn102.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn102.Name = "dataGridViewTextBoxColumn102";
            this.dataGridViewTextBoxColumn102.Width = 50;
            // 
            // dataGridViewTextBoxColumn103
            // 
            this.dataGridViewTextBoxColumn103.HeaderText = "Column21";
            this.dataGridViewTextBoxColumn103.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn103.Name = "dataGridViewTextBoxColumn103";
            this.dataGridViewTextBoxColumn103.Width = 50;
            // 
            // dataGridViewTextBoxColumn104
            // 
            this.dataGridViewTextBoxColumn104.HeaderText = "Column22";
            this.dataGridViewTextBoxColumn104.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn104.Name = "dataGridViewTextBoxColumn104";
            this.dataGridViewTextBoxColumn104.Width = 50;
            // 
            // dataGridViewTextBoxColumn105
            // 
            this.dataGridViewTextBoxColumn105.HeaderText = "Column23";
            this.dataGridViewTextBoxColumn105.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn105.Name = "dataGridViewTextBoxColumn105";
            this.dataGridViewTextBoxColumn105.Width = 50;
            // 
            // Column24
            // 
            this.Column24.HeaderText = "Column24";
            this.Column24.MinimumWidth = 6;
            this.Column24.Name = "Column24";
            this.Column24.Width = 50;
            // 
            // Column25
            // 
            this.Column25.HeaderText = "Column25";
            this.Column25.MinimumWidth = 6;
            this.Column25.Name = "Column25";
            this.Column25.Width = 125;
            // 
            // groupBoxTable2_2_3
            // 
            this.groupBoxTable2_2_3.Controls.Add(this.Table2_2_3);
            this.groupBoxTable2_2_3.Location = new System.Drawing.Point(216, 50);
            this.groupBoxTable2_2_3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_2_3.Name = "groupBoxTable2_2_3";
            this.groupBoxTable2_2_3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_2_3.Size = new System.Drawing.Size(1129, 697);
            this.groupBoxTable2_2_3.TabIndex = 25;
            this.groupBoxTable2_2_3.TabStop = false;
            this.groupBoxTable2_2_3.Text = "Множина настання планових ризикових подій";
            this.groupBoxTable2_2_3.Visible = false;
            // 
            // Table2_2_3
            // 
            this.Table2_2_3.AllowUserToAddRows = false;
            this.Table2_2_3.AllowUserToDeleteRows = false;
            this.Table2_2_3.AllowUserToResizeRows = false;
            this.Table2_2_3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Table2_2_3.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.Table2_2_3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Table2_2_3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table2_2_3.ColumnHeadersVisible = false;
            this.Table2_2_3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn106,
            this.dataGridViewTextBoxColumn107,
            this.dataGridViewTextBoxColumn108,
            this.dataGridViewTextBoxColumn109,
            this.dataGridViewTextBoxColumn110,
            this.dataGridViewTextBoxColumn111,
            this.dataGridViewTextBoxColumn112,
            this.dataGridViewTextBoxColumn113,
            this.dataGridViewTextBoxColumn114,
            this.dataGridViewTextBoxColumn115,
            this.dataGridViewTextBoxColumn116,
            this.dataGridViewTextBoxColumn117,
            this.dataGridViewTextBoxColumn118,
            this.dataGridViewTextBoxColumn119,
            this.dataGridViewTextBoxColumn120,
            this.dataGridViewTextBoxColumn121,
            this.dataGridViewTextBoxColumn122,
            this.dataGridViewTextBoxColumn123,
            this.dataGridViewTextBoxColumn124,
            this.dataGridViewTextBoxColumn125,
            this.dataGridViewTextBoxColumn126,
            this.dataGridViewTextBoxColumn127,
            this.dataGridViewTextBoxColumn128,
            this.dataGridViewTextBoxColumn129,
            this.dataGridViewTextBoxColumn130,
            this.Column26,
            this.Column27});
            this.Table2_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Table2_2_3.Location = new System.Drawing.Point(20, 23);
            this.Table2_2_3.Margin = new System.Windows.Forms.Padding(4);
            this.Table2_2_3.Name = "Table2_2_3";
            this.Table2_2_3.RowHeadersVisible = false;
            this.Table2_2_3.RowHeadersWidth = 51;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Table2_2_3.RowsDefaultCellStyle = dataGridViewCellStyle35;
            this.Table2_2_3.Size = new System.Drawing.Size(1076, 645);
            this.Table2_2_3.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn106
            // 
            this.dataGridViewTextBoxColumn106.HeaderText = "RequireText";
            this.dataGridViewTextBoxColumn106.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn106.Name = "dataGridViewTextBoxColumn106";
            this.dataGridViewTextBoxColumn106.Width = 300;
            // 
            // dataGridViewTextBoxColumn107
            // 
            this.dataGridViewTextBoxColumn107.HeaderText = "ValueReq";
            this.dataGridViewTextBoxColumn107.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn107.Name = "dataGridViewTextBoxColumn107";
            this.dataGridViewTextBoxColumn107.Width = 50;
            // 
            // dataGridViewTextBoxColumn108
            // 
            this.dataGridViewTextBoxColumn108.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn108.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn108.Name = "dataGridViewTextBoxColumn108";
            this.dataGridViewTextBoxColumn108.Width = 50;
            // 
            // dataGridViewTextBoxColumn109
            // 
            this.dataGridViewTextBoxColumn109.HeaderText = "Column2";
            this.dataGridViewTextBoxColumn109.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn109.Name = "dataGridViewTextBoxColumn109";
            this.dataGridViewTextBoxColumn109.Width = 50;
            // 
            // dataGridViewTextBoxColumn110
            // 
            this.dataGridViewTextBoxColumn110.HeaderText = "Column3";
            this.dataGridViewTextBoxColumn110.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn110.Name = "dataGridViewTextBoxColumn110";
            this.dataGridViewTextBoxColumn110.Width = 50;
            // 
            // dataGridViewTextBoxColumn111
            // 
            this.dataGridViewTextBoxColumn111.HeaderText = "Column4";
            this.dataGridViewTextBoxColumn111.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn111.Name = "dataGridViewTextBoxColumn111";
            this.dataGridViewTextBoxColumn111.Width = 50;
            // 
            // dataGridViewTextBoxColumn112
            // 
            this.dataGridViewTextBoxColumn112.HeaderText = "Column5";
            this.dataGridViewTextBoxColumn112.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn112.Name = "dataGridViewTextBoxColumn112";
            this.dataGridViewTextBoxColumn112.Width = 50;
            // 
            // dataGridViewTextBoxColumn113
            // 
            this.dataGridViewTextBoxColumn113.HeaderText = "Column6";
            this.dataGridViewTextBoxColumn113.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn113.Name = "dataGridViewTextBoxColumn113";
            this.dataGridViewTextBoxColumn113.Width = 50;
            // 
            // dataGridViewTextBoxColumn114
            // 
            this.dataGridViewTextBoxColumn114.HeaderText = "Column7";
            this.dataGridViewTextBoxColumn114.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn114.Name = "dataGridViewTextBoxColumn114";
            this.dataGridViewTextBoxColumn114.Width = 50;
            // 
            // dataGridViewTextBoxColumn115
            // 
            this.dataGridViewTextBoxColumn115.HeaderText = "Column8";
            this.dataGridViewTextBoxColumn115.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn115.Name = "dataGridViewTextBoxColumn115";
            this.dataGridViewTextBoxColumn115.Width = 50;
            // 
            // dataGridViewTextBoxColumn116
            // 
            this.dataGridViewTextBoxColumn116.HeaderText = "Column9";
            this.dataGridViewTextBoxColumn116.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn116.Name = "dataGridViewTextBoxColumn116";
            this.dataGridViewTextBoxColumn116.Width = 50;
            // 
            // dataGridViewTextBoxColumn117
            // 
            this.dataGridViewTextBoxColumn117.HeaderText = "Column10";
            this.dataGridViewTextBoxColumn117.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn117.Name = "dataGridViewTextBoxColumn117";
            this.dataGridViewTextBoxColumn117.Width = 50;
            // 
            // dataGridViewTextBoxColumn118
            // 
            this.dataGridViewTextBoxColumn118.HeaderText = "Column11";
            this.dataGridViewTextBoxColumn118.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn118.Name = "dataGridViewTextBoxColumn118";
            this.dataGridViewTextBoxColumn118.Width = 50;
            // 
            // dataGridViewTextBoxColumn119
            // 
            this.dataGridViewTextBoxColumn119.HeaderText = "Column12";
            this.dataGridViewTextBoxColumn119.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn119.Name = "dataGridViewTextBoxColumn119";
            this.dataGridViewTextBoxColumn119.Width = 50;
            // 
            // dataGridViewTextBoxColumn120
            // 
            this.dataGridViewTextBoxColumn120.HeaderText = "Column13";
            this.dataGridViewTextBoxColumn120.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn120.Name = "dataGridViewTextBoxColumn120";
            this.dataGridViewTextBoxColumn120.Width = 50;
            // 
            // dataGridViewTextBoxColumn121
            // 
            this.dataGridViewTextBoxColumn121.HeaderText = "Column14";
            this.dataGridViewTextBoxColumn121.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn121.Name = "dataGridViewTextBoxColumn121";
            this.dataGridViewTextBoxColumn121.Width = 50;
            // 
            // dataGridViewTextBoxColumn122
            // 
            this.dataGridViewTextBoxColumn122.HeaderText = "Column15";
            this.dataGridViewTextBoxColumn122.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn122.Name = "dataGridViewTextBoxColumn122";
            this.dataGridViewTextBoxColumn122.Width = 50;
            // 
            // dataGridViewTextBoxColumn123
            // 
            this.dataGridViewTextBoxColumn123.HeaderText = "Column16";
            this.dataGridViewTextBoxColumn123.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn123.Name = "dataGridViewTextBoxColumn123";
            this.dataGridViewTextBoxColumn123.Width = 50;
            // 
            // dataGridViewTextBoxColumn124
            // 
            this.dataGridViewTextBoxColumn124.HeaderText = "Column17";
            this.dataGridViewTextBoxColumn124.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn124.Name = "dataGridViewTextBoxColumn124";
            this.dataGridViewTextBoxColumn124.Width = 50;
            // 
            // dataGridViewTextBoxColumn125
            // 
            this.dataGridViewTextBoxColumn125.HeaderText = "Column18";
            this.dataGridViewTextBoxColumn125.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn125.Name = "dataGridViewTextBoxColumn125";
            this.dataGridViewTextBoxColumn125.Width = 50;
            // 
            // dataGridViewTextBoxColumn126
            // 
            this.dataGridViewTextBoxColumn126.HeaderText = "Column19";
            this.dataGridViewTextBoxColumn126.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn126.Name = "dataGridViewTextBoxColumn126";
            this.dataGridViewTextBoxColumn126.Width = 50;
            // 
            // dataGridViewTextBoxColumn127
            // 
            this.dataGridViewTextBoxColumn127.HeaderText = "Column20";
            this.dataGridViewTextBoxColumn127.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn127.Name = "dataGridViewTextBoxColumn127";
            this.dataGridViewTextBoxColumn127.Width = 50;
            // 
            // dataGridViewTextBoxColumn128
            // 
            this.dataGridViewTextBoxColumn128.HeaderText = "Column21";
            this.dataGridViewTextBoxColumn128.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn128.Name = "dataGridViewTextBoxColumn128";
            this.dataGridViewTextBoxColumn128.Width = 50;
            // 
            // dataGridViewTextBoxColumn129
            // 
            this.dataGridViewTextBoxColumn129.HeaderText = "Column22";
            this.dataGridViewTextBoxColumn129.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn129.Name = "dataGridViewTextBoxColumn129";
            this.dataGridViewTextBoxColumn129.Width = 50;
            // 
            // dataGridViewTextBoxColumn130
            // 
            this.dataGridViewTextBoxColumn130.HeaderText = "Column23";
            this.dataGridViewTextBoxColumn130.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn130.Name = "dataGridViewTextBoxColumn130";
            this.dataGridViewTextBoxColumn130.Width = 50;
            // 
            // Column26
            // 
            this.Column26.HeaderText = "Column26";
            this.Column26.MinimumWidth = 6;
            this.Column26.Name = "Column26";
            this.Column26.Width = 50;
            // 
            // Column27
            // 
            this.Column27.HeaderText = "Column27";
            this.Column27.MinimumWidth = 6;
            this.Column27.Name = "Column27";
            this.Column27.Width = 125;
            // 
            // groupBoxTable2_2_2
            // 
            this.groupBoxTable2_2_2.Controls.Add(this.Table2_2_2);
            this.groupBoxTable2_2_2.Location = new System.Drawing.Point(216, 50);
            this.groupBoxTable2_2_2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_2_2.Name = "groupBoxTable2_2_2";
            this.groupBoxTable2_2_2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_2_2.Size = new System.Drawing.Size(1129, 697);
            this.groupBoxTable2_2_2.TabIndex = 24;
            this.groupBoxTable2_2_2.TabStop = false;
            this.groupBoxTable2_2_2.Text = "Множина настання вартісних ризикових подій";
            this.groupBoxTable2_2_2.Visible = false;
            // 
            // Table2_2_2
            // 
            this.Table2_2_2.AllowUserToAddRows = false;
            this.Table2_2_2.AllowUserToDeleteRows = false;
            this.Table2_2_2.AllowUserToResizeRows = false;
            this.Table2_2_2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Table2_2_2.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.Table2_2_2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Table2_2_2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table2_2_2.ColumnHeadersVisible = false;
            this.Table2_2_2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn131,
            this.dataGridViewTextBoxColumn132,
            this.dataGridViewTextBoxColumn133,
            this.dataGridViewTextBoxColumn134,
            this.dataGridViewTextBoxColumn135,
            this.dataGridViewTextBoxColumn136,
            this.dataGridViewTextBoxColumn137,
            this.dataGridViewTextBoxColumn138,
            this.dataGridViewTextBoxColumn139,
            this.dataGridViewTextBoxColumn140,
            this.dataGridViewTextBoxColumn141,
            this.dataGridViewTextBoxColumn142,
            this.dataGridViewTextBoxColumn143,
            this.dataGridViewTextBoxColumn144,
            this.dataGridViewTextBoxColumn145,
            this.dataGridViewTextBoxColumn146,
            this.dataGridViewTextBoxColumn147,
            this.dataGridViewTextBoxColumn148,
            this.dataGridViewTextBoxColumn149,
            this.dataGridViewTextBoxColumn150,
            this.dataGridViewTextBoxColumn151,
            this.dataGridViewTextBoxColumn152,
            this.dataGridViewTextBoxColumn153,
            this.dataGridViewTextBoxColumn154,
            this.dataGridViewTextBoxColumn155,
            this.Column28,
            this.Column29});
            this.Table2_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Table2_2_2.Location = new System.Drawing.Point(20, 23);
            this.Table2_2_2.Margin = new System.Windows.Forms.Padding(4);
            this.Table2_2_2.Name = "Table2_2_2";
            this.Table2_2_2.RowHeadersVisible = false;
            this.Table2_2_2.RowHeadersWidth = 51;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Table2_2_2.RowsDefaultCellStyle = dataGridViewCellStyle36;
            this.Table2_2_2.Size = new System.Drawing.Size(1076, 645);
            this.Table2_2_2.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn131
            // 
            this.dataGridViewTextBoxColumn131.HeaderText = "RequireText";
            this.dataGridViewTextBoxColumn131.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn131.Name = "dataGridViewTextBoxColumn131";
            this.dataGridViewTextBoxColumn131.Width = 300;
            // 
            // dataGridViewTextBoxColumn132
            // 
            this.dataGridViewTextBoxColumn132.HeaderText = "ValueReq";
            this.dataGridViewTextBoxColumn132.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn132.Name = "dataGridViewTextBoxColumn132";
            this.dataGridViewTextBoxColumn132.Width = 50;
            // 
            // dataGridViewTextBoxColumn133
            // 
            this.dataGridViewTextBoxColumn133.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn133.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn133.Name = "dataGridViewTextBoxColumn133";
            this.dataGridViewTextBoxColumn133.Width = 50;
            // 
            // dataGridViewTextBoxColumn134
            // 
            this.dataGridViewTextBoxColumn134.HeaderText = "Column2";
            this.dataGridViewTextBoxColumn134.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn134.Name = "dataGridViewTextBoxColumn134";
            this.dataGridViewTextBoxColumn134.Width = 50;
            // 
            // dataGridViewTextBoxColumn135
            // 
            this.dataGridViewTextBoxColumn135.HeaderText = "Column3";
            this.dataGridViewTextBoxColumn135.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn135.Name = "dataGridViewTextBoxColumn135";
            this.dataGridViewTextBoxColumn135.Width = 50;
            // 
            // dataGridViewTextBoxColumn136
            // 
            this.dataGridViewTextBoxColumn136.HeaderText = "Column4";
            this.dataGridViewTextBoxColumn136.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn136.Name = "dataGridViewTextBoxColumn136";
            this.dataGridViewTextBoxColumn136.Width = 50;
            // 
            // dataGridViewTextBoxColumn137
            // 
            this.dataGridViewTextBoxColumn137.HeaderText = "Column5";
            this.dataGridViewTextBoxColumn137.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn137.Name = "dataGridViewTextBoxColumn137";
            this.dataGridViewTextBoxColumn137.Width = 50;
            // 
            // dataGridViewTextBoxColumn138
            // 
            this.dataGridViewTextBoxColumn138.HeaderText = "Column6";
            this.dataGridViewTextBoxColumn138.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn138.Name = "dataGridViewTextBoxColumn138";
            this.dataGridViewTextBoxColumn138.Width = 50;
            // 
            // dataGridViewTextBoxColumn139
            // 
            this.dataGridViewTextBoxColumn139.HeaderText = "Column7";
            this.dataGridViewTextBoxColumn139.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn139.Name = "dataGridViewTextBoxColumn139";
            this.dataGridViewTextBoxColumn139.Width = 50;
            // 
            // dataGridViewTextBoxColumn140
            // 
            this.dataGridViewTextBoxColumn140.HeaderText = "Column8";
            this.dataGridViewTextBoxColumn140.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn140.Name = "dataGridViewTextBoxColumn140";
            this.dataGridViewTextBoxColumn140.Width = 50;
            // 
            // dataGridViewTextBoxColumn141
            // 
            this.dataGridViewTextBoxColumn141.HeaderText = "Column9";
            this.dataGridViewTextBoxColumn141.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn141.Name = "dataGridViewTextBoxColumn141";
            this.dataGridViewTextBoxColumn141.Width = 50;
            // 
            // dataGridViewTextBoxColumn142
            // 
            this.dataGridViewTextBoxColumn142.HeaderText = "Column10";
            this.dataGridViewTextBoxColumn142.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn142.Name = "dataGridViewTextBoxColumn142";
            this.dataGridViewTextBoxColumn142.Width = 50;
            // 
            // dataGridViewTextBoxColumn143
            // 
            this.dataGridViewTextBoxColumn143.HeaderText = "Column11";
            this.dataGridViewTextBoxColumn143.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn143.Name = "dataGridViewTextBoxColumn143";
            this.dataGridViewTextBoxColumn143.Width = 50;
            // 
            // dataGridViewTextBoxColumn144
            // 
            this.dataGridViewTextBoxColumn144.HeaderText = "Column12";
            this.dataGridViewTextBoxColumn144.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn144.Name = "dataGridViewTextBoxColumn144";
            this.dataGridViewTextBoxColumn144.Width = 50;
            // 
            // dataGridViewTextBoxColumn145
            // 
            this.dataGridViewTextBoxColumn145.HeaderText = "Column13";
            this.dataGridViewTextBoxColumn145.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn145.Name = "dataGridViewTextBoxColumn145";
            this.dataGridViewTextBoxColumn145.Width = 50;
            // 
            // dataGridViewTextBoxColumn146
            // 
            this.dataGridViewTextBoxColumn146.HeaderText = "Column14";
            this.dataGridViewTextBoxColumn146.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn146.Name = "dataGridViewTextBoxColumn146";
            this.dataGridViewTextBoxColumn146.Width = 50;
            // 
            // dataGridViewTextBoxColumn147
            // 
            this.dataGridViewTextBoxColumn147.HeaderText = "Column15";
            this.dataGridViewTextBoxColumn147.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn147.Name = "dataGridViewTextBoxColumn147";
            this.dataGridViewTextBoxColumn147.Width = 50;
            // 
            // dataGridViewTextBoxColumn148
            // 
            this.dataGridViewTextBoxColumn148.HeaderText = "Column16";
            this.dataGridViewTextBoxColumn148.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn148.Name = "dataGridViewTextBoxColumn148";
            this.dataGridViewTextBoxColumn148.Width = 50;
            // 
            // dataGridViewTextBoxColumn149
            // 
            this.dataGridViewTextBoxColumn149.HeaderText = "Column17";
            this.dataGridViewTextBoxColumn149.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn149.Name = "dataGridViewTextBoxColumn149";
            this.dataGridViewTextBoxColumn149.Width = 50;
            // 
            // dataGridViewTextBoxColumn150
            // 
            this.dataGridViewTextBoxColumn150.HeaderText = "Column18";
            this.dataGridViewTextBoxColumn150.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn150.Name = "dataGridViewTextBoxColumn150";
            this.dataGridViewTextBoxColumn150.Width = 50;
            // 
            // dataGridViewTextBoxColumn151
            // 
            this.dataGridViewTextBoxColumn151.HeaderText = "Column19";
            this.dataGridViewTextBoxColumn151.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn151.Name = "dataGridViewTextBoxColumn151";
            this.dataGridViewTextBoxColumn151.Width = 50;
            // 
            // dataGridViewTextBoxColumn152
            // 
            this.dataGridViewTextBoxColumn152.HeaderText = "Column20";
            this.dataGridViewTextBoxColumn152.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn152.Name = "dataGridViewTextBoxColumn152";
            this.dataGridViewTextBoxColumn152.Width = 50;
            // 
            // dataGridViewTextBoxColumn153
            // 
            this.dataGridViewTextBoxColumn153.HeaderText = "Column21";
            this.dataGridViewTextBoxColumn153.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn153.Name = "dataGridViewTextBoxColumn153";
            this.dataGridViewTextBoxColumn153.Width = 50;
            // 
            // dataGridViewTextBoxColumn154
            // 
            this.dataGridViewTextBoxColumn154.HeaderText = "Column22";
            this.dataGridViewTextBoxColumn154.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn154.Name = "dataGridViewTextBoxColumn154";
            this.dataGridViewTextBoxColumn154.Width = 50;
            // 
            // dataGridViewTextBoxColumn155
            // 
            this.dataGridViewTextBoxColumn155.HeaderText = "Column23";
            this.dataGridViewTextBoxColumn155.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn155.Name = "dataGridViewTextBoxColumn155";
            this.dataGridViewTextBoxColumn155.Width = 50;
            // 
            // Column28
            // 
            this.Column28.HeaderText = "Column28";
            this.Column28.MinimumWidth = 6;
            this.Column28.Name = "Column28";
            this.Column28.Width = 50;
            // 
            // Column29
            // 
            this.Column29.HeaderText = "Column29";
            this.Column29.MinimumWidth = 6;
            this.Column29.Name = "Column29";
            this.Column29.Width = 125;
            // 
            // groupBoxTable2_2_1
            // 
            this.groupBoxTable2_2_1.Controls.Add(this.Table2_2_1);
            this.groupBoxTable2_2_1.Location = new System.Drawing.Point(216, 50);
            this.groupBoxTable2_2_1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_2_1.Name = "groupBoxTable2_2_1";
            this.groupBoxTable2_2_1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable2_2_1.Size = new System.Drawing.Size(1129, 697);
            this.groupBoxTable2_2_1.TabIndex = 23;
            this.groupBoxTable2_2_1.TabStop = false;
            this.groupBoxTable2_2_1.Text = "Множина настання технічних ризикових подій";
            this.groupBoxTable2_2_1.Visible = false;
            // 
            // Table2_2_1
            // 
            this.Table2_2_1.AllowUserToAddRows = false;
            this.Table2_2_1.AllowUserToDeleteRows = false;
            this.Table2_2_1.AllowUserToResizeRows = false;
            this.Table2_2_1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Table2_2_1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.Table2_2_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Table2_2_1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table2_2_1.ColumnHeadersVisible = false;
            this.Table2_2_1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn156,
            this.dataGridViewTextBoxColumn157,
            this.dataGridViewTextBoxColumn158,
            this.dataGridViewTextBoxColumn159,
            this.dataGridViewTextBoxColumn160,
            this.dataGridViewTextBoxColumn161,
            this.dataGridViewTextBoxColumn162,
            this.dataGridViewTextBoxColumn163,
            this.dataGridViewTextBoxColumn164,
            this.dataGridViewTextBoxColumn165,
            this.dataGridViewTextBoxColumn166,
            this.dataGridViewTextBoxColumn167,
            this.dataGridViewTextBoxColumn168,
            this.dataGridViewTextBoxColumn169,
            this.dataGridViewTextBoxColumn170,
            this.dataGridViewTextBoxColumn171,
            this.dataGridViewTextBoxColumn172,
            this.dataGridViewTextBoxColumn173,
            this.dataGridViewTextBoxColumn174,
            this.dataGridViewTextBoxColumn175,
            this.dataGridViewTextBoxColumn176,
            this.dataGridViewTextBoxColumn177,
            this.dataGridViewTextBoxColumn178,
            this.dataGridViewTextBoxColumn179,
            this.dataGridViewTextBoxColumn180,
            this.Column30,
            this.Column31});
            this.Table2_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Table2_2_1.Location = new System.Drawing.Point(20, 23);
            this.Table2_2_1.Margin = new System.Windows.Forms.Padding(4);
            this.Table2_2_1.Name = "Table2_2_1";
            this.Table2_2_1.RowHeadersVisible = false;
            this.Table2_2_1.RowHeadersWidth = 51;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Table2_2_1.RowsDefaultCellStyle = dataGridViewCellStyle37;
            this.Table2_2_1.Size = new System.Drawing.Size(1076, 645);
            this.Table2_2_1.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn156
            // 
            this.dataGridViewTextBoxColumn156.HeaderText = "RequireText";
            this.dataGridViewTextBoxColumn156.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn156.Name = "dataGridViewTextBoxColumn156";
            this.dataGridViewTextBoxColumn156.Width = 300;
            // 
            // dataGridViewTextBoxColumn157
            // 
            this.dataGridViewTextBoxColumn157.HeaderText = "ValueReq";
            this.dataGridViewTextBoxColumn157.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn157.Name = "dataGridViewTextBoxColumn157";
            this.dataGridViewTextBoxColumn157.Width = 50;
            // 
            // dataGridViewTextBoxColumn158
            // 
            this.dataGridViewTextBoxColumn158.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn158.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn158.Name = "dataGridViewTextBoxColumn158";
            this.dataGridViewTextBoxColumn158.Width = 50;
            // 
            // dataGridViewTextBoxColumn159
            // 
            this.dataGridViewTextBoxColumn159.HeaderText = "Column2";
            this.dataGridViewTextBoxColumn159.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn159.Name = "dataGridViewTextBoxColumn159";
            this.dataGridViewTextBoxColumn159.Width = 50;
            // 
            // dataGridViewTextBoxColumn160
            // 
            this.dataGridViewTextBoxColumn160.HeaderText = "Column3";
            this.dataGridViewTextBoxColumn160.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn160.Name = "dataGridViewTextBoxColumn160";
            this.dataGridViewTextBoxColumn160.Width = 50;
            // 
            // dataGridViewTextBoxColumn161
            // 
            this.dataGridViewTextBoxColumn161.HeaderText = "Column4";
            this.dataGridViewTextBoxColumn161.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn161.Name = "dataGridViewTextBoxColumn161";
            this.dataGridViewTextBoxColumn161.Width = 50;
            // 
            // dataGridViewTextBoxColumn162
            // 
            this.dataGridViewTextBoxColumn162.HeaderText = "Column5";
            this.dataGridViewTextBoxColumn162.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn162.Name = "dataGridViewTextBoxColumn162";
            this.dataGridViewTextBoxColumn162.Width = 50;
            // 
            // dataGridViewTextBoxColumn163
            // 
            this.dataGridViewTextBoxColumn163.HeaderText = "Column6";
            this.dataGridViewTextBoxColumn163.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn163.Name = "dataGridViewTextBoxColumn163";
            this.dataGridViewTextBoxColumn163.Width = 50;
            // 
            // dataGridViewTextBoxColumn164
            // 
            this.dataGridViewTextBoxColumn164.HeaderText = "Column7";
            this.dataGridViewTextBoxColumn164.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn164.Name = "dataGridViewTextBoxColumn164";
            this.dataGridViewTextBoxColumn164.Width = 50;
            // 
            // dataGridViewTextBoxColumn165
            // 
            this.dataGridViewTextBoxColumn165.HeaderText = "Column8";
            this.dataGridViewTextBoxColumn165.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn165.Name = "dataGridViewTextBoxColumn165";
            this.dataGridViewTextBoxColumn165.Width = 50;
            // 
            // dataGridViewTextBoxColumn166
            // 
            this.dataGridViewTextBoxColumn166.HeaderText = "Column9";
            this.dataGridViewTextBoxColumn166.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn166.Name = "dataGridViewTextBoxColumn166";
            this.dataGridViewTextBoxColumn166.Width = 50;
            // 
            // dataGridViewTextBoxColumn167
            // 
            this.dataGridViewTextBoxColumn167.HeaderText = "Column10";
            this.dataGridViewTextBoxColumn167.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn167.Name = "dataGridViewTextBoxColumn167";
            this.dataGridViewTextBoxColumn167.Width = 50;
            // 
            // dataGridViewTextBoxColumn168
            // 
            this.dataGridViewTextBoxColumn168.HeaderText = "Column11";
            this.dataGridViewTextBoxColumn168.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn168.Name = "dataGridViewTextBoxColumn168";
            this.dataGridViewTextBoxColumn168.Width = 50;
            // 
            // dataGridViewTextBoxColumn169
            // 
            this.dataGridViewTextBoxColumn169.HeaderText = "Column12";
            this.dataGridViewTextBoxColumn169.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn169.Name = "dataGridViewTextBoxColumn169";
            this.dataGridViewTextBoxColumn169.Width = 50;
            // 
            // dataGridViewTextBoxColumn170
            // 
            this.dataGridViewTextBoxColumn170.HeaderText = "Column13";
            this.dataGridViewTextBoxColumn170.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn170.Name = "dataGridViewTextBoxColumn170";
            this.dataGridViewTextBoxColumn170.Width = 50;
            // 
            // dataGridViewTextBoxColumn171
            // 
            this.dataGridViewTextBoxColumn171.HeaderText = "Column14";
            this.dataGridViewTextBoxColumn171.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn171.Name = "dataGridViewTextBoxColumn171";
            this.dataGridViewTextBoxColumn171.Width = 50;
            // 
            // dataGridViewTextBoxColumn172
            // 
            this.dataGridViewTextBoxColumn172.HeaderText = "Column15";
            this.dataGridViewTextBoxColumn172.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn172.Name = "dataGridViewTextBoxColumn172";
            this.dataGridViewTextBoxColumn172.Width = 50;
            // 
            // dataGridViewTextBoxColumn173
            // 
            this.dataGridViewTextBoxColumn173.HeaderText = "Column16";
            this.dataGridViewTextBoxColumn173.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn173.Name = "dataGridViewTextBoxColumn173";
            this.dataGridViewTextBoxColumn173.Width = 50;
            // 
            // dataGridViewTextBoxColumn174
            // 
            this.dataGridViewTextBoxColumn174.HeaderText = "Column17";
            this.dataGridViewTextBoxColumn174.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn174.Name = "dataGridViewTextBoxColumn174";
            this.dataGridViewTextBoxColumn174.Width = 50;
            // 
            // dataGridViewTextBoxColumn175
            // 
            this.dataGridViewTextBoxColumn175.HeaderText = "Column18";
            this.dataGridViewTextBoxColumn175.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn175.Name = "dataGridViewTextBoxColumn175";
            this.dataGridViewTextBoxColumn175.Width = 50;
            // 
            // dataGridViewTextBoxColumn176
            // 
            this.dataGridViewTextBoxColumn176.HeaderText = "Column19";
            this.dataGridViewTextBoxColumn176.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn176.Name = "dataGridViewTextBoxColumn176";
            this.dataGridViewTextBoxColumn176.Width = 50;
            // 
            // dataGridViewTextBoxColumn177
            // 
            this.dataGridViewTextBoxColumn177.HeaderText = "Column20";
            this.dataGridViewTextBoxColumn177.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn177.Name = "dataGridViewTextBoxColumn177";
            this.dataGridViewTextBoxColumn177.Width = 50;
            // 
            // dataGridViewTextBoxColumn178
            // 
            this.dataGridViewTextBoxColumn178.HeaderText = "Column21";
            this.dataGridViewTextBoxColumn178.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn178.Name = "dataGridViewTextBoxColumn178";
            this.dataGridViewTextBoxColumn178.Width = 50;
            // 
            // dataGridViewTextBoxColumn179
            // 
            this.dataGridViewTextBoxColumn179.HeaderText = "Column22";
            this.dataGridViewTextBoxColumn179.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn179.Name = "dataGridViewTextBoxColumn179";
            this.dataGridViewTextBoxColumn179.Width = 50;
            // 
            // dataGridViewTextBoxColumn180
            // 
            this.dataGridViewTextBoxColumn180.HeaderText = "Column23";
            this.dataGridViewTextBoxColumn180.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn180.Name = "dataGridViewTextBoxColumn180";
            this.dataGridViewTextBoxColumn180.Width = 50;
            // 
            // Column30
            // 
            this.Column30.HeaderText = "Column30";
            this.Column30.MinimumWidth = 6;
            this.Column30.Name = "Column30";
            this.Column30.Width = 50;
            // 
            // Column31
            // 
            this.Column31.HeaderText = "Column31";
            this.Column31.MinimumWidth = 6;
            this.Column31.Name = "Column31";
            this.Column31.Width = 125;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1393, 370);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.MaximumSize = new System.Drawing.Size(173, 86);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(157, 42);
            this.label13.TabIndex = 20;
            this.label13.Text = "Додаткова вартість, тис. грн";
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.Color.Lime;
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox9.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox9.Enabled = false;
            this.textBox9.Location = new System.Drawing.Point(1353, 375);
            this.textBox9.Margin = new System.Windows.Forms.Padding(4);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(37, 27);
            this.textBox9.TabIndex = 19;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(1393, 300);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.MaximumSize = new System.Drawing.Size(213, 86);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(153, 63);
            this.label14.TabIndex = 18;
            this.label14.Text = "Оцінки експертів з урахуванням їхньої вагомості";
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.Color.CornflowerBlue;
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox10.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox10.Enabled = false;
            this.textBox10.Location = new System.Drawing.Point(1353, 313);
            this.textBox10.Margin = new System.Windows.Forms.Padding(4);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(37, 27);
            this.textBox10.TabIndex = 17;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(1393, 219);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(0, 21);
            this.label15.TabIndex = 16;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(1393, 261);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(171, 21);
            this.label16.TabIndex = 15;
            this.label16.Text = "Сума оцінок експертів";
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.Color.CadetBlue;
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox11.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox11.Enabled = false;
            this.textBox11.Location = new System.Drawing.Point(1353, 261);
            this.textBox11.Margin = new System.Windows.Forms.Padding(4);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(37, 27);
            this.textBox11.TabIndex = 14;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(1393, 201);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.MaximumSize = new System.Drawing.Size(187, 86);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(173, 42);
            this.label17.TabIndex = 13;
            this.label17.Text = "Середнє значення коефіцієнтів вагомості";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(1393, 160);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(132, 21);
            this.label18.TabIndex = 12;
            this.label18.Text = "Оцінки експертів";
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.Color.Aquamarine;
            this.textBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox12.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox12.Enabled = false;
            this.textBox12.Location = new System.Drawing.Point(1353, 206);
            this.textBox12.Margin = new System.Windows.Forms.Padding(4);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(37, 27);
            this.textBox12.TabIndex = 11;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(1393, 169);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(0, 21);
            this.label19.TabIndex = 9;
            // 
            // textBox13
            // 
            this.textBox13.BackColor = System.Drawing.Color.Green;
            this.textBox13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox13.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox13.Enabled = false;
            this.textBox13.Location = new System.Drawing.Point(1353, 155);
            this.textBox13.Margin = new System.Windows.Forms.Padding(4);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(37, 27);
            this.textBox13.TabIndex = 8;
            // 
            // buttonTable2_2_4
            // 
            this.buttonTable2_2_4.BackColor = System.Drawing.Color.NavajoWhite;
            this.buttonTable2_2_4.Location = new System.Drawing.Point(22, 364);
            this.buttonTable2_2_4.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTable2_2_4.Name = "buttonTable2_2_4";
            this.buttonTable2_2_4.Size = new System.Drawing.Size(173, 123);
            this.buttonTable2_2_4.TabIndex = 6;
            this.buttonTable2_2_4.Text = "Множина настання ризикових подій реалізації процесу управління програмним проекто" +
    "м";
            this.buttonTable2_2_4.UseVisualStyleBackColor = false;
            this.buttonTable2_2_4.Click += new System.EventHandler(this.buttonTable2_2_4_Click);
            // 
            // buttonTable2_2_3
            // 
            this.buttonTable2_2_3.BackColor = System.Drawing.Color.NavajoWhite;
            this.buttonTable2_2_3.Location = new System.Drawing.Point(22, 272);
            this.buttonTable2_2_3.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTable2_2_3.Name = "buttonTable2_2_3";
            this.buttonTable2_2_3.Size = new System.Drawing.Size(173, 74);
            this.buttonTable2_2_3.TabIndex = 5;
            this.buttonTable2_2_3.Text = "Множина настання планових ризикових подій";
            this.buttonTable2_2_3.UseVisualStyleBackColor = false;
            this.buttonTable2_2_3.Click += new System.EventHandler(this.buttonTable2_2_3_Click);
            // 
            // buttonTable2_2_2
            // 
            this.buttonTable2_2_2.BackColor = System.Drawing.Color.NavajoWhite;
            this.buttonTable2_2_2.Location = new System.Drawing.Point(22, 179);
            this.buttonTable2_2_2.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTable2_2_2.Name = "buttonTable2_2_2";
            this.buttonTable2_2_2.Size = new System.Drawing.Size(173, 73);
            this.buttonTable2_2_2.TabIndex = 4;
            this.buttonTable2_2_2.Text = "Множина настання вартісних ризикових подій";
            this.buttonTable2_2_2.UseVisualStyleBackColor = false;
            this.buttonTable2_2_2.Click += new System.EventHandler(this.buttonTable2_2_2_Click);
            // 
            // buttonTable2_2_1
            // 
            this.buttonTable2_2_1.BackColor = System.Drawing.Color.NavajoWhite;
            this.buttonTable2_2_1.Location = new System.Drawing.Point(22, 87);
            this.buttonTable2_2_1.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTable2_2_1.Name = "buttonTable2_2_1";
            this.buttonTable2_2_1.Size = new System.Drawing.Size(173, 74);
            this.buttonTable2_2_1.TabIndex = 3;
            this.buttonTable2_2_1.Text = "Множина настання технічних ризикових подій";
            this.buttonTable2_2_1.UseVisualStyleBackColor = false;
            this.buttonTable2_2_1.Click += new System.EventHandler(this.buttonTable2_2_1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBoxTable3_1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1571, 771);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Етап 3";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBoxTable3_1
            // 
            this.groupBoxTable3_1.BackColor = System.Drawing.Color.PapayaWhip;
            this.groupBoxTable3_1.Controls.Add(this.Table3_1);
            this.groupBoxTable3_1.Controls.Add(this.label42);
            this.groupBoxTable3_1.Controls.Add(this.label46);
            this.groupBoxTable3_1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxTable3_1.Location = new System.Drawing.Point(3, 3);
            this.groupBoxTable3_1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable3_1.Name = "groupBoxTable3_1";
            this.groupBoxTable3_1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable3_1.Size = new System.Drawing.Size(1576, 763);
            this.groupBoxTable3_1.TabIndex = 8;
            this.groupBoxTable3_1.TabStop = false;
            this.groupBoxTable3_1.Text = "Заходи із зменшення або усунення ризику";
            this.groupBoxTable3_1.Visible = false;
            // 
            // Table3_1
            // 
            this.Table3_1.AllowUserToAddRows = false;
            this.Table3_1.AllowUserToDeleteRows = false;
            this.Table3_1.AllowUserToResizeRows = false;
            this.Table3_1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Table3_1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Table3_1.BackgroundColor = System.Drawing.Color.PapayaWhip;
            this.Table3_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Table3_1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table3_1.ColumnHeadersVisible = false;
            this.Table3_1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn274,
            this.dataGridViewTextBoxColumn275,
            this.dataGridViewTextBoxColumn276,
            this.dataGridViewTextBoxColumn277,
            this.Column34});
            this.Table3_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Table3_1.Location = new System.Drawing.Point(17, 31);
            this.Table3_1.Margin = new System.Windows.Forms.Padding(4);
            this.Table3_1.Name = "Table3_1";
            this.Table3_1.RowHeadersVisible = false;
            this.Table3_1.RowHeadersWidth = 51;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Table3_1.RowsDefaultCellStyle = dataGridViewCellStyle38;
            this.Table3_1.Size = new System.Drawing.Size(1499, 700);
            this.Table3_1.TabIndex = 17;
            this.Table3_1.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.Table3_1_CellEnter);
            // 
            // dataGridViewTextBoxColumn274
            // 
            this.dataGridViewTextBoxColumn274.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn274.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn274.Name = "dataGridViewTextBoxColumn274";
            // 
            // dataGridViewTextBoxColumn275
            // 
            this.dataGridViewTextBoxColumn275.HeaderText = "Column2";
            this.dataGridViewTextBoxColumn275.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn275.Name = "dataGridViewTextBoxColumn275";
            // 
            // dataGridViewTextBoxColumn276
            // 
            this.dataGridViewTextBoxColumn276.HeaderText = "Column3";
            this.dataGridViewTextBoxColumn276.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn276.Name = "dataGridViewTextBoxColumn276";
            // 
            // dataGridViewTextBoxColumn277
            // 
            this.dataGridViewTextBoxColumn277.HeaderText = "Column4";
            this.dataGridViewTextBoxColumn277.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn277.Name = "dataGridViewTextBoxColumn277";
            // 
            // Column34
            // 
            this.Column34.HeaderText = "Column5";
            this.Column34.MinimumWidth = 6;
            this.Column34.Name = "Column34";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(1393, 219);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(0, 21);
            this.label42.TabIndex = 16;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(1393, 169);
            this.label46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(0, 21);
            this.label46.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBoxTable4_1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1571, 771);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Етап 4";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBoxTable4_1
            // 
            this.groupBoxTable4_1.BackColor = System.Drawing.Color.PapayaWhip;
            this.groupBoxTable4_1.Controls.Add(this.panel2);
            this.groupBoxTable4_1.Controls.Add(this.label26);
            this.groupBoxTable4_1.Controls.Add(this.label27);
            this.groupBoxTable4_1.Controls.Add(this.Table4_1);
            this.groupBoxTable4_1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxTable4_1.Location = new System.Drawing.Point(3, 3);
            this.groupBoxTable4_1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTable4_1.Name = "groupBoxTable4_1";
            this.groupBoxTable4_1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTable4_1.Size = new System.Drawing.Size(1576, 763);
            this.groupBoxTable4_1.TabIndex = 9;
            this.groupBoxTable4_1.TabStop = false;
            this.groupBoxTable4_1.Text = "Моніторинг ризиків розроблення ПЗ";
            this.groupBoxTable4_1.Visible = false;
            // 
            // lowLevelBefore
            // 
            this.lowLevelBefore.AutoSize = true;
            this.lowLevelBefore.Location = new System.Drawing.Point(98, 8);
            this.lowLevelBefore.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lowLevelBefore.Name = "lowLevelBefore";
            this.lowLevelBefore.Size = new System.Drawing.Size(115, 21);
            this.lowLevelBefore.TabIndex = 58;
            this.lowLevelBefore.Text = "lowLevelBefore";
            // 
            // MediumLevelBefore
            // 
            this.MediumLevelBefore.AutoSize = true;
            this.MediumLevelBefore.Location = new System.Drawing.Point(103, 11);
            this.MediumLevelBefore.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.MediumLevelBefore.Name = "MediumLevelBefore";
            this.MediumLevelBefore.Size = new System.Drawing.Size(149, 21);
            this.MediumLevelBefore.TabIndex = 57;
            this.MediumLevelBefore.Text = "MediumLevelBefore";
            // 
            // HighLevelBefore
            // 
            this.HighLevelBefore.AutoSize = true;
            this.HighLevelBefore.Location = new System.Drawing.Point(100, 10);
            this.HighLevelBefore.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HighLevelBefore.Name = "HighLevelBefore";
            this.HighLevelBefore.Size = new System.Drawing.Size(122, 21);
            this.HighLevelBefore.TabIndex = 56;
            this.HighLevelBefore.Text = "HighLevelBefore";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(7, 8);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.MaximumSize = new System.Drawing.Size(173, 86);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(79, 21);
            this.label33.TabIndex = 55;
            this.label33.Text = "Високий :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(4, 10);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.MaximumSize = new System.Drawing.Size(173, 86);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(84, 21);
            this.label34.TabIndex = 54;
            this.label34.Text = "Середній :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.Location = new System.Drawing.Point(299, 0);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(380, 21);
            this.label35.TabIndex = 53;
            this.label35.Text = "Рівні пріоритетів ризиків (до застосування заходів)";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(5, 8);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.MaximumSize = new System.Drawing.Size(173, 86);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(80, 21);
            this.label36.TabIndex = 52;
            this.label36.Text = "Низький :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(1393, 219);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(0, 21);
            this.label26.TabIndex = 16;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(1393, 169);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(0, 21);
            this.label27.TabIndex = 9;
            // 
            // Table4_1
            // 
            this.Table4_1.AllowUserToAddRows = false;
            this.Table4_1.AllowUserToDeleteRows = false;
            this.Table4_1.AllowUserToResizeRows = false;
            this.Table4_1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Table4_1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Table4_1.BackgroundColor = System.Drawing.Color.PapayaWhip;
            this.Table4_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Table4_1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table4_1.ColumnHeadersVisible = false;
            this.Table4_1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn185,
            this.dataGridViewTextBoxColumn186,
            this.dataGridViewTextBoxColumn187,
            this.dataGridViewTextBoxColumn188});
            this.Table4_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Table4_1.Location = new System.Drawing.Point(19, 132);
            this.Table4_1.Margin = new System.Windows.Forms.Padding(4);
            this.Table4_1.Name = "Table4_1";
            this.Table4_1.RowHeadersVisible = false;
            this.Table4_1.RowHeadersWidth = 51;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Table4_1.RowsDefaultCellStyle = dataGridViewCellStyle39;
            this.Table4_1.Size = new System.Drawing.Size(1533, 619);
            this.Table4_1.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn185
            // 
            this.dataGridViewTextBoxColumn185.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn185.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn185.Name = "dataGridViewTextBoxColumn185";
            // 
            // dataGridViewTextBoxColumn186
            // 
            this.dataGridViewTextBoxColumn186.HeaderText = "Column2";
            this.dataGridViewTextBoxColumn186.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn186.Name = "dataGridViewTextBoxColumn186";
            // 
            // dataGridViewTextBoxColumn187
            // 
            this.dataGridViewTextBoxColumn187.HeaderText = "Column3";
            this.dataGridViewTextBoxColumn187.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn187.Name = "dataGridViewTextBoxColumn187";
            // 
            // dataGridViewTextBoxColumn188
            // 
            this.dataGridViewTextBoxColumn188.HeaderText = "Column4";
            this.dataGridViewTextBoxColumn188.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn188.Name = "dataGridViewTextBoxColumn188";
            // 
            // panel1
            // 
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.textBox7);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.textBox8);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.textBox17);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.textBox18);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.label37);
            this.panel1.Controls.Add(this.textBox19);
            this.panel1.Controls.Add(this.textBox20);
            this.panel1.Location = new System.Drawing.Point(1326, 123);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(230, 400);
            this.panel1.TabIndex = 32;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.PapayaWhip;
            this.label28.Location = new System.Drawing.Point(50, 292);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.MaximumSize = new System.Drawing.Size(173, 86);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(172, 84);
            this.label28.TabIndex = 34;
            this.label28.Text = "Середнє значення ймовірності виникнення ризикової події";
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.Gold;
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox7.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox7.Enabled = false;
            this.textBox7.Location = new System.Drawing.Point(10, 304);
            this.textBox7.Margin = new System.Windows.Forms.Padding(4);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(37, 27);
            this.textBox7.TabIndex = 33;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(50, 216);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.MaximumSize = new System.Drawing.Size(173, 86);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(172, 63);
            this.label29.TabIndex = 32;
            this.label29.Text = "Ймовірність виникнення ризикової події";
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.Color.DarkKhaki;
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox8.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox8.Enabled = false;
            this.textBox8.Location = new System.Drawing.Point(11, 228);
            this.textBox8.Margin = new System.Windows.Forms.Padding(4);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(37, 27);
            this.textBox8.TabIndex = 31;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(50, 142);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.MaximumSize = new System.Drawing.Size(213, 86);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(153, 63);
            this.label30.TabIndex = 30;
            this.label30.Text = "Оцінки експертів з урахуванням їхньої вагомості";
            // 
            // textBox17
            // 
            this.textBox17.BackColor = System.Drawing.Color.CornflowerBlue;
            this.textBox17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox17.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox17.Enabled = false;
            this.textBox17.Location = new System.Drawing.Point(10, 154);
            this.textBox17.Margin = new System.Windows.Forms.Padding(4);
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.Size = new System.Drawing.Size(37, 27);
            this.textBox17.TabIndex = 29;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(50, 110);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(171, 21);
            this.label31.TabIndex = 28;
            this.label31.Text = "Сума оцінок експертів";
            // 
            // textBox18
            // 
            this.textBox18.BackColor = System.Drawing.Color.CadetBlue;
            this.textBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox18.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox18.Enabled = false;
            this.textBox18.Location = new System.Drawing.Point(10, 106);
            this.textBox18.Margin = new System.Windows.Forms.Padding(4);
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.Size = new System.Drawing.Size(37, 27);
            this.textBox18.TabIndex = 27;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(50, 54);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.MaximumSize = new System.Drawing.Size(187, 86);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(173, 42);
            this.label32.TabIndex = 26;
            this.label32.Text = "Середнє значення коефіцієнтів вагомості";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(50, 18);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(132, 21);
            this.label37.TabIndex = 25;
            this.label37.Text = "Оцінки експертів";
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.Color.Aquamarine;
            this.textBox19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox19.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox19.Enabled = false;
            this.textBox19.Location = new System.Drawing.Point(10, 59);
            this.textBox19.Margin = new System.Windows.Forms.Padding(4);
            this.textBox19.Name = "textBox19";
            this.textBox19.ReadOnly = true;
            this.textBox19.Size = new System.Drawing.Size(37, 27);
            this.textBox19.TabIndex = 24;
            // 
            // textBox20
            // 
            this.textBox20.BackColor = System.Drawing.Color.Green;
            this.textBox20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox20.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox20.Enabled = false;
            this.textBox20.Location = new System.Drawing.Point(10, 13);
            this.textBox20.Margin = new System.Windows.Forms.Padding(4);
            this.textBox20.Name = "textBox20";
            this.textBox20.ReadOnly = true;
            this.textBox20.Size = new System.Drawing.Size(37, 27);
            this.textBox20.TabIndex = 23;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.label35);
            this.panel2.Location = new System.Drawing.Point(298, 27);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(978, 88);
            this.panel2.TabIndex = 59;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.label36);
            this.panel3.Controls.Add(this.lowLevelBefore);
            this.panel3.Location = new System.Drawing.Point(42, 30);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(238, 39);
            this.panel3.TabIndex = 59;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label34);
            this.panel4.Controls.Add(this.MediumLevelBefore);
            this.panel4.Location = new System.Drawing.Point(347, 30);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(238, 39);
            this.panel4.TabIndex = 60;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.label33);
            this.panel5.Controls.Add(this.HighLevelBefore);
            this.panel5.Location = new System.Drawing.Point(699, 30);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(238, 39);
            this.panel5.TabIndex = 61;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.NavajoWhite;
            this.ClientSize = new System.Drawing.Size(1602, 789);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Аналіз специфікації вимог та управління ризиками";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainForm_KeyPress);
            this.tabControl1.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.groupBoxTable1_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Table1_1)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.groupBoxTable1_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Table1_2)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.groupBoxTable2.ResumeLayout(false);
            this.groupBoxTable2.PerformLayout();
            this.groupBoxTable2_1_4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Table2_1_4)).EndInit();
            this.groupBoxTable2_1_3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Table2_1_3)).EndInit();
            this.groupBoxTable2_1_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Table2_1_2)).EndInit();
            this.groupBoxTable2_1_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Table2_1_1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBoxTable2_2.ResumeLayout(false);
            this.groupBoxTable2_2.PerformLayout();
            this.groupBoxTable2_2_Sum.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Table2_2_Sum)).EndInit();
            this.groupBoxTable2_2_4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Table2_2_4)).EndInit();
            this.groupBoxTable2_2_3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Table2_2_3)).EndInit();
            this.groupBoxTable2_2_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Table2_2_2)).EndInit();
            this.groupBoxTable2_2_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Table2_2_1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBoxTable3_1.ResumeLayout(false);
            this.groupBoxTable3_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table3_1)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.groupBoxTable4_1.ResumeLayout(false);
            this.groupBoxTable4_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table4_1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        public System.Windows.Forms.GroupBox groupBoxTable4_1;
        public System.Windows.Forms.Label lowLevelBefore;
        public System.Windows.Forms.Label MediumLevelBefore;
        public System.Windows.Forms.Label HighLevelBefore;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        public System.Windows.Forms.DataGridView Table4_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn185;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn186;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn187;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn188;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.GroupBox groupBoxTable3_1;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TabPage tabPage3;
        public System.Windows.Forms.GroupBox groupBoxTable2_2;
        public System.Windows.Forms.GroupBox groupBoxTable2_2_Sum;
        public System.Windows.Forms.DataGridView Table2_2_Sum;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn183;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn184;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn181;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn182;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column32;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column33;
        public System.Windows.Forms.Label minReqValue;
        public System.Windows.Forms.Label maxReqValue;
        public System.Windows.Forms.Label highLevel;
        public System.Windows.Forms.Label mediumLevel;
        public System.Windows.Forms.Label lowLevel;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Button buttonTableSum;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox14;
        public System.Windows.Forms.GroupBox groupBoxTable2_2_4;
        public System.Windows.Forms.DataGridView Table2_2_4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn81;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn82;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn83;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn84;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn85;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn86;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn87;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn88;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn89;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn90;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn91;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn92;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn93;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn94;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn95;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn96;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn97;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn98;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn99;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn100;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn101;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn102;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn103;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn104;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn105;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        public System.Windows.Forms.GroupBox groupBoxTable2_2_3;
        public System.Windows.Forms.DataGridView Table2_2_3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn106;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn107;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn108;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn109;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn110;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn111;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn112;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn113;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn114;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn115;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn116;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn117;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn118;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn119;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn120;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn121;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn122;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn123;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn124;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn125;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn126;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn127;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn128;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn129;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn130;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        public System.Windows.Forms.GroupBox groupBoxTable2_2_2;
        public System.Windows.Forms.DataGridView Table2_2_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn131;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn132;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn133;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn134;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn135;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn136;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn137;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn138;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn139;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn140;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn141;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn142;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn143;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn144;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn145;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn146;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn147;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn148;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn149;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn150;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn151;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn152;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn153;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn154;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn155;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        public System.Windows.Forms.GroupBox groupBoxTable2_2_1;
        public System.Windows.Forms.DataGridView Table2_2_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn156;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn157;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn158;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn159;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn160;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn161;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn162;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn163;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn164;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn165;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn166;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn167;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn168;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn169;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn170;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn171;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn172;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn173;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn174;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn175;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn176;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn177;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn178;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn179;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn180;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column31;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Button buttonTable2_2_4;
        private System.Windows.Forms.Button buttonTable2_2_3;
        private System.Windows.Forms.Button buttonTable2_2_2;
        private System.Windows.Forms.Button buttonTable2_2_1;
        private System.Windows.Forms.TabPage tabPage4;
        public System.Windows.Forms.GroupBox groupBoxTable2;
        public System.Windows.Forms.TextBox genRyskLevel;
        public System.Windows.Forms.Label genRyskValue;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.GroupBox groupBoxTable2_1_4;
        public System.Windows.Forms.DataGridView Table2_1_4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn59;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn61;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn62;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn63;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn64;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn65;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn66;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn67;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn68;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn69;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn70;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn71;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn72;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn73;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn74;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn75;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn76;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn77;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn78;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn79;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn80;
        public System.Windows.Forms.GroupBox groupBoxTable2_1_3;
        public System.Windows.Forms.DataGridView Table2_1_3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        public System.Windows.Forms.GroupBox groupBoxTable2_1_2;
        public System.Windows.Forms.DataGridView Table2_1_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        public System.Windows.Forms.GroupBox groupBoxTable2_1_1;
        public System.Windows.Forms.DataGridView Table2_1_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonTable2_1_4;
        private System.Windows.Forms.Button buttonTable2_1_3;
        private System.Windows.Forms.Button buttonTable2_1_2;
        private System.Windows.Forms.Button buttonTable2_1_1;
        private System.Windows.Forms.TabPage tabPage5;
        public System.Windows.Forms.GroupBox groupBoxTable1_2;
        public System.Windows.Forms.DataGridView Table1_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.TabPage tabPage6;
        public System.Windows.Forms.GroupBox groupBoxTable1_1;
        public System.Windows.Forms.DataGridView Table1_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RequireText;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValueReq;
        private System.Windows.Forms.DataGridViewTextBoxColumn PercentSum;
        public System.Windows.Forms.DataGridView Table3_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn274;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn275;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn276;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn277;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column34;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
    }
}

