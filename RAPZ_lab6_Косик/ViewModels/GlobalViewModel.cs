﻿using System;
using System.Collections.Generic;
using System.Linq;
using RAPZ_Lab6.Models;
using RAPZ_Lab6.UserControls;

namespace RAPZ_Lab6.ViewModels
{
    public class GlobalViewModel
    {
        public static WeightCoefFromExpertViewModel WeightCoefFromExpert { get; set; }
        public static List<ExpertWeightModel>? ExpertsWeights { get; set; }

        public static ComplexQualityIndicatorsViewModel ComplexQualityIndicators { get; set; }
        public static double AverageRelationalWeight => Math.Round(ExpertsWeights.Select(r => r.CoefOfWeightRelational).Average(), 2);

        public GlobalViewModel()
        {

        }
    }
}
