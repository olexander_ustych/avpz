﻿using RAPZ_Lab6.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RAPZ_Lab6.UserControls
{
    /// <summary>
    /// Interaction logic for СomplexQualityIndicators.xaml
    /// </summary>
    public partial class СomplexQualityIndicators : UserControl
    {
        public GlobalViewModel GlobalViewModelProp { get; set; } = new();
        public СomplexQualityIndicators()
        {
            //Thread.Sleep(200);s

            GlobalViewModel.WeightCoefFromExpert = WeightCoefsAndExpertsEvaluation.WeightCoefFromExpertViewModel;
            GlobalViewModel.ExpertsWeights = ExpertWeights.ExpertsWeights;
            GlobalViewModel.ComplexQualityIndicators = new(GlobalViewModel.WeightCoefFromExpert, 
                GlobalViewModel.ExpertsWeights, GlobalViewModel.AverageRelationalWeight);

            DataContext = GlobalViewModelProp;
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
