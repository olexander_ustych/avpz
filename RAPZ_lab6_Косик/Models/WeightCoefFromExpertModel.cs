﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;

namespace RAPZ_Lab6.Models
{
    public class WeightCoefFromExpertModel
    {
        public string? Critery { get; set; }
        public List<double>? Weights { get; set; } = new();
        public List<double>? Evaluations { get; set; } = new();

        private int _countOfExperts = 4;
        private int _minRandom = 4;
        private int _maxRandom = 10;
        public WeightCoefFromExpertModel()
        {
            Random random = new(Guid.NewGuid().GetHashCode());

            for (int i = 0; i < _countOfExperts; i++)
            {
                Weights.Add(random.Next(_minRandom, _maxRandom));
                double valueToInsert = random.Next(_minRandom, _maxRandom);
                valueToInsert = i == _countOfExperts - 1 ? valueToInsert + Math.Round(random.NextDouble(), 2) : valueToInsert;
                Evaluations.Add(valueToInsert);
            }
        }
    }
}
