﻿namespace RA6
{
    partial class QualityVisualizer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageWages = new System.Windows.Forms.TabPage();
            this.tabControlWages = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableWages = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableWeightness = new System.Windows.Forms.DataGridView();
            this.tabPageGrades = new System.Windows.Forms.TabPage();
            this.tabControlGrades = new System.Windows.Forms.TabControl();
            this.tabPageUsersGrades = new System.Windows.Forms.TabPage();
            this.tableUsersGrades = new System.Windows.Forms.DataGridView();
            this.tabPageAllGrades = new System.Windows.Forms.TabPage();
            this.tableAllGrades = new System.Windows.Forms.DataGridView();
            this.tabPageWagedGrades = new System.Windows.Forms.TabPage();
            this.tableWagedGrades = new System.Windows.Forms.DataGridView();
            this.tabPageDiagrams = new System.Windows.Forms.TabPage();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxGrader = new System.Windows.Forms.ComboBox();
            this.polarChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableChartData = new System.Windows.Forms.DataGridView();
            this.tabControl.SuspendLayout();
            this.tabPageWages.SuspendLayout();
            this.tabControlWages.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableWages)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableWeightness)).BeginInit();
            this.tabPageGrades.SuspendLayout();
            this.tabControlGrades.SuspendLayout();
            this.tabPageUsersGrades.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableUsersGrades)).BeginInit();
            this.tabPageAllGrades.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableAllGrades)).BeginInit();
            this.tabPageWagedGrades.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableWagedGrades)).BeginInit();
            this.tabPageDiagrams.SuspendLayout();
            this.groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.polarChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableChartData)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageWages);
            this.tabControl.Controls.Add(this.tabPageGrades);
            this.tabControl.Controls.Add(this.tabPageWagedGrades);
            this.tabControl.Controls.Add(this.tabPageDiagrams);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1317, 578);
            this.tabControl.TabIndex = 0;
            // 
            // tabPageWages
            // 
            this.tabPageWages.Controls.Add(this.tabControlWages);
            this.tabPageWages.Location = new System.Drawing.Point(4, 25);
            this.tabPageWages.Name = "tabPageWages";
            this.tabPageWages.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageWages.Size = new System.Drawing.Size(1309, 549);
            this.tabPageWages.TabIndex = 0;
            this.tabPageWages.Text = "Вагові коефіцієнти";
            this.tabPageWages.UseVisualStyleBackColor = true;
            // 
            // tabControlWages
            // 
            this.tabControlWages.Controls.Add(this.tabPage1);
            this.tabControlWages.Controls.Add(this.tabPage2);
            this.tabControlWages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlWages.Location = new System.Drawing.Point(3, 3);
            this.tabControlWages.Name = "tabControlWages";
            this.tabControlWages.SelectedIndex = 0;
            this.tabControlWages.Size = new System.Drawing.Size(1303, 543);
            this.tabControlWages.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableWages);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1295, 514);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Вагові коефіцієнти суб\'єктів оцінювання";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableWages
            // 
            this.tableWages.AllowUserToAddRows = false;
            this.tableWages.AllowUserToDeleteRows = false;
            this.tableWages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableWages.ColumnHeadersVisible = false;
            this.tableWages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableWages.Location = new System.Drawing.Point(3, 3);
            this.tableWages.Name = "tableWages";
            this.tableWages.RowHeadersVisible = false;
            this.tableWages.RowTemplate.Height = 24;
            this.tableWages.Size = new System.Drawing.Size(1289, 508);
            this.tableWages.TabIndex = 1;
            this.tableWages.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.tableWages_CellValueChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableWeightness);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1073, 514);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Коефіцієнти вагомості";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableWeightness
            // 
            this.tableWeightness.AllowUserToAddRows = false;
            this.tableWeightness.AllowUserToDeleteRows = false;
            this.tableWeightness.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableWeightness.ColumnHeadersVisible = false;
            this.tableWeightness.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableWeightness.Location = new System.Drawing.Point(3, 3);
            this.tableWeightness.Name = "tableWeightness";
            this.tableWeightness.RowHeadersVisible = false;
            this.tableWeightness.RowTemplate.Height = 24;
            this.tableWeightness.Size = new System.Drawing.Size(1067, 508);
            this.tableWeightness.TabIndex = 2;
            this.tableWeightness.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.tableWeightness_CellValueChanged);
            // 
            // tabPageGrades
            // 
            this.tabPageGrades.Controls.Add(this.tabControlGrades);
            this.tabPageGrades.Location = new System.Drawing.Point(4, 25);
            this.tabPageGrades.Name = "tabPageGrades";
            this.tabPageGrades.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGrades.Size = new System.Drawing.Size(1087, 549);
            this.tabPageGrades.TabIndex = 1;
            this.tabPageGrades.Text = "Оцінки";
            this.tabPageGrades.UseVisualStyleBackColor = true;
            // 
            // tabControlGrades
            // 
            this.tabControlGrades.Controls.Add(this.tabPageUsersGrades);
            this.tabControlGrades.Controls.Add(this.tabPageAllGrades);
            this.tabControlGrades.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlGrades.Location = new System.Drawing.Point(3, 3);
            this.tabControlGrades.Name = "tabControlGrades";
            this.tabControlGrades.SelectedIndex = 0;
            this.tabControlGrades.Size = new System.Drawing.Size(1081, 543);
            this.tabControlGrades.TabIndex = 0;
            // 
            // tabPageUsersGrades
            // 
            this.tabPageUsersGrades.Controls.Add(this.tableUsersGrades);
            this.tabPageUsersGrades.Location = new System.Drawing.Point(4, 25);
            this.tabPageUsersGrades.Name = "tabPageUsersGrades";
            this.tabPageUsersGrades.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUsersGrades.Size = new System.Drawing.Size(1073, 514);
            this.tabPageUsersGrades.TabIndex = 0;
            this.tabPageUsersGrades.Text = "Оцінки користувачів";
            this.tabPageUsersGrades.UseVisualStyleBackColor = true;
            // 
            // tableUsersGrades
            // 
            this.tableUsersGrades.AllowUserToAddRows = false;
            this.tableUsersGrades.AllowUserToDeleteRows = false;
            this.tableUsersGrades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableUsersGrades.ColumnHeadersVisible = false;
            this.tableUsersGrades.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableUsersGrades.Location = new System.Drawing.Point(3, 3);
            this.tableUsersGrades.Name = "tableUsersGrades";
            this.tableUsersGrades.RowHeadersVisible = false;
            this.tableUsersGrades.RowTemplate.Height = 24;
            this.tableUsersGrades.Size = new System.Drawing.Size(1067, 508);
            this.tableUsersGrades.TabIndex = 0;
            this.tableUsersGrades.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.tableUsersGrades_CellValueChanged);
            // 
            // tabPageAllGrades
            // 
            this.tabPageAllGrades.Controls.Add(this.tableAllGrades);
            this.tabPageAllGrades.Location = new System.Drawing.Point(4, 25);
            this.tabPageAllGrades.Name = "tabPageAllGrades";
            this.tabPageAllGrades.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAllGrades.Size = new System.Drawing.Size(1073, 514);
            this.tabPageAllGrades.TabIndex = 1;
            this.tabPageAllGrades.Text = "Оцінки всіх";
            this.tabPageAllGrades.UseVisualStyleBackColor = true;
            // 
            // tableAllGrades
            // 
            this.tableAllGrades.AllowUserToAddRows = false;
            this.tableAllGrades.AllowUserToDeleteRows = false;
            this.tableAllGrades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableAllGrades.ColumnHeadersVisible = false;
            this.tableAllGrades.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableAllGrades.Location = new System.Drawing.Point(3, 3);
            this.tableAllGrades.Name = "tableAllGrades";
            this.tableAllGrades.RowHeadersVisible = false;
            this.tableAllGrades.RowTemplate.Height = 24;
            this.tableAllGrades.Size = new System.Drawing.Size(1067, 508);
            this.tableAllGrades.TabIndex = 1;
            this.tableAllGrades.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.tableAllGrades_CellValueChanged);
            // 
            // tabPageWagedGrades
            // 
            this.tabPageWagedGrades.Controls.Add(this.tableWagedGrades);
            this.tabPageWagedGrades.Location = new System.Drawing.Point(4, 25);
            this.tabPageWagedGrades.Name = "tabPageWagedGrades";
            this.tabPageWagedGrades.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageWagedGrades.Size = new System.Drawing.Size(1087, 549);
            this.tabPageWagedGrades.TabIndex = 2;
            this.tabPageWagedGrades.Text = "Зважені оцінки";
            this.tabPageWagedGrades.UseVisualStyleBackColor = true;
            // 
            // tableWagedGrades
            // 
            this.tableWagedGrades.AllowUserToAddRows = false;
            this.tableWagedGrades.AllowUserToDeleteRows = false;
            this.tableWagedGrades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableWagedGrades.ColumnHeadersVisible = false;
            this.tableWagedGrades.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableWagedGrades.Location = new System.Drawing.Point(3, 3);
            this.tableWagedGrades.Name = "tableWagedGrades";
            this.tableWagedGrades.ReadOnly = true;
            this.tableWagedGrades.RowHeadersVisible = false;
            this.tableWagedGrades.RowTemplate.Height = 24;
            this.tableWagedGrades.Size = new System.Drawing.Size(1081, 543);
            this.tableWagedGrades.TabIndex = 1;
            // 
            // tabPageDiagrams
            // 
            this.tabPageDiagrams.Controls.Add(this.groupBox);
            this.tabPageDiagrams.Controls.Add(this.tableChartData);
            this.tabPageDiagrams.Location = new System.Drawing.Point(4, 25);
            this.tabPageDiagrams.Name = "tabPageDiagrams";
            this.tabPageDiagrams.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDiagrams.Size = new System.Drawing.Size(1309, 549);
            this.tabPageDiagrams.TabIndex = 3;
            this.tabPageDiagrams.Text = "Діаграми";
            this.tabPageDiagrams.UseVisualStyleBackColor = true;
            // 
            // groupBox
            // 
            this.groupBox.Controls.Add(this.label1);
            this.groupBox.Controls.Add(this.comboBoxGrader);
            this.groupBox.Controls.Add(this.polarChart);
            this.groupBox.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox.Location = new System.Drawing.Point(945, 3);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(361, 543);
            this.groupBox.TabIndex = 2;
            this.groupBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Суб\'єкт оцінювання";
            // 
            // comboBoxGrader
            // 
            this.comboBoxGrader.FormattingEnabled = true;
            this.comboBoxGrader.Items.AddRange(new object[] {
            "Експерти галузі",
            "Експерти юзабіліті",
            "Експерти з програмування",
            "Потенційні користувачі"});
            this.comboBoxGrader.Location = new System.Drawing.Point(19, 47);
            this.comboBoxGrader.Name = "comboBoxGrader";
            this.comboBoxGrader.Size = new System.Drawing.Size(206, 24);
            this.comboBoxGrader.TabIndex = 1;
            this.comboBoxGrader.SelectedIndexChanged += new System.EventHandler(this.comboBoxGrader_SelectedIndexChanged);
            // 
            // polarChart
            // 
            chartArea1.Name = "ChartArea1";
            this.polarChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.polarChart.Legends.Add(legend1);
            this.polarChart.Location = new System.Drawing.Point(6, 122);
            this.polarChart.Name = "polarChart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.polarChart.Series.Add(series1);
            this.polarChart.Size = new System.Drawing.Size(349, 353);
            this.polarChart.TabIndex = 0;
            this.polarChart.Text = "chart1";
            // 
            // tableChartData
            // 
            this.tableChartData.AllowUserToAddRows = false;
            this.tableChartData.AllowUserToDeleteRows = false;
            this.tableChartData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableChartData.ColumnHeadersVisible = false;
            this.tableChartData.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableChartData.Location = new System.Drawing.Point(3, 3);
            this.tableChartData.Margin = new System.Windows.Forms.Padding(3, 3, 100, 3);
            this.tableChartData.Name = "tableChartData";
            this.tableChartData.ReadOnly = true;
            this.tableChartData.RowHeadersVisible = false;
            this.tableChartData.RowTemplate.Height = 24;
            this.tableChartData.Size = new System.Drawing.Size(933, 543);
            this.tableChartData.TabIndex = 1;
            // 
            // QualityVisualizer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1317, 578);
            this.Controls.Add(this.tabControl);
            this.Name = "QualityVisualizer";
            this.Text = "Software Quality Visualization";
            this.Load += new System.EventHandler(this.QualityVisualizer_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPageWages.ResumeLayout(false);
            this.tabControlWages.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tableWages)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tableWeightness)).EndInit();
            this.tabPageGrades.ResumeLayout(false);
            this.tabControlGrades.ResumeLayout(false);
            this.tabPageUsersGrades.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tableUsersGrades)).EndInit();
            this.tabPageAllGrades.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tableAllGrades)).EndInit();
            this.tabPageWagedGrades.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tableWagedGrades)).EndInit();
            this.tabPageDiagrams.ResumeLayout(false);
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.polarChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableChartData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageWages;
        private System.Windows.Forms.TabPage tabPageGrades;
        private System.Windows.Forms.TabControl tabControlGrades;
        private System.Windows.Forms.TabPage tabPageUsersGrades;
        private System.Windows.Forms.DataGridView tableUsersGrades;
        private System.Windows.Forms.TabPage tabPageAllGrades;
        private System.Windows.Forms.TabPage tabPageWagedGrades;
        private System.Windows.Forms.TabPage tabPageDiagrams;
        private System.Windows.Forms.DataGridView tableAllGrades;
        private System.Windows.Forms.DataGridView tableWagedGrades;
        private System.Windows.Forms.DataGridView tableChartData;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart polarChart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxGrader;
        private System.Windows.Forms.TabControl tabControlWages;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView tableWages;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView tableWeightness;
    }
}

