﻿using System;
using System.Linq;

namespace RA6
{
    class QualityManager
    {
        public static readonly int CriteriaCount = 10;
        public static readonly int ExpertsCount = 3;
        public static readonly int GradersCount = 4;
        public static readonly int UsersCount = 10;
        public static readonly int CircumferenceAngle = 360;

        private static readonly int MinGrade = 1;
        private static readonly int MaxGrade = 10;

        public static readonly string[] Criteria = {
            "Точність управління та обчислень",
            "Ступінь стандартності інтерфейсів",
            "Функціональна повнота",
            "Стійкість до помилок",
            "Можливість розширення",
            "Зручність роботи",
            "Простота роботи",
            "Відповідність чинним стандартам",
            "Переносність між програмним (апаратним) забезпеченням",
            "Зручність навчання"
        };
        public static readonly string[] ExpertTypes = { "Експерти галузі", "Експерти юзабіліті", "Експерти з програмування", "Потенційні користувачі" };

        public readonly int[][] UsersGrades = new int[UsersCount][];
        public readonly double[][] AllGrades = new double[GradersCount][];
        public readonly int[][] GradersWages = new int[GradersCount][];
        public readonly double[] Weightness = new double[GradersCount];

        public readonly double[][] WagedGrades = new double[GradersCount][];
        public readonly double[] AverageGrades = new double[GradersCount];
        public readonly double[] AverageWagedGrades = new double[GradersCount];
        public readonly double[] AverageCriteriaGrades = new double[CriteriaCount];

        public readonly double[][] radialAngles = new double[GradersCount][];
        public readonly double[][] radialValues = new double[GradersCount][];
        public readonly double[][] Acords = new double[GradersCount][];
        public readonly double[][] Bcords = new double[GradersCount][];
        public readonly double[][] Ccords = new double[GradersCount][];
        public readonly double[] PolygonAreas = new double[GradersCount];

        public QualityManager()
        {
            FillUsersGrades();
            FillAllGrades();
            FillGradersWages();
            FillWeightness();
            CalculateWagedGrades();
            CalculateAverages();
            CalculateAngles();
            DoIntegralCalculations();
        }

        private void FillUsersGrades()
        {
            Random rand = new Random();
            for (int i = 0; i < UsersCount; ++i)
            {
                UsersGrades[i] = new int[CriteriaCount];
                for (int j = 0; j < CriteriaCount; ++j)
                {
                    int userGrade = rand.Next(MinGrade, MaxGrade + 1);
                    UsersGrades[i][j] = userGrade;
                }
            }
        }

        private void FillAllGrades()
        {
            Random rand = new Random();
            for (int i = 0; i < ExpertsCount; ++i)
            {
                AllGrades[i] = new double[CriteriaCount];
                for (int j = 0; j < CriteriaCount; ++j)
                {
                    AllGrades[i][j] = rand.Next(MinGrade, MaxGrade + 1);
                }
            }
            AllGrades[ExpertsCount] = new double[CriteriaCount];
            for (int i = 0; i < CriteriaCount; ++i)
            {
                AllGrades[ExpertsCount][i] = UsersGrades[i].Average();
            }
        }

        private void FillGradersWages()
        {
            Random rand = new Random();
            for (int i = 0; i < GradersCount; ++i)
            {
                GradersWages[i] = new int[CriteriaCount];
                for (int j = 0; j < CriteriaCount; ++j)
                {
                    int wage = rand.Next(MinGrade, MaxGrade + 1);
                    GradersWages[i][j] = wage;
                }
            }
        }

        private void FillWeightness()
        {
            Random rand = new Random();
            for (int i = 0; i < GradersCount; ++i)
            {
                double weight = rand.NextDouble() * 0.8;
                Weightness[i] = weight;
            }
        }

        public void CalculateWagedGrades()
        {
            for (int i = 0; i < GradersCount; ++i)
            {
                WagedGrades[i] = new double[CriteriaCount];
                for (int j = 0; j < CriteriaCount; ++j)
                {
                    WagedGrades[i][j] = AllGrades[i][j] * GradersWages[i][j];
                }
            }
        }

        public void CalculateAverages()
        {
            for (int i = 0; i < GradersCount; ++i)
            {
                AverageGrades[i] = WagedGrades[i].Sum() / GradersWages[i].Sum();
                AverageWagedGrades[i] = AverageGrades[i] * Weightness[i];
            }
            for (int i = 0; i < CriteriaCount; ++i)
            {
                double sum = 0;
                for (int j = 0; j < GradersCount; ++j)
                {
                    sum += WagedGrades[j][i] * Weightness[j];
                }
                AverageCriteriaGrades[i] = sum / Weightness.Sum();
            }
        }

        public void CalculateAngles()
        {
            for (int i = 0; i < GradersCount; ++i)
            {
                radialAngles[i] = new double[CriteriaCount];
                radialValues[i] = new double[CriteriaCount];
                double lastAngle = 0;
                for (int j = 0; j < CriteriaCount; ++j)
                {
                    radialAngles[i][j] = lastAngle;
                    lastAngle += (double)GradersWages[i][j] / GradersWages[i].Sum() * CircumferenceAngle;
                    radialValues[i][j] = WagedGrades[i][j] * Weightness[i];
                }
            }
        }

        public void DoIntegralCalculations()
        {
            for (int i = 0; i < GradersCount; ++i)
            {
                Acords[i] = new double[CriteriaCount];
                Bcords[i] = new double[CriteriaCount];
                Ccords[i] = new double[CriteriaCount];
                double sum = 0;
                for (int j = 0; j < CriteriaCount; ++j)
                {
                    Acords[i][j] = radialValues[i][j] * Math.Sin(radialAngles[i][j] * Math.PI / 180);
                    Bcords[i][j] = radialValues[i][j] * Math.Cos(radialAngles[i][j] * Math.PI / 180);
                    Ccords[i][j] = Math.Sqrt(Acords[i][j] * Acords[i][j] + Bcords[i][j] * Bcords[i][j]);
                }
                for (int j = 0; j < CriteriaCount - 1; ++j)
                {
                    sum += Math.Abs(Acords[i][j] * Bcords[i][j + 1] - Acords[i][j + 1] * Bcords[i][j]);
                }
                sum += Math.Abs(Acords[i][CriteriaCount - 1] * Bcords[i][0] - Acords[i][0] * Bcords[i][CriteriaCount - 1]);
                PolygonAreas[i] = sum / 2;
            }
        }
    }
}
