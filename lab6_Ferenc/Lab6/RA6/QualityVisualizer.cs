﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;

namespace RA6
{
    public partial class QualityVisualizer : Form
    {
        QualityManager Manager = new QualityManager();
        int CurrentGrader = 0;
        static readonly int ChartRows = 11;

        public QualityVisualizer()
        {
            InitializeComponent();
        }
        private void QualityVisualizer_Load(object sender, System.EventArgs e)
        {
            FillWagesTable();
            FillWeightnessTable();
            FillUsersGradesTable();
            FillAllGradesTable();
            FillWagedGradesTable();
            FillChartTable(CurrentGrader);
            DrawChart(CurrentGrader);
        }

        private void FillWagesTable()
        {
            tableWages.Rows.Clear();
            tableWages.Refresh();
            tableWages.ColumnCount = 6;
            tableWages.Columns[0].Width = 40;
            for(int i = 1; i < 6; ++i)
            {
                tableWages.Columns[i].Width = 200;
            }

            List<string> row = new List<string> { "#", "Критерій"};
            row.AddRange(QualityManager.ExpertTypes);
            tableWages.Rows.Add(row.ToArray());
            tableWages.Rows[0].ReadOnly = true;

            for (int i = 0; i < QualityManager.CriteriaCount; ++i)
            {
                row = new List<string> { "" + (i + 1), QualityManager.Criteria[i] };
                for (int j = 0; j < QualityManager.GradersCount; ++j)
                {
                    row.Add(Manager.GradersWages[j][i].ToString());
                }
                int iRow = tableWages.Rows.Add(row.ToArray());
                tableWages.Rows[iRow].Cells[0].ReadOnly = true;
                tableWages.Rows[iRow].Cells[1].ReadOnly = true;
            }

        }
        private void FillWeightnessTable()
        {
            tableWeightness.Rows.Clear();
            tableWeightness.Refresh();
            tableWeightness.ColumnCount = 3;
            for (int i = 0; i < 3; ++i)
            {
                tableWeightness.Columns[i].Width = 200;
            }

            List<string> row = new List<string> { "Типи експертів", "Абсолютний коефіцієнт", "Відносний коефіцієнт" };
            tableWeightness.Rows.Add(row.ToArray());
            tableWeightness.Rows[0].ReadOnly = true;

            for (int i = 0; i < QualityManager.GradersCount; ++i)
            {
                row = new List<string> { QualityManager.ExpertTypes[i], (Manager.Weightness[i] * 10).ToString("n1"),
                    Manager.Weightness[i].ToString("n2") };

                int iRow = tableWeightness.Rows.Add(row.ToArray());
                tableWeightness.Rows[iRow].Cells[0].ReadOnly = true;
                tableWeightness.Rows[iRow].Cells[1].ReadOnly = true;
            }
        }
        private void FillUsersGradesTable()
        {
            tableUsersGrades.Rows.Clear();
            tableUsersGrades.Refresh();
            tableUsersGrades.ColumnCount = 2 + QualityManager.UsersCount;

            tableUsersGrades.Columns[0].Width = 20;
            tableUsersGrades.Columns[1].Width = 300;
            tableUsersGrades.Columns[2].Width = 90;
            for (int i = 3; i < 2 + QualityManager.UsersCount; ++i)
            {
                tableUsersGrades.Columns[i].Width = 60;
            }

            List<string> row = new List<string> { "", "", "Користувачі" };
            tableUsersGrades.Rows.Add(row.ToArray());
            tableUsersGrades.Rows[0].ReadOnly = true;

            row = new List<string> { "#", "Критерії" };
            row.AddRange(Enumerable.Range(1, QualityManager.UsersCount).Select(num => "#" + num));
            tableUsersGrades.Rows.Add(row.ToArray());
            tableUsersGrades.Rows[1].ReadOnly = true;

            for (int i = 0; i < QualityManager.CriteriaCount; ++i)
            {
                row = new List<string> { "" + i, QualityManager.Criteria[i] };
                for (int j = 0; j < QualityManager.UsersCount; ++j)
                {
                    row.Add(Manager.UsersGrades[j][i] + "");
                }
                int iRow = tableUsersGrades.Rows.Add(row.ToArray());
                tableUsersGrades.Rows[iRow].Cells[0].ReadOnly = true;
                tableUsersGrades.Rows[iRow].Cells[1].ReadOnly = true;

            }
        }
        private void FillAllGradesTable()
        {
            tableAllGrades.Rows.Clear();
            tableAllGrades.Refresh();
            tableAllGrades.ColumnCount = 6;
            tableAllGrades.Columns[0].Width = 40;
            for (int i = 1; i < 6; ++i)
            {
                tableAllGrades.Columns[i].Width = 200;
            }

            List<string> row = new List<string> { "#", "Критерій" };
            row.AddRange(QualityManager.ExpertTypes);
            tableAllGrades.Rows.Add(row.ToArray());
            tableAllGrades.Rows[0].ReadOnly = true;

            for (int i = 0; i < QualityManager.CriteriaCount; ++i)
            {
                row = new List<string> { "" + (i + 1), QualityManager.Criteria[i] };
                for (int j = 0; j < QualityManager.GradersCount; ++j)
                {
                    row.Add(Manager.AllGrades[j][i].ToString("n2"));
                }
                int iRow = tableAllGrades.Rows.Add(row.ToArray());
                tableAllGrades.Rows[iRow].Cells[0].ReadOnly = true;
                tableAllGrades.Rows[iRow].Cells[1].ReadOnly = true;
                tableAllGrades.Rows[iRow].Cells[1 + QualityManager.GradersCount].ReadOnly = true;
            }
        }
        private void FillWagedGradesTable()
        {
            tableWagedGrades.Rows.Clear();
            tableWagedGrades.Refresh();
            tableWagedGrades.ColumnCount = 7;
            tableWagedGrades.Columns[0].Width = 40;
            for (int i = 1; i < 6; ++i)
            {
                tableWagedGrades.Columns[i].Width = 150;
            }

            List<string> row = new List<string> { "#", "Критерій" };
            row.AddRange(QualityManager.ExpertTypes);
            row.Add("Усереднені оцінки");
            tableWagedGrades.Rows.Add(row.ToArray());

            for (int i = 0; i < QualityManager.CriteriaCount; ++i)
            {
                row = new List<string> { "" + (i + 1), QualityManager.Criteria[i] };
                for (int j = 0; j < QualityManager.GradersCount; ++j)
                {
                    row.Add(Manager.WagedGrades[j][i].ToString("n2"));
                }
                row.Add(Manager.AverageCriteriaGrades[i].ToString("n2"));

                tableWagedGrades.Rows.Add(row.ToArray());
            }
            row = new List<string> { "", "Усереднені оцінки" };
            for (int j = 0; j < QualityManager.GradersCount; ++j)
            {
                row.Add(Manager.AverageGrades[j].ToString("n2"));
            }
            tableWagedGrades.Rows.Add(row.ToArray());

            row = new List<string> { "", "Зважені оцінки" };
            for (int j = 0; j < QualityManager.GradersCount; ++j)
            {
                row.Add(Manager.AverageWagedGrades[j].ToString("n2"));
            }
            tableWagedGrades.Rows.Add(row.ToArray());

            tableWagedGrades.Rows.Add("");

            row = new List<string> { "", "Інтегральна оцінка",
                (Manager.AverageWagedGrades.Sum() / Manager.Weightness.Sum()).ToString("n2")};
            tableWagedGrades.Rows.Add(row.ToArray());
        }
        private void FillChartTable(int grader)
        {
            tableChartData.Rows.Clear();
            tableChartData.Refresh();
            tableChartData.ColumnCount = 9;
            tableChartData.Columns[0].Width = 40;
            tableChartData.Columns[1].Width = 200;
            tableChartData.Columns[2].Width = 60;
            tableChartData.Columns[3].Width = 200;
            List<string> row = new List<string> { "#", "Критерій", "Оцінка", "Частка вагового коефіцієнту",
                "Кут повороту", "Кут вектору", "A", "B", "C" };
            tableChartData.Rows.Add(row.ToArray());
            
            for (int i = 0; i < QualityManager.CriteriaCount; ++i)
            {
                row = new List<string> { "" + (i + 1), QualityManager.Criteria[i], Manager.radialValues[grader][i].ToString("n2") };

                double particle = (double)Manager.GradersWages[grader][i] / Manager.GradersWages[grader].Sum();
                row.Add(particle.ToString("n2"));
                row.Add((particle * QualityManager.CircumferenceAngle).ToString("n2"));
                row.Add(Manager.radialAngles[grader][i].ToString("n2"));
                row.Add(Manager.Acords[grader][i].ToString("n2"));
                row.Add(Manager.Bcords[grader][i].ToString("n2"));
                row.Add(Manager.Ccords[grader][i].ToString("n2"));

                tableChartData.Rows.Add(row.ToArray());
            }
            tableChartData.Rows.Add("");

            row = new List<string> { "", "Площа утвореного багатокутника",
                Manager.PolygonAreas[grader].ToString("n2")};
            tableChartData.Rows.Add(row.ToArray());
        }
        private void DrawChart(int grader)
        {
            polarChart.Series.Clear();
            Series serie = polarChart.Series.Add("points");

            serie.BorderWidth = 5;
            serie.ChartType = SeriesChartType.Polar;
            for (int i = 0; i < QualityManager.CriteriaCount; ++i)
            {
                serie.Points.AddXY(Manager.radialAngles[grader][i], Manager.radialValues[grader][i]);
            }
            serie.Points.AddXY(Manager.radialAngles[grader][0], Manager.radialValues[grader][0]);
            polarChart.Legends.Clear();
        }

        private void comboBoxGrader_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            CurrentGrader = comboBoxGrader.SelectedIndex;
            FillChartTable(CurrentGrader);
            DrawChart(CurrentGrader);
        }

        private void tableWages_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            string input = (string)tableWages[e.ColumnIndex, e.RowIndex].Value;
            int res;

            if (int.TryParse(input, out res) && (res >= 0 && res <= 10))
            {
                res = int.Parse((string)tableWages[e.ColumnIndex, e.RowIndex].Value);
            }
            else
            {
                res = 0;
                MessageBox.Show("Некоректне введення. Будь ласка, введіть число від 0 до 10.");
            }
            if (res > 10)
            {
                res = 10;
            }
            if (res < 0)
            {
                res = 0;
            }
            Manager.GradersWages[e.ColumnIndex - 2][e.RowIndex - 1] = res;

            Manager.CalculateWagedGrades();
            Manager.CalculateAverages();
            Manager.CalculateAngles();
            Manager.DoIntegralCalculations();

            FillWagedGradesTable();
            if (e.ColumnIndex - 2 == CurrentGrader)
            {
                FillChartTable(CurrentGrader);
                DrawChart(CurrentGrader);
            }
        }

        private void tableWeightness_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            string input = (string)tableWages[e.ColumnIndex, e.RowIndex].Value;
            double res;

            if (double.TryParse(input, out res) && (res >= 0 && res <= 10))
            {
                res = double.Parse((string)tableWages[e.ColumnIndex, e.RowIndex].Value);
            }
            else
            {
                res = 0.0;
                MessageBox.Show("Некоректне введення. Будь ласка, введіть число  від 0 до 10.");
            }
            if (res > 10)
            {
                res = 10;
            }
            if (res < 0)
            {
                res = 0;
            }
            Manager.Weightness[e.RowIndex - 1] = res;

            Manager.CalculateWagedGrades();
            Manager.CalculateAverages();
            Manager.CalculateAngles();
            Manager.DoIntegralCalculations();

            FillWagedGradesTable();
            if (e.RowIndex - 1 == CurrentGrader)
            {
                FillChartTable(CurrentGrader);
                DrawChart(CurrentGrader);
            }
        }

        private void tableUsersGrades_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            string input = (string)tableWages[e.ColumnIndex, e.RowIndex].Value;
            int res;

            if (int.TryParse(input, out res) && (res >= 0 && res <= 10))
            {
                res = int.Parse((string)tableWages[e.ColumnIndex, e.RowIndex].Value);
            }
            else
            {
                res = 0;
                MessageBox.Show("Некоректне введення. Будь ласка, введіть число  від 0 до 10.");
            }
            if (res > 10)
            {
                res = 10;
            }
            if (res < 0)
            {
                res = 0;
            }
            Manager.UsersGrades[e.ColumnIndex - 2][e.RowIndex - 2] = res;

            Manager.CalculateWagedGrades();
            Manager.CalculateAverages();
            Manager.CalculateAngles();
            Manager.DoIntegralCalculations();

            FillWagedGradesTable();
            FillAllGradesTable();
            if (e.ColumnIndex - 2 == CurrentGrader)
            {
                FillChartTable(CurrentGrader);
                DrawChart(CurrentGrader);
            }
        }

        private void tableAllGrades_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            string input = (string)tableWages[e.ColumnIndex, e.RowIndex].Value;
            double res;

            if (double.TryParse(input, out res) && (res >= 0 && res <= 10))
            {
                res = double.Parse((string)tableWages[e.ColumnIndex, e.RowIndex].Value);
            }
            else
            {
                res = 0.0;
                MessageBox.Show("Некоректне введення. Будь ласка, введіть число  від 0 до 10.");
            }
            if (res > 10)
            {
                res = 10;
            }
            if (res < 0)
            {
                res = 0;
            }
            Manager.AllGrades[e.ColumnIndex - 2][e.RowIndex - 1] = res;

            Manager.CalculateWagedGrades();
            Manager.CalculateAverages();
            Manager.CalculateAngles();
            Manager.DoIntegralCalculations();

            FillWagedGradesTable();
            if (e.ColumnIndex - 2 == CurrentGrader)
            {
                FillChartTable(CurrentGrader);
                DrawChart(CurrentGrader);
            }
        }

    }
}
