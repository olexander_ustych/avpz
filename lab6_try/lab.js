const layout = {title: "Diagrama"};
const plot = document.getElementById('plot');   

/*const data = [{
    labels: xL,
    values: yV,
    type: "pie"
}, 
{
    labels: xL1,
    values: yV1,
    type: "pie"
}];*/



const r = 10; //r^2 = 100
const x = r;


const xTopValues = [];
const yTopValues = [];
for(let x = r * -1; x <= r; x += Math.PI / 100)
{
    xTopValues.push(x);
    yTopValues.push(Math.sqrt(r*r - x*x));

    xTopValues.push(x);    
    yTopValues.push(Math.sqrt(r*r - x*x) * -1);
}

xTopValues.push(x);
yTopValues.push(Math.sqrt(r*r - x*x));

xTopValues.push(x);
yTopValues.push(Math.sqrt(r*r - x*x) * -1);

/*const xBottomValues = [];
const yBottomValues = [];
for(let x = r * -1; x <= r; x += Math.PI / 100)
{
    xBottomValues.push(x);    
    yBottomValues.push(Math.sqrt(r*r - x*x) * -1);
}

xBottomValues.push(x);
yBottomValues.push(Math.sqrt(r*r - x*x) * -1);*/



const Line1X = [0, Math.sqrt(50)];
const Line1Y = [0, Math.sqrt(50)];

const Line2X = [0, -1 * Math.sqrt(50)];
const Line2Y = [0, Math.sqrt(50)];



const segmentX = [];
const segmentY = [];
for(let x = Math.sqrt(50) * -1; x <= Math.sqrt(50); x += 0.01)
{
    segmentX.push(x / 1.5);
    let potY = Math.sqrt(r*r - x*x);    
    segmentY.push(potY / 1.5);

    segmentX.push(0);
    segmentY.push(0);
}


const data = [{
    x: xTopValues, 
    y: yTopValues, 
    mode:"lines"
},
{
    x: Line1X,
    y: Line1Y,
    mode: "lines"
},
{
    x: Line2X,
    y: Line2Y,
    mode: "lines"
},
{
    x: segmentX,
    y: segmentY,
    mode: "lines"
}];

Plotly.newPlot(plot, data, layout);