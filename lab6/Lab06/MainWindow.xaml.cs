﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveChartsCore;
using LiveChartsCore.Defaults;
using LiveChartsCore.Kernel.Sketches;
using LiveChartsCore.SkiaSharpView;
using LiveChartsCore.SkiaSharpView.Painting;
using RA_06.Charts;
using RA_06.Tables;
using SkiaSharp;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace RA_06
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TablesSeeder seeder = new TablesSeeder();

        Chart chart1;
        public MainWindow()
        {
            InitializeComponent();

            Table1.ItemsSource = new ObservableCollection<Table1>(seeder.table1);
            Table1_Experts.ItemsSource = seeder.table1_experts;
            Table2.ItemsSource = seeder.table2;
            Table3.ItemsSource = new ObservableCollection<Table3>(seeder.table3);

            Table4Domain.ItemsSource = seeder.GetDomain();
            Table4Usability.ItemsSource = seeder.GetUsability();
            Table4Users.ItemsSource = seeder.GetUsers();
            Table4Programming.ItemsSource = seeder.GetProgramming();
            Table4Average.ItemsSource = seeder.GetAverage();
            Table4ExpertValues.ItemsSource = seeder.GetExpertValues();   
            
            RefreshCharts();
        }

        private void RefreshCharts()
        {
            const double C = Math.PI * 10000;
            List<Table4> table4domain = new List<Table4>();
            List<Table4> table4usability = new List<Table4>();
            List<Table4> table4programming = new List<Table4>();
            List<Table4> table4user = new List<Table4>();
            List<Table4> table4avg = seeder.GetAverage();

            int size = CoefNames.Names.Count;
            for(int i = 0; i < size; ++i)
            {
                table4domain.Add(new Table4(i + 1, "", seeder.table1[i].expertIndustry));
                table4usability.Add(new Table4(i + 1, "", seeder.table1[i].expertUsability));
                table4programming.Add(new Table4(i + 1, "", seeder.table1[i].expertProgramming));
                table4user.Add(new Table4(i + 1, "", seeder.table1[i].potentialUsers));
            }

            double area = seeder.CalculateDiagram(table4domain, "Експерт галузі", out Chart.degreeDomain, out Chart.radiusDomain);
            LabelDomain.Content = "S = " + area + " z = " + area / C;
            area = seeder.CalculateDiagram(table4usability, "Експерт юзабіліті", out Chart.degreeUsability, out Chart.radiusUsability);
            LabelUsability.Content = "S = " + area + " z = " + area / C;
            area = seeder.CalculateDiagram(table4programming, "Експерт з програмування", out Chart.degreeProgramming, out Chart.radiusProgramming);
            LabelProgramming.Content = "S = " + area + " z = " + area / C;
            area = seeder.CalculateDiagram(table4user, "Потенційні користувачі", out Chart.degreeUsers, out Chart.radiusUsers);
            LabelUsers.Content = "S = " + area + " z = " + area / C;
            area = seeder.CalculateDiagram(table4avg, "Усереднені оцінки", out Chart.degreeAverage, out Chart.radiusAverage);
            LabelAverage.Content = "S = " + area + " z = " + area / C;

            PolarLineSeries<ObservablePolarPoint> domain =  new PolarLineSeries<ObservablePolarPoint>
            {
                Values = new ObservablePolarPoint[]
                {
                    new ObservablePolarPoint(Chart.degreeDomain[0],Chart. radiusDomain[0]),
                    new ObservablePolarPoint(Chart.degreeDomain[1],Chart. radiusDomain[1]),
                    new ObservablePolarPoint(Chart.degreeDomain[2],Chart. radiusDomain[2]),
                    new ObservablePolarPoint(Chart.degreeDomain[3],Chart. radiusDomain[3]),
                    new ObservablePolarPoint(Chart.degreeDomain[4],Chart. radiusDomain[4]),
                    new ObservablePolarPoint(Chart.degreeDomain[5],Chart. radiusDomain[5]),
                    new ObservablePolarPoint(Chart.degreeDomain[6],Chart. radiusDomain[6]),
                    new ObservablePolarPoint(Chart.degreeDomain[7],Chart. radiusDomain[7]),
                    new ObservablePolarPoint(Chart.degreeDomain[8],Chart. radiusDomain[8]),
                    new ObservablePolarPoint(Chart.degreeDomain[9],Chart. radiusDomain[9]),
                },

                IsClosed = true,

                GeometryFill = null,
                GeometryStroke = null,
                LineSmoothness = 0,      
                Fill = new SolidColorPaint(new SKColor(255, 0, 0, 30)),
                Stroke = new SolidColorPaint(new SKColor(255, 0, 0, 100)),  
            };

            PolarLineSeries<ObservablePolarPoint> usability = new PolarLineSeries<ObservablePolarPoint>
            {
                Values = new ObservablePolarPoint[]
                {
                    new ObservablePolarPoint(Chart.degreeUsability[0],Chart. radiusUsability[0]),
                    new ObservablePolarPoint(Chart.degreeUsability[1],Chart. radiusUsability[1]),
                    new ObservablePolarPoint(Chart.degreeUsability[2],Chart. radiusUsability[2]),
                    new ObservablePolarPoint(Chart.degreeUsability[3],Chart. radiusUsability[3]),
                    new ObservablePolarPoint(Chart.degreeUsability[4],Chart. radiusUsability[4]),
                    new ObservablePolarPoint(Chart.degreeUsability[5],Chart. radiusUsability[5]),
                    new ObservablePolarPoint(Chart.degreeUsability[6],Chart. radiusUsability[6]),
                    new ObservablePolarPoint(Chart.degreeUsability[7],Chart. radiusUsability[7]),
                    new ObservablePolarPoint(Chart.degreeUsability[8],Chart. radiusUsability[8]),
                    new ObservablePolarPoint(Chart.degreeUsability[9],Chart. radiusUsability[9]),
                },

                IsClosed = true,

                GeometryFill = null,
                GeometryStroke = null,
                LineSmoothness = 0,        
                Fill = new SolidColorPaint(new SKColor(0, 0, 255, 30)),
                Stroke = new SolidColorPaint(new SKColor(0, 0, 255, 100)),   
            };

            PolarLineSeries<ObservablePolarPoint> programming = new PolarLineSeries<ObservablePolarPoint>
            {
                Values = new ObservablePolarPoint[]
                {
                    new ObservablePolarPoint(Chart.degreeUsers[0],Chart. radiusProgramming[0]),
                    new ObservablePolarPoint(Chart.degreeUsers[1],Chart. radiusProgramming[1]),
                    new ObservablePolarPoint(Chart.degreeUsers[2],Chart. radiusProgramming[2]),
                    new ObservablePolarPoint(Chart.degreeUsers[3],Chart. radiusProgramming[3]),
                    new ObservablePolarPoint(Chart.degreeUsers[4],Chart. radiusProgramming[4]),
                    new ObservablePolarPoint(Chart.degreeUsers[5],Chart. radiusProgramming[5]),
                    new ObservablePolarPoint(Chart.degreeUsers[6],Chart. radiusProgramming[6]),
                    new ObservablePolarPoint(Chart.degreeUsers[7],Chart. radiusProgramming[7]),
                    new ObservablePolarPoint(Chart.degreeUsers[8],Chart. radiusProgramming[8]),
                    new ObservablePolarPoint(Chart.degreeUsers[9],Chart. radiusProgramming[9]),
                },

                IsClosed = true,

                GeometryFill = null,
                GeometryStroke = null,
                LineSmoothness = 0,         
                Fill = new SolidColorPaint(new SKColor(255, 0, 255, 30)),
                Stroke = new SolidColorPaint(new SKColor(255, 0, 255, 100)),   
            };

            PolarLineSeries<ObservablePolarPoint> users = new PolarLineSeries<ObservablePolarPoint>
            {
                Values = new ObservablePolarPoint[]
                {
                    new ObservablePolarPoint(Chart.degreeUsers[0],Chart. radiusUsers[0]),
                    new ObservablePolarPoint(Chart.degreeUsers[1],Chart. radiusUsers[1]),
                    new ObservablePolarPoint(Chart.degreeUsers[2],Chart. radiusUsers[2]),
                    new ObservablePolarPoint(Chart.degreeUsers[3],Chart. radiusUsers[3]),
                    new ObservablePolarPoint(Chart.degreeUsers[4],Chart. radiusUsers[4]),
                    new ObservablePolarPoint(Chart.degreeUsers[5],Chart. radiusUsers[5]),
                    new ObservablePolarPoint(Chart.degreeUsers[6],Chart. radiusUsers[6]),
                    new ObservablePolarPoint(Chart.degreeUsers[7],Chart. radiusUsers[7]),
                    new ObservablePolarPoint(Chart.degreeUsers[8],Chart. radiusUsers[8]),
                    new ObservablePolarPoint(Chart.degreeUsers[9],Chart. radiusUsers[9]),
                },

                IsClosed = true,

                GeometryFill = null,
                GeometryStroke = null,
                LineSmoothness = 0,    
                Fill = new SolidColorPaint(new SKColor(0, 255, 0, 30)),
                Stroke = new SolidColorPaint(new SKColor(0, 255, 0, 100)),   
            };

            PolarLineSeries<ObservablePolarPoint> average = new PolarLineSeries<ObservablePolarPoint>
            {
                Values = new ObservablePolarPoint[]
                {
                    new ObservablePolarPoint(Chart.degreeAverage[0],Chart. radiusAverage[0]),
                    new ObservablePolarPoint(Chart.degreeAverage[1],Chart. radiusAverage[1]),
                    new ObservablePolarPoint(Chart.degreeAverage[2],Chart. radiusAverage[2]),
                    new ObservablePolarPoint(Chart.degreeAverage[3],Chart. radiusAverage[3]),
                    new ObservablePolarPoint(Chart.degreeAverage[4],Chart. radiusAverage[4]),
                    new ObservablePolarPoint(Chart.degreeAverage[5],Chart. radiusAverage[5]),
                    new ObservablePolarPoint(Chart.degreeAverage[6],Chart. radiusAverage[6]),
                    new ObservablePolarPoint(Chart.degreeAverage[7],Chart. radiusAverage[7]),
                    new ObservablePolarPoint(Chart.degreeAverage[8],Chart. radiusAverage[8]),
                    new ObservablePolarPoint(Chart.degreeAverage[9],Chart. radiusAverage[9]),
                },

                IsClosed = true,

                GeometryFill = null,
                GeometryStroke = null,
                LineSmoothness = 0,             
            };

            ChartDomain.Series = new[] { domain };
            ChartUsability.Series = new[] { usability };
            ChartProgramming.Series = new[] { programming };
            ChartUser.Series = new[] { users };
            ChartAverage.Series = new[] { average };

            ChartAll.Series = new[] { domain, usability, programming, users, average };
        }

        private void Validate(object sender, DataGridCellEditEndingEventArgs e)
        {
            try
            {
                int ColumnIndex = e.Column.DisplayIndex;

                double val = double.Parse(((TextBox)e.EditingElement).Text, CultureInfo.InvariantCulture);

                if (val > 10 || val < 0)
                {
                    ((TextBox)e.EditingElement).Text = "0";
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Некоректні вхідні дані!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Table1_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            Validate(sender, e);

            seeder.CalculateTable1();

            seeder.CalculateTable3();
            CollectionViewSource.GetDefaultView(Table3.ItemsSource).Refresh();

            Table4Domain.ItemsSource = seeder.GetDomain();
            Table4Usability.ItemsSource = seeder.GetUsability();
            Table4Users.ItemsSource = seeder.GetUsers();
            Table4Programming.ItemsSource = seeder.GetProgramming();
            Table4Average.ItemsSource = seeder.GetAverage();

            RefreshCharts();
        }

        private void Table1_Experts_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            Validate(sender, e);
            seeder.CalculateTable3();
            CollectionViewSource.GetDefaultView(Table3.ItemsSource).Refresh();

            RefreshCharts();
        }

        private void Table2_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            Validate(sender, e);

            if (e.Column.DisplayIndex == 1)
            {
                seeder.CalculateTable2RelativeCoef();
            }
            else if (e.Column.DisplayIndex == 2)
            {
                seeder.CalculateTable2AbsoluteCoef();
            }

            RefreshCharts();
        }

        private void Table2_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                CollectionViewSource.GetDefaultView(Table2.ItemsSource).Refresh();
            }
            catch (Exception)
            {
                MessageBox.Show("Некоректні вхідні дані!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Table1_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                CollectionViewSource.GetDefaultView(Table1.ItemsSource).Refresh();
            }
            catch (Exception)
            {
                MessageBox.Show("Некоректні вхідні дані!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
