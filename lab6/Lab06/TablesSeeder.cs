﻿using RA_06.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiveChartsCore.Defaults;
using LiveChartsCore.SkiaSharpView;
using RA_06.Charts;
using RA_06.Tables;
using LiveChartsCore;

namespace RA_06
{
    public class TablesSeeder
    {
        public List<Table1> table1 = new List<Table1>();
        public List<Table1_Experts> table1_experts = new List<Table1_Experts>();
        public List<Table2> table2 = new List<Table2>();
        public List<Table3> table3 = new List<Table3>();
        //public List<Table4> table4 = new List<Table4>();

        public TablesSeeder()
        {
            int size = CoefNames.Names.Count;
            for(int i = 0; i < size; ++i)
            {
                table1.Add(new Table1(
                    i + 1, 
                    CoefNames.Names[i], 
                    Table1Coefs.domain[i],
                    Table1Coefs.usability[i],
                    Table1Coefs.programming[i],
                    Table1Coefs.users[i]
                    ));

                table1_experts.Add(new Table1_Experts(
                   Table1_ExpertsCoefs.domain[i],
                   Table1_ExpertsCoefs.usability[i],
                   Table1_ExpertsCoefs.programming[i],
                   Table1_ExpertsCoefs.users[i]
                    ));
            }

            size = TypesOfExperts.Names.Count;
            for (int i = 0; i < size; ++i)
            {
                table2.Add(new Table2(
                    TypesOfExperts.Names[i],
                    Table2Coefs.absolute[i],
                    Table2Coefs.relative[i]
                    ));
            }


            size = CoefNames.Names.Count;

            double domain, usability, programming, users, sum, Xi;
            double sumOfRelativeCoef = table2.Sum(x => x.relativeСoef);

            for (int i = 0; i < size; ++i)
            {
                domain = table1[i].expertIndustry * table1_experts[i].domain;
                usability = table1[i].expertUsability * table1_experts[i].usability;
                programming = table1[i].expertProgramming * table1_experts[i].programming;
                users = table1[i].potentialUsers * table1_experts[i].users;
                sum = (domain * table2[0].relativeСoef
                       + usability * table2[1].relativeСoef
                       + programming * table2[2].relativeСoef
                       + users * table2[3].relativeСoef)
                      / sumOfRelativeCoef;
                Xi = sum / table1[i].avg;

                table3.Add(new Table3(
                    i + 1,
                    CoefNames.Names[i],
                    domain,
                    usability,
                    programming,
                    users,
                    sum,
                    Xi
                ));
            }
        }

        public void CalculateTable1()
        {
            int size = CoefNames.Names.Count;

            for (int i = 0; i < size; ++i)
            {
                table1[i].sum = table1[i].potentialUsers + table1[i].expertUsability + table1[i].expertIndustry + table1[i].expertProgramming;
                table1[i].avg = table1[i].sum / 4;
            }
        }

        public void CalculateTable2AbsoluteCoef()
        {
            int size = TypesOfExperts.Names.Count;

            for(int i = 0; i < size; ++i)
            {
                table2[i].absoluteСoef = table2[i].relativeСoef * 10;
            }
        }

        public void CalculateTable2RelativeCoef()
        {
            int size = TypesOfExperts.Names.Count;

            for(int i = 0; i < size; ++i)
            {
                table2[i].relativeСoef = table2[i].absoluteСoef / 10;
            }
        }

        public void CalculateTable3()
        {
            int size = CoefNames.Names.Count;
            
            double domain, usability, programming, users, sum, Xi;
            double sumOfRelativeCoef = table2.Sum(x => x.relativeСoef);
        
            for(int i = 0; i < size; ++i)
            {
                domain = table1[i].expertIndustry * table1_experts[i].domain;
                usability = table1[i].expertUsability * table1_experts[i].usability;
                programming = table1[i].expertProgramming * table1_experts[i].programming;
                users = table1[i].potentialUsers * table1_experts[i].users;
                sum = (domain * table2[0].relativeСoef 
                       + usability * table2[1].relativeСoef
                       + programming * table2[2].relativeСoef
                       + users * table2[3].relativeСoef)
                      / sumOfRelativeCoef;
                Xi = sum / table1[i].avg;

                table3[i].expertIndustry = domain;
                table3[i].expertUsability = usability;
                table3[i].expertProgramming = programming;
                table3[i].potentialUsers = users;
                table3[i].sum = sum;
                table3[i].Xi = Xi;
            }
        }

        public List<Table4> GetDomain()
        {
            List<Table4> domain = new List<Table4>();

            int size = CoefNames.Names.Count;
            for(int i = 0; i < size; ++i)
            {
                domain.Add(new Table4(
                    i + 1,
                    CoefNames.Names[i],
                    table1[i].expertIndustry
                    ));
            }

            return domain;
        }

        public List<Table4> GetUsability()
        {
            List<Table4> usability = new List<Table4>();

            int size = CoefNames.Names.Count;
            for(int i = 0; i < size; ++i)
            {
                usability.Add(new Table4(
                    i + 1,
                    CoefNames.Names[i],
                    table1[i].expertUsability
                ));
            }

            return usability;
        }

        public List<Table4> GetProgramming()
        {
            List<Table4> programming = new List<Table4>();

            int size = CoefNames.Names.Count;
            for(int i = 0; i < size; ++i)
            {
                programming.Add(new Table4(
                    i + 1,
                    CoefNames.Names[i],
                    table1[i].expertProgramming
                ));
            }

            return programming;
        }

        public List<Table4> GetUsers()
        {
            List<Table4> users = new List<Table4>();

            int size = CoefNames.Names.Count;
            for(int i = 0; i < size; ++i)
            {
                users.Add(new Table4(
                    i + 1,
                    CoefNames.Names[i],
                    table1[i].potentialUsers
                ));
            }

            return users;
        }

        public List<Table4> GetAverage()
        {
            List<Table4> average = new List<Table4>();

            int size = CoefNames.Names.Count;
            for(int i = 0; i < size; ++i)
            {
                average.Add(new Table4(
                    i + 1,
                    CoefNames.Names[i],
                    table1[i].avg
                ));
            }

            return average;
        }

        public List<Table4> GetExpertValues()
        {
            List<Table4> vals = new List<Table4>();

            List<double> values = new List<double>()
            {
                table2[0].relativeСoef,
                table2[1].relativeСoef,
                table2[2].relativeСoef,
                table2[3].relativeСoef
            };

            values.Add(values.Average());

            int size = TypesOfExperts.Names.Count;
            for(int i = 0; i < size; ++i)
            {
                vals.Add(new Table4(
                    i + 1,
                    CoefNames.Names[i],
                    values[i]
                ));
            }

            vals.Add(new Table4(
                size + 1,
                "Усереднені оцінки (Xi)",
                values.Last()
                ));

            return vals;
        }

        public double CalculateDiagram(List<Table4> table, string typeOfExpert, out List<double> Degree, out List<double> Radius)
        {
            double area = 0;
            int size = CoefNames.Names.Count;
            double sumCoef = table.Sum(x => x.coef);

            List<double> partOfCircle = new List<double>();

            for(int i = 0; i < size; ++i) 
            {
                partOfCircle.Add(table[i].coef / sumCoef * 360 );    
            }

            List<double> vals = new List<double>();
            vals.Add((0 - partOfCircle[0]) / 2);

            for(int i = 0; i < size; ++i)
            {
                vals.Add(partOfCircle[i] + vals[i]);
            }

            List<double> degree = new();

            for(int i = 0; i < size; ++i)
            {
                degree.Add((vals[i] + vals[i + 1]) / 2);
            }

            List<double> radian = new List<double>();

            for( int i = 0; i < size; ++i)
            {
                radian.Add((Math.PI / 180) * degree[i]);
            }

            int index = -1;
            string fieldName = "";

            switch(typeOfExpert)
            {
                case "Експерт галузі":
                    index = 0;
                    fieldName = "domain";
                    break;
                case "Експерт юзабіліті":
                    index = 1;
                    fieldName = "usability";
                    break;
                case "Експерт з програмування":
                    index = 2;
                    fieldName = "programming";
                    break;
                case "Потенційні користувачі":
                    index = 3;
                    fieldName = "users";
                    break;
                case "Усереднені оцінки":
                    index = -1;
                    break;
            }

            double q = 0;
            List<double> xwq = new List<double>();

            if (index >= 0)
            {
                q = table2[index].relativeСoef;

                for (int i = 0; i < size; ++i)
                {
                    xwq.Add(table[i].coef * (double)GetPropValue(table1_experts[i], fieldName) * q);
                }                
            }
            else if(index == -1)
            {
                for(int i = 0; i < table2.Count; ++i)
                {
                    q += table2[i].relativeСoef;
                }

                for (int i = 0; i < size; ++i)
                {
                    xwq.Add(
                        (table1[i].expertIndustry * table1_experts[i].domain * table2[0].relativeСoef
                            + table1[i].expertUsability * table1_experts[i].usability * table2[1].relativeСoef
                            + table1[i].expertProgramming * table1_experts[i].programming * table2[2].relativeСoef
                            + table1[i].potentialUsers * table1_experts[i].users * table2[3].relativeСoef)
                            / q
                        );    
                }

            }

            Degree = new List<double>(degree);
            Radius = new List<double>(xwq);

            List<double> a = new List<double>();
            List<double> b = new List<double>();

            for(int i = 0; i < size; ++i)
            {
                a.Add(xwq[i] * Math.Sin(radian[i]));
                b.Add(xwq[i] * Math.Cos(radian[i]));           
            }

            a.Add(a[0]);
            b.Add(b[0]);

            for(int i = 0; i < size; ++i)
            {
                area += Math.Abs(a[i] * b[i + 1] - b[i] * a[i + 1]);
            }

            return area / 2;
        }

        private static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }
    }
}
