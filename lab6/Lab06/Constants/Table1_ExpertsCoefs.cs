﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA_06.Constants
{
    public class Table1_ExpertsCoefs
    {
        public static List<double> domain = new List<double>()
        {
            10, 9, 9, 6, 7, 9, 10, 6, 9, 6
        };

        public static List<double> usability = new List<double>()
        {
            9, 8, 7, 5, 5, 7, 9, 8, 7, 5
        };

        public static List<double> programming = new List<double>()
        {
            10, 8, 9, 8, 8, 8, 10, 7, 6, 9
        };

        public static List<double> users = new List<double>()
        {
            8.05, 7.50, 6.10, 7.70, 6.05, 7.85, 7.35, 5.55, 7.85, 4.3
        };
    }
}
