﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA_06.Constants
{
    public class TypesOfExperts
    {
        public static List<string> Names = new List<string>
        {
            "Експерт галузі", 
            "Експерт юзабіліті", 
            "Експерт з програмування",
            "Потенційні користувачі",
        };
    }
}
