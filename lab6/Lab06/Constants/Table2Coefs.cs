﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA_06.Constants
{
    public class Table2Coefs
    {
        public static List<double> absolute = new List<double>()
        {
            7, 8, 9, 5
        };

        public static List<double> relative = absolute.Select(x => x / 10).ToList();
    }
}
