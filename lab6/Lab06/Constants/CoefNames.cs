﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace RA_06
{
    public class CoefNames
    {
        public static List<string> Names = new List<string>()
        {
            "Точність управління та обчислень",      
            "Ступінь стандартності інтерфейсівь", 
            "Функціональна повнота",
            "Стійкість до помилок",
            "Можливість розширення",
            "Зручність роботи",
            "Простота роботи",
            "Відповідність чинним стандартам",
            "Переносимість між ПЗ",
            "Зручність навчання",
        };
    }
}
