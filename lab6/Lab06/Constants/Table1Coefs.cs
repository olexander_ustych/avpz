﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RA_06.Constants
{
    public class Table1Coefs
    {
        public static List<double> domain = new List<double>()
        {
            8, 5, 10, 6, 5, 9, 9, 6, 8, 7
        };

        public static List<double> usability = new List<double>()
        {
            5, 9, 6, 5, 5, 9, 7, 5, 6, 8
        };

        public static List<double> programming = new List<double>()
        {
            9, 6, 9, 10, 10, 7, 6, 10, 9, 6 
        };

        public static List<double> users = new List<double>()
        {
            7, 5, 6, 7, 4, 10, 10, 5, 6, 10
        };
    }
}
