﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using LiveChartsCore;
using LiveChartsCore.Defaults;
using LiveChartsCore.SkiaSharpView;

namespace RA_06.Charts
{
    public class Chart
    {
        public static List<double> degreeDomain = new List<double>() { 0, 32.05, 69.04, 108.49, 135.62, 170.14, 214.52, 251.51, 286.03, 323.01  };
        public static List<double> radiusDomain = new List<double>() { 56, 25.2, 63, 25.2, 19.6, 56.7, 63, 21, 50.4, 29.4 };

        public static List<double> degreeUsability = new List<double>() { 0, 32.05, 69.04, 108.49, 135.62, 170.14, 214.52, 251.51, 286.03, 323.01  };
        public static List<double> radiusUsability = new List<double>() { 56, 25.2, 63, 25.2, 19.6, 56.7, 63, 21, 50.4, 29.4 };

        public static List<double> degreeProgramming = new List<double>() { 0, 32.05, 69.04, 108.49, 135.62, 170.14, 214.52, 251.51, 286.03, 323.01  };
        public static List<double> radiusProgramming = new List<double>() { 56, 25.2, 63, 25.2, 19.6, 56.7, 63, 21, 50.4, 29.4 };

        public static List<double> degreeUsers = new List<double>() { 0, 32.05, 69.04, 108.49, 135.62, 170.14, 214.52, 251.51, 286.03, 323.01  };
        public static List<double> radiusUsers = new List<double>() { 56, 25.2, 63, 25.2, 19.6, 56.7, 63, 21, 50.4, 29.4 };

        public static List<double> degreeAverage = new List<double>() { 0, 32.05, 69.04, 108.49, 135.62, 170.14, 214.52, 251.51, 286.03, 323.01  };
        public static List<double> radiusAverage = new List<double>() { 56, 25.2, 63, 25.2, 19.6, 56.7, 63, 21, 50.4, 29.4 };

        public static IEnumerable<ObservablePolarPoint> data =  new ObservablePolarPoint[]
        {
            new ObservablePolarPoint(degreeDomain[0], radiusDomain[0]),
            new ObservablePolarPoint(degreeDomain[1], radiusDomain[1]),
            new ObservablePolarPoint(degreeDomain[2], radiusDomain[2]),
            new ObservablePolarPoint(degreeDomain[3], radiusDomain[3]),
            new ObservablePolarPoint(degreeDomain[4], radiusDomain[4]),
            new ObservablePolarPoint(degreeDomain[5], radiusDomain[5]),
            new ObservablePolarPoint(degreeDomain[6], radiusDomain[6]),
            new ObservablePolarPoint(degreeDomain[7], radiusDomain[7]),
            new ObservablePolarPoint(degreeDomain[8], radiusDomain[8]),
            new ObservablePolarPoint(degreeDomain[9], radiusDomain[9]),
        };

        public ISeries[] SeriesDomain { get; set; } = new[]
        {

            new PolarLineSeries<ObservablePolarPoint>
            {
                Values = data,

                IsClosed = true,

                GeometryFill = null,
                GeometryStroke = null,
                LineSmoothness = 0,          
            }
        };
        public ISeries[] SeriesUsability{ get; set; } = new[]
        {
            new PolarLineSeries<ObservablePolarPoint>
            {
                Values = data,

                IsClosed = true,

                GeometryFill = null,
                GeometryStroke = null,
                LineSmoothness = 0,          
            }
        };

        public ISeries[] SeriesProgramming { get; set; } = new[]
        {

            new PolarLineSeries<ObservablePolarPoint>
            {
                Values = data,

                IsClosed = true,

                GeometryFill = null,
                GeometryStroke = null,
                LineSmoothness = 0,          
            }
        };

        public ISeries[] SeriesUser { get; set; } = new[]
        {

            new PolarLineSeries<ObservablePolarPoint>
            {
                Values = data,

                IsClosed = true,

                GeometryFill = null,
                GeometryStroke = null,
                LineSmoothness = 0,          
            }
        };

        public ISeries[] SeriesAverage { get; set; } = new[]
        {

            new PolarLineSeries<ObservablePolarPoint>
            {
                Values = data,

                IsClosed = true,

                GeometryFill = null,
                GeometryStroke = null,
                LineSmoothness = 0,          
            }
        };
    } 
 }
