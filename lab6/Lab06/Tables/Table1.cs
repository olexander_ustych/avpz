﻿namespace RA_06.Tables
{
    public class Table1 
    {
        public int num { get; set; }
        public string criteriaName { get; set; }
        public double expertIndustry { get; set; }
        public double expertUsability { get; set; }
        public double expertProgramming { get; set; }
        public double potentialUsers { get; set; }
        public double sum { get; set; }
        public double avg { get; set; }


        public Table1() 
        {   
        }

        public Table1(int n, string cname, double industry, double usab, double p, double users) 
        {         
            num = n;
            criteriaName = cname;
            expertIndustry = industry;
            expertUsability = usab;
            expertProgramming = p;
            potentialUsers = users;
            sum = industry + usab + p + users;
            avg = sum / 4;
        }
    }
}
