﻿namespace RA_06.Tables
{
    public class Table4
    {
        public int num { get; set; }
        public string criteriaName { get; set; }
        public double coef { get; set; }

        public Table4() { }
        public Table4(int n, string cname, double cf)
        {
            num = n;
            criteriaName = cname;
            coef = cf;
        }
    }
}
