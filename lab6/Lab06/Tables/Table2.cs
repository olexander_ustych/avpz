﻿namespace RA_06.Tables
{
    public class Table2
    {
        public string typeOfExpert {get; set; }
        public double absoluteСoef {get; set; }
        public double relativeСoef {get; set; }

        public Table2() { }
        public Table2(string type, double abs, double rel)
        {
            typeOfExpert = type;
            absoluteСoef = abs;
            relativeСoef = rel;
        }
    }
}
