﻿namespace RA_06.Tables
{
    public class Table1_Experts
    {
        public double domain {get;set;}
        public double usability {get;set;}
        public double programming {get;set;}
        public double users { get; set; }
        public Table1_Experts() { }
        public Table1_Experts(double d, double usab, double progr, double use)
        {
            domain = d;
            usability = usab;
            programming = progr;
            users = use;
        }
    }
}
