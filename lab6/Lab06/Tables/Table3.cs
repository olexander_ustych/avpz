﻿namespace RA_06.Tables
{
    public class Table3 
    {
        public int num { get; set; }
        public string criteriaName { get; set; }
        public double expertIndustry { get; set; }
        public double expertUsability { get; set; }
        public double expertProgramming { get; set; }
        public double potentialUsers { get; set; }
        public double sum { get; set; }
        public double Xi { get; set; }

        public Table3() { }
        public Table3(int n, string critname, double ind, double usab, double progr, double users, double s, double xi)
        {
            num = n;
            criteriaName = critname;
            expertIndustry = ind;
            expertUsability = usab;
            expertProgramming = progr;
            potentialUsers = users;
            sum = s;
            Xi = xi;
        }
    }
}
